package main

import (
	"github.com/ajhager/enj"
	"math/rand"
)

var (
	app    *enj.App
	region *enj.Region
	batch  *enj.Batch
	meats  []*Sprite
	on     bool
	num    int
	i      int
)

func init() {
}

type Sprite struct {
	X, Y   float32
	DX, DY float32
	Image  *enj.Region
}

type Flyingmeat struct {
	*enj.Game
}

func (f *Flyingmeat) Load() {
	app.Load.Image("../data/meat.png")
}

func (f *Flyingmeat) Setup() {
	batch = app.NewBatch()
	region = app.NewTexture("../data/meat.png", false).Region(0, 0, 106, 126)
	for i := 0; i < 100; i++ {
		meats = append(meats, &Sprite{0, 0, rand.Float32() * 500, (rand.Float32() * 500) - 250, region})
	}
}

func (f *Flyingmeat) Update(dt float32) {
	minX := float32(0)
	maxX := app.Width() - region.Width()
	minY := float32(0)
	maxY := app.Height() - region.Height()

	for _, meat := range meats {
		meat.X += meat.DX * dt
		meat.Y += meat.DY * dt
		meat.DY += 750 * dt

		if meat.X < minX {
			meat.DX *= -1
			meat.X = minX
		} else if meat.X > maxX {
			meat.DX *= -1
			meat.X = maxX
		}

		if meat.Y < minY {
			meat.DY = 0
			meat.Y = minY
		} else if meat.Y > maxY {
			meat.DY *= -.85
			meat.Y = maxY
			if rand.Float32() > 0.5 {
				meat.DY -= rand.Float32() * 200
			}
		}
	}
}

func (f *Flyingmeat) Draw() {
	batch.Begin()
	for _, meat := range meats {
		batch.Draw(meat.Image, meat.X, meat.Y, 0, 0, 1, 1, 0)
	}
	batch.End()
}

func main() {
	app = enj.NewApp(800, 600, false, "Meatcubes", new(Flyingmeat))
}
