"use strict";
(function() {

Error.stackTraceLimit = -1;

var $global, $module;
if (typeof window !== "undefined") { /* web page */
  $global = window;
} else if (typeof self !== "undefined") { /* web worker */
  $global = self;
} else if (typeof global !== "undefined") { /* Node.js */
  $global = global;
  $global.require = require;
} else {
  console.log("warning: no global object found");
}
if (typeof module !== "undefined") {
  $module = module;
}

var $packages = {}, $reflect, $idCounter = 0;
var $keys = function(m) { return m ? Object.keys(m) : []; };
var $min = Math.min;
var $mod = function(x, y) { return x % y; };
var $parseInt = parseInt;
var $parseFloat = function(f) {
  if (f.constructor === Number) {
    return f;
  }
  return parseFloat(f);
};

var $mapArray = function(array, f) {
  var newArray = new array.constructor(array.length), i;
  for (i = 0; i < array.length; i++) {
    newArray[i] = f(array[i]);
  }
  return newArray;
};

var $methodVal = function(recv, name) {
  var vals = recv.$methodVals || {};
  recv.$methodVals = vals; /* noop for primitives */
  var f = vals[name];
  if (f !== undefined) {
    return f;
  }
  var method = recv[name];
  f = function() {
    $stackDepthOffset--;
    try {
      return method.apply(recv, arguments);
    } finally {
      $stackDepthOffset++;
    }
  };
  vals[name] = f;
  return f;
};

var $methodExpr = function(method) {
  if (method.$expr === undefined) {
    method.$expr = function() {
      $stackDepthOffset--;
      try {
        return Function.call.apply(method, arguments);
      } finally {
        $stackDepthOffset++;
      }
    };
  }
  return method.$expr;
};

var $subslice = function(slice, low, high, max) {
  if (low < 0 || high < low || max < high || high > slice.$capacity || max > slice.$capacity) {
    $throwRuntimeError("slice bounds out of range");
  }
  var s = new slice.constructor(slice.$array);
  s.$offset = slice.$offset + low;
  s.$length = slice.$length - low;
  s.$capacity = slice.$capacity - low;
  if (high !== undefined) {
    s.$length = high - low;
  }
  if (max !== undefined) {
    s.$capacity = max - low;
  }
  return s;
};

var $sliceToArray = function(slice) {
  if (slice.$length === 0) {
    return [];
  }
  if (slice.$array.constructor !== Array) {
    return slice.$array.subarray(slice.$offset, slice.$offset + slice.$length);
  }
  return slice.$array.slice(slice.$offset, slice.$offset + slice.$length);
};

var $decodeRune = function(str, pos) {
  var c0 = str.charCodeAt(pos);

  if (c0 < 0x80) {
    return [c0, 1];
  }

  if (c0 !== c0 || c0 < 0xC0) {
    return [0xFFFD, 1];
  }

  var c1 = str.charCodeAt(pos + 1);
  if (c1 !== c1 || c1 < 0x80 || 0xC0 <= c1) {
    return [0xFFFD, 1];
  }

  if (c0 < 0xE0) {
    var r = (c0 & 0x1F) << 6 | (c1 & 0x3F);
    if (r <= 0x7F) {
      return [0xFFFD, 1];
    }
    return [r, 2];
  }

  var c2 = str.charCodeAt(pos + 2);
  if (c2 !== c2 || c2 < 0x80 || 0xC0 <= c2) {
    return [0xFFFD, 1];
  }

  if (c0 < 0xF0) {
    var r = (c0 & 0x0F) << 12 | (c1 & 0x3F) << 6 | (c2 & 0x3F);
    if (r <= 0x7FF) {
      return [0xFFFD, 1];
    }
    if (0xD800 <= r && r <= 0xDFFF) {
      return [0xFFFD, 1];
    }
    return [r, 3];
  }

  var c3 = str.charCodeAt(pos + 3);
  if (c3 !== c3 || c3 < 0x80 || 0xC0 <= c3) {
    return [0xFFFD, 1];
  }

  if (c0 < 0xF8) {
    var r = (c0 & 0x07) << 18 | (c1 & 0x3F) << 12 | (c2 & 0x3F) << 6 | (c3 & 0x3F);
    if (r <= 0xFFFF || 0x10FFFF < r) {
      return [0xFFFD, 1];
    }
    return [r, 4];
  }

  return [0xFFFD, 1];
};

var $encodeRune = function(r) {
  if (r < 0 || r > 0x10FFFF || (0xD800 <= r && r <= 0xDFFF)) {
    r = 0xFFFD;
  }
  if (r <= 0x7F) {
    return String.fromCharCode(r);
  }
  if (r <= 0x7FF) {
    return String.fromCharCode(0xC0 | r >> 6, 0x80 | (r & 0x3F));
  }
  if (r <= 0xFFFF) {
    return String.fromCharCode(0xE0 | r >> 12, 0x80 | (r >> 6 & 0x3F), 0x80 | (r & 0x3F));
  }
  return String.fromCharCode(0xF0 | r >> 18, 0x80 | (r >> 12 & 0x3F), 0x80 | (r >> 6 & 0x3F), 0x80 | (r & 0x3F));
};

var $stringToBytes = function(str) {
  var array = new Uint8Array(str.length), i;
  for (i = 0; i < str.length; i++) {
    array[i] = str.charCodeAt(i);
  }
  return array;
};

var $bytesToString = function(slice) {
  if (slice.$length === 0) {
    return "";
  }
  var str = "", i;
  for (i = 0; i < slice.$length; i += 10000) {
    str += String.fromCharCode.apply(null, slice.$array.subarray(slice.$offset + i, slice.$offset + Math.min(slice.$length, i + 10000)));
  }
  return str;
};

var $stringToRunes = function(str) {
  var array = new Int32Array(str.length);
  var rune, i, j = 0;
  for (i = 0; i < str.length; i += rune[1], j++) {
    rune = $decodeRune(str, i);
    array[j] = rune[0];
  }
  return array.subarray(0, j);
};

var $runesToString = function(slice) {
  if (slice.$length === 0) {
    return "";
  }
  var str = "", i;
  for (i = 0; i < slice.$length; i++) {
    str += $encodeRune(slice.$array[slice.$offset + i]);
  }
  return str;
};

var $copyString = function(dst, src) {
  var n = Math.min(src.length, dst.$length), i;
  for (i = 0; i < n; i++) {
    dst.$array[dst.$offset + i] = src.charCodeAt(i);
  }
  return n;
};

var $copySlice = function(dst, src) {
  var n = Math.min(src.$length, dst.$length), i;
  $internalCopy(dst.$array, src.$array, dst.$offset, src.$offset, n, dst.constructor.elem);
  return n;
};

var $copy = function(dst, src, type) {
  var i;
  switch (type.kind) {
  case "Array":
    $internalCopy(dst, src, 0, 0, src.length, type.elem);
    return true;
  case "Struct":
    for (i = 0; i < type.fields.length; i++) {
      var field = type.fields[i];
      var name = field[0];
      if (!$copy(dst[name], src[name], field[3])) {
        dst[name] = src[name];
      }
    }
    return true;
  default:
    return false;
  }
};

var $internalCopy = function(dst, src, dstOffset, srcOffset, n, elem) {
  var i;
  if (n === 0) {
    return;
  }

  if (src.subarray) {
    dst.set(src.subarray(srcOffset, srcOffset + n), dstOffset);
    return;
  }

  switch (elem.kind) {
  case "Array":
  case "Struct":
    for (i = 0; i < n; i++) {
      $copy(dst[dstOffset + i], src[srcOffset + i], elem);
    }
    return;
  }

  for (i = 0; i < n; i++) {
    dst[dstOffset + i] = src[srcOffset + i];
  }
};

var $clone = function(src, type) {
  var clone = type.zero();
  $copy(clone, src, type);
  return clone;
};

var $append = function(slice) {
  return $internalAppend(slice, arguments, 1, arguments.length - 1);
};

var $appendSlice = function(slice, toAppend) {
  return $internalAppend(slice, toAppend.$array, toAppend.$offset, toAppend.$length);
};

var $internalAppend = function(slice, array, offset, length) {
  if (length === 0) {
    return slice;
  }

  var newArray = slice.$array;
  var newOffset = slice.$offset;
  var newLength = slice.$length + length;
  var newCapacity = slice.$capacity;

  if (newLength > newCapacity) {
    newOffset = 0;
    newCapacity = Math.max(newLength, slice.$capacity < 1024 ? slice.$capacity * 2 : Math.floor(slice.$capacity * 5 / 4));

    if (slice.$array.constructor === Array) {
      newArray = slice.$array.slice(slice.$offset, slice.$offset + slice.$length);
      newArray.length = newCapacity;
      var zero = slice.constructor.elem.zero, i;
      for (i = slice.$length; i < newCapacity; i++) {
        newArray[i] = zero();
      }
    } else {
      newArray = new slice.$array.constructor(newCapacity);
      newArray.set(slice.$array.subarray(slice.$offset, slice.$offset + slice.$length));
    }
  }

  $internalCopy(newArray, array, newOffset + slice.$length, offset, length, slice.constructor.elem);

  var newSlice = new slice.constructor(newArray);
  newSlice.$offset = newOffset;
  newSlice.$length = newLength;
  newSlice.$capacity = newCapacity;
  return newSlice;
};

var $equal = function(a, b, type) {
  if (a === b) {
    return true;
  }
  var i;
  switch (type.kind) {
  case "Float32":
    return $float32IsEqual(a, b);
  case "Complex64":
    return $float32IsEqual(a.$real, b.$real) && $float32IsEqual(a.$imag, b.$imag);
  case "Complex128":
    return a.$real === b.$real && a.$imag === b.$imag;
  case "Int64":
  case "Uint64":
    return a.$high === b.$high && a.$low === b.$low;
  case "Ptr":
    if (a.constructor.Struct) {
      return false;
    }
    return $pointerIsEqual(a, b);
  case "Array":
    if (a.length != b.length) {
      return false;
    }
    var i;
    for (i = 0; i < a.length; i++) {
      if (!$equal(a[i], b[i], type.elem)) {
        return false;
      }
    }
    return true;
  case "Struct":
    for (i = 0; i < type.fields.length; i++) {
      var field = type.fields[i];
      var name = field[0];
      if (!$equal(a[name], b[name], field[3])) {
        return false;
      }
    }
    return true;
  default:
    return false;
  }
};

var $interfaceIsEqual = function(a, b) {
  if (a === null || b === null || a === undefined || b === undefined || a.constructor !== b.constructor) {
    return a === b;
  }
  switch (a.constructor.kind) {
  case "Func":
  case "Map":
  case "Slice":
  case "Struct":
    $throwRuntimeError("comparing uncomparable type " + a.constructor.string);
  case undefined: /* js.Object */
    return a === b;
  default:
    return $equal(a.$val, b.$val, a.constructor);
  }
};

var $float32IsEqual = function(a, b) {
  if (a === b) {
    return true;
  }
  if (a === 0 || b === 0 || a === 1/0 || b === 1/0 || a === -1/0 || b === -1/0 || a !== a || b !== b) {
    return false;
  }
  var math = $packages["math"];
  return math !== undefined && math.Float32bits(a) === math.Float32bits(b);
};

var $sliceIsEqual = function(a, ai, b, bi) {
  return a.$array === b.$array && a.$offset + ai === b.$offset + bi;
};

var $pointerIsEqual = function(a, b) {
  if (a === b) {
    return true;
  }
  if (a.$get === $throwNilPointerError || b.$get === $throwNilPointerError) {
    return a.$get === $throwNilPointerError && b.$get === $throwNilPointerError;
  }
  var old = a.$get();
  var dummy = new Object();
  a.$set(dummy);
  var equal = b.$get() === dummy;
  a.$set(old);
  return equal;
};

var $typeAssertionFailed = function(obj, expected) {
  var got = "";
  if (obj !== null) {
    got = obj.constructor.string;
  }
  $panic(new $packages["runtime"].TypeAssertionError.Ptr("", got, expected.string, ""));
};

var $newType = function(size, kind, string, name, pkgPath, constructor) {
  var typ;
  switch(kind) {
  case "Bool":
  case "Int":
  case "Int8":
  case "Int16":
  case "Int32":
  case "Uint":
  case "Uint8" :
  case "Uint16":
  case "Uint32":
  case "Uintptr":
  case "String":
  case "UnsafePointer":
    typ = function(v) { this.$val = v; };
    typ.prototype.$key = function() { return string + "$" + this.$val; };
    break;

  case "Float32":
  case "Float64":
    typ = function(v) { this.$val = v; };
    typ.prototype.$key = function() { return string + "$" + $floatKey(this.$val); };
    break;

  case "Int64":
    typ = function(high, low) {
      this.$high = (high + Math.floor(Math.ceil(low) / 4294967296)) >> 0;
      this.$low = low >>> 0;
      this.$val = this;
    };
    typ.prototype.$key = function() { return string + "$" + this.$high + "$" + this.$low; };
    break;

  case "Uint64":
    typ = function(high, low) {
      this.$high = (high + Math.floor(Math.ceil(low) / 4294967296)) >>> 0;
      this.$low = low >>> 0;
      this.$val = this;
    };
    typ.prototype.$key = function() { return string + "$" + this.$high + "$" + this.$low; };
    break;

  case "Complex64":
  case "Complex128":
    typ = function(real, imag) {
      this.$real = real;
      this.$imag = imag;
      this.$val = this;
    };
    typ.prototype.$key = function() { return string + "$" + this.$real + "$" + this.$imag; };
    break;

  case "Array":
    typ = function(v) { this.$val = v; };
    typ.Ptr = $newType(4, "Ptr", "*" + string, "", "", function(array) {
      this.$get = function() { return array; };
      this.$val = array;
    });
    typ.init = function(elem, len) {
      typ.elem = elem;
      typ.len = len;
      typ.prototype.$key = function() {
        return string + "$" + Array.prototype.join.call($mapArray(this.$val, function(e) {
          var key = e.$key ? e.$key() : String(e);
          return key.replace(/\\/g, "\\\\").replace(/\$/g, "\\$");
        }), "$");
      };
      typ.extendReflectType = function(rt) {
        rt.arrayType = new $reflect.arrayType.Ptr(rt, elem.reflectType(), undefined, len);
      };
      typ.Ptr.init(typ);
      for (var i = 0; i < len; i++) {
        Object.defineProperty(typ.Ptr.nil, i, { get: $throwNilPointerError, set: $throwNilPointerError });
      }
    };
    break;

  case "Chan":
    typ = function(capacity) {
      this.$val = this;
      this.$capacity = capacity;
      this.$buffer = [];
      this.$sendQueue = [];
      this.$recvQueue = [];
      this.$closed = false;
    };
    typ.prototype.$key = function() {
      if (this.$id === undefined) {
        $idCounter++;
        this.$id = $idCounter;
      }
      return String(this.$id);
    };
    typ.init = function(elem, sendOnly, recvOnly) {
      typ.elem = elem;
      typ.sendOnly = sendOnly;
      typ.recvOnly = recvOnly;
      typ.nil = new typ(0);
      typ.nil.$sendQueue = typ.nil.$recvQueue = { length: 0, push: function() {}, shift: function() { return undefined; } };
      typ.extendReflectType = function(rt) {
        rt.chanType = new $reflect.chanType.Ptr(rt, elem.reflectType(), sendOnly ? $reflect.SendDir : (recvOnly ? $reflect.RecvDir : $reflect.BothDir));
      };
    };
    break;

  case "Func":
    typ = function(v) { this.$val = v; };
    typ.init = function(params, results, variadic) {
      typ.params = params;
      typ.results = results;
      typ.variadic = variadic;
      typ.extendReflectType = function(rt) {
        var typeSlice = ($sliceType($ptrType($reflect.rtype.Ptr)));
        rt.funcType = new $reflect.funcType.Ptr(rt, variadic, new typeSlice($mapArray(params, function(p) { return p.reflectType(); })), new typeSlice($mapArray(results, function(p) { return p.reflectType(); })));
      };
    };
    break;

  case "Interface":
    typ = { implementedBy: [] };
    typ.init = function(methods) {
      typ.methods = methods;
      typ.extendReflectType = function(rt) {
        var imethods = $mapArray(methods, function(m) {
          return new $reflect.imethod.Ptr($newStringPtr(m[1]), $newStringPtr(m[2]), $funcType(m[3], m[4], m[5]).reflectType());
        });
        var methodSlice = ($sliceType($ptrType($reflect.imethod.Ptr)));
        rt.interfaceType = new $reflect.interfaceType.Ptr(rt, new methodSlice(imethods));
      };
    };
    break;

  case "Map":
    typ = function(v) { this.$val = v; };
    typ.init = function(key, elem) {
      typ.key = key;
      typ.elem = elem;
      typ.extendReflectType = function(rt) {
        rt.mapType = new $reflect.mapType.Ptr(rt, key.reflectType(), elem.reflectType(), undefined, undefined);
      };
    };
    break;

  case "Ptr":
    typ = constructor || function(getter, setter, target) {
      this.$get = getter;
      this.$set = setter;
      this.$target = target;
      this.$val = this;
    };
    typ.prototype.$key = function() {
      if (this.$id === undefined) {
        $idCounter++;
        this.$id = $idCounter;
      }
      return String(this.$id);
    };
    typ.init = function(elem) {
      typ.nil = new typ($throwNilPointerError, $throwNilPointerError);
      typ.extendReflectType = function(rt) {
        rt.ptrType = new $reflect.ptrType.Ptr(rt, elem.reflectType());
      };
    };
    break;

  case "Slice":
    var nativeArray;
    typ = function(array) {
      if (array.constructor !== nativeArray) {
        array = new nativeArray(array);
      }
      this.$array = array;
      this.$offset = 0;
      this.$length = array.length;
      this.$capacity = array.length;
      this.$val = this;
    };
    typ.make = function(length, capacity) {
      capacity = capacity || length;
      var array = new nativeArray(capacity), i;
      if (nativeArray === Array) {
        for (i = 0; i < capacity; i++) {
          array[i] = typ.elem.zero();
        }
      }
      var slice = new typ(array);
      slice.$length = length;
      return slice;
    };
    typ.init = function(elem) {
      typ.elem = elem;
      nativeArray = $nativeArray(elem.kind);
      typ.nil = new typ([]);
      typ.extendReflectType = function(rt) {
        rt.sliceType = new $reflect.sliceType.Ptr(rt, elem.reflectType());
      };
    };
    break;

  case "Struct":
    typ = function(v) { this.$val = v; };
    typ.prototype.$key = function() { $throwRuntimeError("hash of unhashable type " + string); };
    typ.Ptr = $newType(4, "Ptr", "*" + string, "", "", constructor);
    typ.Ptr.Struct = typ;
    typ.Ptr.prototype.$get = function() { return this; };
    typ.init = function(fields) {
      var i;
      typ.fields = fields;
      typ.Ptr.extendReflectType = function(rt) {
        rt.ptrType = new $reflect.ptrType.Ptr(rt, typ.reflectType());
      };
      /* nil value */
      typ.Ptr.nil = Object.create(constructor.prototype);
      typ.Ptr.nil.$val = typ.Ptr.nil;
      for (i = 0; i < fields.length; i++) {
        var field = fields[i];
        Object.defineProperty(typ.Ptr.nil, field[0], { get: $throwNilPointerError, set: $throwNilPointerError });
      }
      /* methods for embedded fields */
      for (i = 0; i < typ.methods.length; i++) {
        var method = typ.methods[i];
        if (method[6] != -1) {
          (function(field, methodName) {
            typ.prototype[methodName] = function() {
              var v = this.$val[field[0]];
              return v[methodName].apply(v, arguments);
            };
          })(fields[method[6]], method[0]);
        }
      }
      for (i = 0; i < typ.Ptr.methods.length; i++) {
        var method = typ.Ptr.methods[i];
        if (method[6] != -1) {
          (function(field, methodName) {
            typ.Ptr.prototype[methodName] = function() {
              var v = this[field[0]];
              if (v.$val === undefined) {
                v = new field[3](v);
              }
              return v[methodName].apply(v, arguments);
            };
          })(fields[method[6]], method[0]);
        }
      }
      /* reflect type */
      typ.extendReflectType = function(rt) {
        var reflectFields = new Array(fields.length), i;
        for (i = 0; i < fields.length; i++) {
          var field = fields[i];
          reflectFields[i] = new $reflect.structField.Ptr($newStringPtr(field[1]), $newStringPtr(field[2]), field[3].reflectType(), $newStringPtr(field[4]), i);
        }
        rt.structType = new $reflect.structType.Ptr(rt, new ($sliceType($reflect.structField.Ptr))(reflectFields));
      };
    };
    break;

  default:
    $panic(new $String("invalid kind: " + kind));
  }

  switch(kind) {
  case "Bool":
  case "Map":
    typ.zero = function() { return false; };
    break;

  case "Int":
  case "Int8":
  case "Int16":
  case "Int32":
  case "Uint":
  case "Uint8" :
  case "Uint16":
  case "Uint32":
  case "Uintptr":
  case "UnsafePointer":
  case "Float32":
  case "Float64":
    typ.zero = function() { return 0; };
    break;

  case "String":
    typ.zero = function() { return ""; };
    break;

  case "Int64":
  case "Uint64":
  case "Complex64":
  case "Complex128":
    var zero = new typ(0, 0);
    typ.zero = function() { return zero; };
    break;

  case "Chan":
  case "Ptr":
  case "Slice":
    typ.zero = function() { return typ.nil; };
    break;

  case "Func":
    typ.zero = function() { return $throwNilPointerError; };
    break;

  case "Interface":
    typ.zero = function() { return null; };
    break;

  case "Array":
    typ.zero = function() {
      var arrayClass = $nativeArray(typ.elem.kind);
      if (arrayClass !== Array) {
        return new arrayClass(typ.len);
      }
      var array = new Array(typ.len), i;
      for (i = 0; i < typ.len; i++) {
        array[i] = typ.elem.zero();
      }
      return array;
    };
    break;

  case "Struct":
    typ.zero = function() { return new typ.Ptr(); };
    break;

  default:
    $panic(new $String("invalid kind: " + kind));
  }

  typ.kind = kind;
  typ.string = string;
  typ.typeName = name;
  typ.pkgPath = pkgPath;
  typ.methods = [];
  var rt = null;
  typ.reflectType = function() {
    if (rt === null) {
      rt = new $reflect.rtype.Ptr(size, 0, 0, 0, 0, $reflect.kinds[kind], undefined, undefined, $newStringPtr(string), undefined, undefined);
      rt.jsType = typ;

      var methods = [];
      if (typ.methods !== undefined) {
        var i;
        for (i = 0; i < typ.methods.length; i++) {
          var m = typ.methods[i];
          methods.push(new $reflect.method.Ptr($newStringPtr(m[1]), $newStringPtr(m[2]), $funcType(m[3], m[4], m[5]).reflectType(), $funcType([typ].concat(m[3]), m[4], m[5]).reflectType(), undefined, undefined));
        }
      }
      if (name !== "" || methods.length !== 0) {
        var methodSlice = ($sliceType($ptrType($reflect.method.Ptr)));
        rt.uncommonType = new $reflect.uncommonType.Ptr($newStringPtr(name), $newStringPtr(pkgPath), new methodSlice(methods));
        rt.uncommonType.jsType = typ;
      }

      if (typ.extendReflectType !== undefined) {
        typ.extendReflectType(rt);
      }
    }
    return rt;
  };
  return typ;
};

var $Bool          = $newType( 1, "Bool",          "bool",           "bool",       "", null);
var $Int           = $newType( 4, "Int",           "int",            "int",        "", null);
var $Int8          = $newType( 1, "Int8",          "int8",           "int8",       "", null);
var $Int16         = $newType( 2, "Int16",         "int16",          "int16",      "", null);
var $Int32         = $newType( 4, "Int32",         "int32",          "int32",      "", null);
var $Int64         = $newType( 8, "Int64",         "int64",          "int64",      "", null);
var $Uint          = $newType( 4, "Uint",          "uint",           "uint",       "", null);
var $Uint8         = $newType( 1, "Uint8",         "uint8",          "uint8",      "", null);
var $Uint16        = $newType( 2, "Uint16",        "uint16",         "uint16",     "", null);
var $Uint32        = $newType( 4, "Uint32",        "uint32",         "uint32",     "", null);
var $Uint64        = $newType( 8, "Uint64",        "uint64",         "uint64",     "", null);
var $Uintptr       = $newType( 4, "Uintptr",       "uintptr",        "uintptr",    "", null);
var $Float32       = $newType( 4, "Float32",       "float32",        "float32",    "", null);
var $Float64       = $newType( 8, "Float64",       "float64",        "float64",    "", null);
var $Complex64     = $newType( 8, "Complex64",     "complex64",      "complex64",  "", null);
var $Complex128    = $newType(16, "Complex128",    "complex128",     "complex128", "", null);
var $String        = $newType( 8, "String",        "string",         "string",     "", null);
var $UnsafePointer = $newType( 4, "UnsafePointer", "unsafe.Pointer", "Pointer",    "", null);

var $nativeArray = function(elemKind) {
  return ({ Int: Int32Array, Int8: Int8Array, Int16: Int16Array, Int32: Int32Array, Uint: Uint32Array, Uint8: Uint8Array, Uint16: Uint16Array, Uint32: Uint32Array, Uintptr: Uint32Array, Float32: Float32Array, Float64: Float64Array })[elemKind] || Array;
};
var $toNativeArray = function(elemKind, array) {
  var nativeArray = $nativeArray(elemKind);
  if (nativeArray === Array) {
    return array;
  }
  return new nativeArray(array);
};
var $arrayTypes = {};
var $arrayType = function(elem, len) {
  var string = "[" + len + "]" + elem.string;
  var typ = $arrayTypes[string];
  if (typ === undefined) {
    typ = $newType(12, "Array", string, "", "", null);
    typ.init(elem, len);
    $arrayTypes[string] = typ;
  }
  return typ;
};

var $chanType = function(elem, sendOnly, recvOnly) {
  var string = (recvOnly ? "<-" : "") + "chan" + (sendOnly ? "<- " : " ") + elem.string;
  var field = sendOnly ? "SendChan" : (recvOnly ? "RecvChan" : "Chan");
  var typ = elem[field];
  if (typ === undefined) {
    typ = $newType(4, "Chan", string, "", "", null);
    typ.init(elem, sendOnly, recvOnly);
    elem[field] = typ;
  }
  return typ;
};

var $funcSig = function(params, results, variadic) {
  var paramTypes = $mapArray(params, function(p) { return p.string; });
  if (variadic) {
    paramTypes[paramTypes.length - 1] = "..." + paramTypes[paramTypes.length - 1].substr(2);
  }
  var string = "(" + paramTypes.join(", ") + ")";
  if (results.length === 1) {
    string += " " + results[0].string;
  } else if (results.length > 1) {
    string += " (" + $mapArray(results, function(r) { return r.string; }).join(", ") + ")";
  }
  return string;
};

var $funcTypes = {};
var $funcType = function(params, results, variadic) {
  var string = "func" + $funcSig(params, results, variadic);
  var typ = $funcTypes[string];
  if (typ === undefined) {
    typ = $newType(4, "Func", string, "", "", null);
    typ.init(params, results, variadic);
    $funcTypes[string] = typ;
  }
  return typ;
};

var $interfaceTypes = {};
var $interfaceType = function(methods) {
  var string = "interface {}";
  if (methods.length !== 0) {
    string = "interface { " + $mapArray(methods, function(m) {
      return (m[2] !== "" ? m[2] + "." : "") + m[1] + $funcSig(m[3], m[4], m[5]);
    }).join("; ") + " }";
  }
  var typ = $interfaceTypes[string];
  if (typ === undefined) {
    typ = $newType(8, "Interface", string, "", "", null);
    typ.init(methods);
    $interfaceTypes[string] = typ;
  }
  return typ;
};
var $emptyInterface = $interfaceType([]);
var $interfaceNil = { $key: function() { return "nil"; } };
var $error = $newType(8, "Interface", "error", "error", "", null);
$error.init([["Error", "Error", "", [], [$String], false]]);

var $Map = function() {};
(function() {
  var names = Object.getOwnPropertyNames(Object.prototype), i;
  for (i = 0; i < names.length; i++) {
    $Map.prototype[names[i]] = undefined;
  }
})();
var $mapTypes = {};
var $mapType = function(key, elem) {
  var string = "map[" + key.string + "]" + elem.string;
  var typ = $mapTypes[string];
  if (typ === undefined) {
    typ = $newType(4, "Map", string, "", "", null);
    typ.init(key, elem);
    $mapTypes[string] = typ;
  }
  return typ;
};


var $throwNilPointerError = function() { $throwRuntimeError("invalid memory address or nil pointer dereference"); };
var $ptrType = function(elem) {
  var typ = elem.Ptr;
  if (typ === undefined) {
    typ = $newType(4, "Ptr", "*" + elem.string, "", "", null);
    typ.init(elem);
    elem.Ptr = typ;
  }
  return typ;
};

var $stringPtrMap = new $Map();
var $newStringPtr = function(str) {
  if (str === undefined || str === "") {
    return $ptrType($String).nil;
  }
  var ptr = $stringPtrMap[str];
  if (ptr === undefined) {
    ptr = new ($ptrType($String))(function() { return str; }, function(v) { str = v; });
    $stringPtrMap[str] = ptr;
  }
  return ptr;
};

var $newDataPointer = function(data, constructor) {
  if (constructor.Struct) {
    return data;
  }
  return new constructor(function() { return data; }, function(v) { data = v; });
};

var $sliceType = function(elem) {
  var typ = elem.Slice;
  if (typ === undefined) {
    typ = $newType(12, "Slice", "[]" + elem.string, "", "", null);
    typ.init(elem);
    elem.Slice = typ;
  }
  return typ;
};

var $structTypes = {};
var $structType = function(fields) {
  var string = "struct { " + $mapArray(fields, function(f) {
    return f[1] + " " + f[3].string + (f[4] !== "" ? (" \"" + f[4].replace(/\\/g, "\\\\").replace(/"/g, "\\\"") + "\"") : "");
  }).join("; ") + " }";
  if (fields.length === 0) {
    string = "struct {}";
  }
  var typ = $structTypes[string];
  if (typ === undefined) {
    typ = $newType(0, "Struct", string, "", "", function() {
      this.$val = this;
      var i;
      for (i = 0; i < fields.length; i++) {
        var field = fields[i];
        var arg = arguments[i];
        this[field[0]] = arg !== undefined ? arg : field[3].zero();
      }
    });
    /* collect methods for anonymous fields */
    var i, j;
    for (i = 0; i < fields.length; i++) {
      var field = fields[i];
      if (field[1] === "") {
        var methods = field[3].methods;
        for (j = 0; j < methods.length; j++) {
          var m = methods[j].slice(0, 6).concat([i]);
          typ.methods.push(m);
          typ.Ptr.methods.push(m);
        }
        if (field[3].kind === "Struct") {
          var methods = field[3].Ptr.methods;
          for (j = 0; j < methods.length; j++) {
            typ.Ptr.methods.push(methods[j].slice(0, 6).concat([i]));
          }
        }
      }
    }
    typ.init(fields);
    $structTypes[string] = typ;
  }
  return typ;
};

var $coerceFloat32 = function(f) {
  var math = $packages["math"];
  if (math === undefined) {
    return f;
  }
  return math.Float32frombits(math.Float32bits(f));
};

var $floatKey = function(f) {
  if (f !== f) {
    $idCounter++;
    return "NaN$" + $idCounter;
  }
  return String(f);
};

var $flatten64 = function(x) {
  return x.$high * 4294967296 + x.$low;
};

var $shiftLeft64 = function(x, y) {
  if (y === 0) {
    return x;
  }
  if (y < 32) {
    return new x.constructor(x.$high << y | x.$low >>> (32 - y), (x.$low << y) >>> 0);
  }
  if (y < 64) {
    return new x.constructor(x.$low << (y - 32), 0);
  }
  return new x.constructor(0, 0);
};

var $shiftRightInt64 = function(x, y) {
  if (y === 0) {
    return x;
  }
  if (y < 32) {
    return new x.constructor(x.$high >> y, (x.$low >>> y | x.$high << (32 - y)) >>> 0);
  }
  if (y < 64) {
    return new x.constructor(x.$high >> 31, (x.$high >> (y - 32)) >>> 0);
  }
  if (x.$high < 0) {
    return new x.constructor(-1, 4294967295);
  }
  return new x.constructor(0, 0);
};

var $shiftRightUint64 = function(x, y) {
  if (y === 0) {
    return x;
  }
  if (y < 32) {
    return new x.constructor(x.$high >>> y, (x.$low >>> y | x.$high << (32 - y)) >>> 0);
  }
  if (y < 64) {
    return new x.constructor(0, x.$high >>> (y - 32));
  }
  return new x.constructor(0, 0);
};

var $mul64 = function(x, y) {
  var high = 0, low = 0, i;
  if ((y.$low & 1) !== 0) {
    high = x.$high;
    low = x.$low;
  }
  for (i = 1; i < 32; i++) {
    if ((y.$low & 1<<i) !== 0) {
      high += x.$high << i | x.$low >>> (32 - i);
      low += (x.$low << i) >>> 0;
    }
  }
  for (i = 0; i < 32; i++) {
    if ((y.$high & 1<<i) !== 0) {
      high += x.$low << i;
    }
  }
  return new x.constructor(high, low);
};

var $div64 = function(x, y, returnRemainder) {
  if (y.$high === 0 && y.$low === 0) {
    $throwRuntimeError("integer divide by zero");
  }

  var s = 1;
  var rs = 1;

  var xHigh = x.$high;
  var xLow = x.$low;
  if (xHigh < 0) {
    s = -1;
    rs = -1;
    xHigh = -xHigh;
    if (xLow !== 0) {
      xHigh--;
      xLow = 4294967296 - xLow;
    }
  }

  var yHigh = y.$high;
  var yLow = y.$low;
  if (y.$high < 0) {
    s *= -1;
    yHigh = -yHigh;
    if (yLow !== 0) {
      yHigh--;
      yLow = 4294967296 - yLow;
    }
  }

  var high = 0, low = 0, n = 0, i;
  while (yHigh < 2147483648 && ((xHigh > yHigh) || (xHigh === yHigh && xLow > yLow))) {
    yHigh = (yHigh << 1 | yLow >>> 31) >>> 0;
    yLow = (yLow << 1) >>> 0;
    n++;
  }
  for (i = 0; i <= n; i++) {
    high = high << 1 | low >>> 31;
    low = (low << 1) >>> 0;
    if ((xHigh > yHigh) || (xHigh === yHigh && xLow >= yLow)) {
      xHigh = xHigh - yHigh;
      xLow = xLow - yLow;
      if (xLow < 0) {
        xHigh--;
        xLow += 4294967296;
      }
      low++;
      if (low === 4294967296) {
        high++;
        low = 0;
      }
    }
    yLow = (yLow >>> 1 | yHigh << (32 - 1)) >>> 0;
    yHigh = yHigh >>> 1;
  }

  if (returnRemainder) {
    return new x.constructor(xHigh * rs, xLow * rs);
  }
  return new x.constructor(high * s, low * s);
};

var $divComplex = function(n, d) {
  var ninf = n.$real === 1/0 || n.$real === -1/0 || n.$imag === 1/0 || n.$imag === -1/0;
  var dinf = d.$real === 1/0 || d.$real === -1/0 || d.$imag === 1/0 || d.$imag === -1/0;
  var nnan = !ninf && (n.$real !== n.$real || n.$imag !== n.$imag);
  var dnan = !dinf && (d.$real !== d.$real || d.$imag !== d.$imag);
  if(nnan || dnan) {
    return new n.constructor(0/0, 0/0);
  }
  if (ninf && !dinf) {
    return new n.constructor(1/0, 1/0);
  }
  if (!ninf && dinf) {
    return new n.constructor(0, 0);
  }
  if (d.$real === 0 && d.$imag === 0) {
    if (n.$real === 0 && n.$imag === 0) {
      return new n.constructor(0/0, 0/0);
    }
    return new n.constructor(1/0, 1/0);
  }
  var a = Math.abs(d.$real);
  var b = Math.abs(d.$imag);
  if (a <= b) {
    var ratio = d.$real / d.$imag;
    var denom = d.$real * ratio + d.$imag;
    return new n.constructor((n.$real * ratio + n.$imag) / denom, (n.$imag * ratio - n.$real) / denom);
  }
  var ratio = d.$imag / d.$real;
  var denom = d.$imag * ratio + d.$real;
  return new n.constructor((n.$imag * ratio + n.$real) / denom, (n.$imag - n.$real * ratio) / denom);
};

var $getStack = function() {
  return (new Error()).stack.split("\n");
};
var $stackDepthOffset = 0;
var $getStackDepth = function() {
  return $stackDepthOffset + $getStack().length;
};

var $deferFrames = [], $skippedDeferFrames = 0, $jumpToDefer = false, $panicStackDepth = null, $panicValue;
var $callDeferred = function(deferred, jsErr) {
  if ($skippedDeferFrames !== 0) {
    $skippedDeferFrames--;
    throw jsErr;
  }
  if ($jumpToDefer) {
    $jumpToDefer = false;
    throw jsErr;
  }

  $stackDepthOffset--;
  var outerPanicStackDepth = $panicStackDepth;
  var outerPanicValue = $panicValue;

  var localPanicValue = $curGoroutine.panicStack.pop();
  if (jsErr) {
    localPanicValue = new $packages["github.com/gopherjs/gopherjs/js"].Error.Ptr(jsErr);
  }
  if (localPanicValue !== undefined) {
    $panicStackDepth = $getStackDepth();
    $panicValue = localPanicValue;
  }

  var call;
  try {
    while (true) {
      if (deferred === null) {
        deferred = $deferFrames[$deferFrames.length - 1 - $skippedDeferFrames];
        if (deferred === undefined) {
          if (localPanicValue.constructor === $String) {
            throw new Error(localPanicValue.$val);
          } else if (localPanicValue.Error !== undefined) {
            throw new Error(localPanicValue.Error());
          } else if (localPanicValue.String !== undefined) {
            throw new Error(localPanicValue.String());
          } else {
            throw new Error(localPanicValue);
          }
        }
      }
      var call = deferred.pop();
      if (call === undefined) {
        if (localPanicValue !== undefined) {
          $skippedDeferFrames++;
          deferred = null;
          continue;
        }
        return;
      }
      var r = call[0].apply(undefined, call[1]);
      if (r && r.constructor === Function) {
        deferred.push([r, []]);
      }

      if (localPanicValue !== undefined && $panicStackDepth === null) {
        throw null; /* error was recovered */
      }
    }
  } finally {
    if ($curGoroutine.asleep) {
      deferred.push(call);
      $jumpToDefer = true;
    }
    if (localPanicValue !== undefined) {
      if ($panicStackDepth !== null) {
        $curGoroutine.panicStack.push(localPanicValue);
      }
      $panicStackDepth = outerPanicStackDepth;
      $panicValue = outerPanicValue;
    }
    $stackDepthOffset++;
  }
};

var $panic = function(value) {
  $curGoroutine.panicStack.push(value);
  $callDeferred(null, null);
};
var $recover = function() {
  if ($panicStackDepth === null || $panicStackDepth !== $getStackDepth() - 2) {
    return null;
  }
  $panicStackDepth = null;
  return $panicValue;
};
var $nonblockingCall = function() {
  $panic(new $packages["runtime"].NotSupportedError.Ptr("non-blocking call to blocking function (mark call with \"//gopherjs:blocking\" to fix)"));
};
var $throw = function(err) { throw err; };
var $throwRuntimeError; /* set by package "runtime" */

var $dummyGoroutine = { asleep: false, exit: false, panicStack: [] };
var $curGoroutine = $dummyGoroutine, $totalGoroutines = 0, $awakeGoroutines = 0, $checkForDeadlock = true;
var $go = function(fun, args, direct) {
  $totalGoroutines++;
  $awakeGoroutines++;
  args.push(true);
  var goroutine = function() {
    try {
      $curGoroutine = goroutine;
      $skippedDeferFrames = 0;
      $jumpToDefer = false;
      var r = fun.apply(undefined, args);
      if (r !== undefined) {
        fun = r;
        args = [];
        $schedule(goroutine, direct);
        return;
      }
      goroutine.exit = true;
    } catch (err) {
      if (!$curGoroutine.asleep) {
        goroutine.exit = true;
        throw err;
      }
    } finally {
      $curGoroutine = $dummyGoroutine;
      if (goroutine.exit) { /* also set by runtime.Goexit() */
        $totalGoroutines--;
        goroutine.asleep = true;
      }
      if (goroutine.asleep) {
        $awakeGoroutines--;
        if ($awakeGoroutines === 0 && $totalGoroutines !== 0 && $checkForDeadlock) {
          $panic(new $String("fatal error: all goroutines are asleep - deadlock!"));
        }
      }
    }
  };
  goroutine.asleep = false;
  goroutine.exit = false;
  goroutine.panicStack = [];
  $schedule(goroutine, direct);
};

var $scheduled = [], $schedulerLoopActive = false;
var $schedule = function(goroutine, direct) {
  if (goroutine.asleep) {
    goroutine.asleep = false;
    $awakeGoroutines++;
  }

  if (direct) {
    goroutine();
    return;
  }

  $scheduled.push(goroutine);
  if (!$schedulerLoopActive) {
    $schedulerLoopActive = true;
    setTimeout(function() {
      while (true) {
        var r = $scheduled.shift();
        if (r === undefined) {
          $schedulerLoopActive = false;
          break;
        }
        r();
      };
    }, 0);
  }
};

var $send = function(chan, value) {
  if (chan.$closed) {
    $throwRuntimeError("send on closed channel");
  }
  var queuedRecv = chan.$recvQueue.shift();
  if (queuedRecv !== undefined) {
    queuedRecv.chanValue = [value, true];
    $schedule(queuedRecv);
    return;
  }
  if (chan.$buffer.length < chan.$capacity) {
    chan.$buffer.push(value);
    return;
  }

  chan.$sendQueue.push([$curGoroutine, value]);
  var blocked = false;
  return function() {
    if (blocked) {
      if (chan.$closed) {
        $throwRuntimeError("send on closed channel");
      }
      return;
    };
    blocked = true;
    $curGoroutine.asleep = true;
    throw null;
  };
};
var $recv = function(chan) {
  var queuedSend = chan.$sendQueue.shift();
  if (queuedSend !== undefined) {
    $schedule(queuedSend[0]);
    chan.$buffer.push(queuedSend[1]);
  }
  var bufferedValue = chan.$buffer.shift();
  if (bufferedValue !== undefined) {
    return [bufferedValue, true];
  }
  if (chan.$closed) {
    return [chan.constructor.elem.zero(), false];
  }

  chan.$recvQueue.push($curGoroutine);
  var blocked = false;
  return function() {
    if (blocked) {
      var value = $curGoroutine.chanValue;
      $curGoroutine.chanValue = undefined;
      return value;
    };
    blocked = true;
    $curGoroutine.asleep = true;
    throw null;
  };
};
var $close = function(chan) {
  if (chan.$closed) {
    $throwRuntimeError("close of closed channel");
  }
  chan.$closed = true;
  while (true) {
    var queuedSend = chan.$sendQueue.shift();
    if (queuedSend === undefined) {
      break;
    }
    $schedule(queuedSend[0]); /* will panic because of closed channel */
  }
  while (true) {
    var queuedRecv = chan.$recvQueue.shift();
    if (queuedRecv === undefined) {
      break;
    }
    queuedRecv.chanValue = [chan.constructor.elem.zero(), false];
    $schedule(queuedRecv);
  }
};
var $select = function(comms) {
  var ready = [], i;
  var selection = -1;
  for (i = 0; i < comms.length; i++) {
    var comm = comms[i];
    var chan = comm[0];
    switch (comm.length) {
    case 0: /* default */
      selection = i;
      break;
    case 1: /* recv */
      if (chan.$sendQueue.length !== 0 || chan.$buffer.length !== 0 || chan.$closed) {
        ready.push(i);
      }
      break;
    case 2: /* send */
      if (chan.$closed) {
        $throwRuntimeError("send on closed channel");
      }
      if (chan.$recvQueue.length !== 0 || chan.$buffer.length < chan.$capacity) {
        ready.push(i);
      }
      break;
    }
  }

  if (ready.length !== 0) {
    selection = ready[Math.floor(Math.random() * ready.length)];
  }
  if (selection !== -1) {
    var comm = comms[selection];
    switch (comm.length) {
    case 0: /* default */
      return [selection];
    case 1: /* recv */
      return [selection, $recv(comm[0])];
    case 2: /* send */
      $send(comm[0], comm[1]);
      return [selection];
    }
  }

  for (i = 0; i < comms.length; i++) {
    var comm = comms[i];
    switch (comm.length) {
    case 1: /* recv */
      comm[0].$recvQueue.push($curGoroutine);
      break;
    case 2: /* send */
      var queueEntry = [$curGoroutine, comm[1]];
      comm.push(queueEntry);
      comm[0].$sendQueue.push(queueEntry);
      break;
    }
  }
  var blocked = false;
  return function() {
    if (blocked) {
      var selection;
      for (i = 0; i < comms.length; i++) {
        var comm = comms[i];
        switch (comm.length) {
        case 1: /* recv */
          var queue = comm[0].$recvQueue;
          var index = queue.indexOf($curGoroutine);
          if (index !== -1) {
            queue.splice(index, 1);
            break;
          }
          var value = $curGoroutine.chanValue;
          $curGoroutine.chanValue = undefined;
          selection = [i, value];
          break;
        case 3: /* send */
          var queue = comm[0].$sendQueue;
          var index = queue.indexOf(comm[2]);
          if (index !== -1) {
            queue.splice(index, 1);
            break;
          }
          if (comm[0].$closed) {
            $throwRuntimeError("send on closed channel");
          }
          selection = [i];
          break;
        }
      }
      return selection;
    };
    blocked = true;
    $curGoroutine.asleep = true;
    throw null;
  };
};

var $needsExternalization = function(t) {
  switch (t.kind) {
    case "Bool":
    case "Int":
    case "Int8":
    case "Int16":
    case "Int32":
    case "Uint":
    case "Uint8":
    case "Uint16":
    case "Uint32":
    case "Uintptr":
    case "Float32":
    case "Float64":
      return false;
    case "Interface":
      return t !== $packages["github.com/gopherjs/gopherjs/js"].Object;
    default:
      return true;
  }
};

var $externalize = function(v, t) {
  switch (t.kind) {
  case "Bool":
  case "Int":
  case "Int8":
  case "Int16":
  case "Int32":
  case "Uint":
  case "Uint8":
  case "Uint16":
  case "Uint32":
  case "Uintptr":
  case "Float32":
  case "Float64":
    return v;
  case "Int64":
  case "Uint64":
    return $flatten64(v);
  case "Array":
    if ($needsExternalization(t.elem)) {
      return $mapArray(v, function(e) { return $externalize(e, t.elem); });
    }
    return v;
  case "Func":
    if (v === $throwNilPointerError) {
      return null;
    }
    if (v.$externalizeWrapper === undefined) {
      $checkForDeadlock = false;
      var convert = false;
      var i;
      for (i = 0; i < t.params.length; i++) {
        convert = convert || (t.params[i] !== $packages["github.com/gopherjs/gopherjs/js"].Object);
      }
      for (i = 0; i < t.results.length; i++) {
        convert = convert || $needsExternalization(t.results[i]);
      }
      if (!convert) {
        return v;
      }
      v.$externalizeWrapper = function() {
        var args = [], i;
        for (i = 0; i < t.params.length; i++) {
          if (t.variadic && i === t.params.length - 1) {
            var vt = t.params[i].elem, varargs = [], j;
            for (j = i; j < arguments.length; j++) {
              varargs.push($internalize(arguments[j], vt));
            }
            args.push(new (t.params[i])(varargs));
            break;
          }
          args.push($internalize(arguments[i], t.params[i]));
        }
        var result = v.apply(this, args);
        switch (t.results.length) {
        case 0:
          return;
        case 1:
          return $externalize(result, t.results[0]);
        default:
          for (i = 0; i < t.results.length; i++) {
            result[i] = $externalize(result[i], t.results[i]);
          }
          return result;
        }
      };
    }
    return v.$externalizeWrapper;
  case "Interface":
    if (v === null) {
      return null;
    }
    if (t === $packages["github.com/gopherjs/gopherjs/js"].Object || v.constructor.kind === undefined) {
      return v;
    }
    return $externalize(v.$val, v.constructor);
  case "Map":
    var m = {};
    var keys = $keys(v), i;
    for (i = 0; i < keys.length; i++) {
      var entry = v[keys[i]];
      m[$externalize(entry.k, t.key)] = $externalize(entry.v, t.elem);
    }
    return m;
  case "Ptr":
    var o = {}, i;
    for (i = 0; i < t.methods.length; i++) {
      var m = t.methods[i];
      if (m[2] !== "") { /* not exported */
        continue;
      }
      (function(m) {
        o[m[1]] = $externalize(function() {
          return v[m[0]].apply(v, arguments);
        }, $funcType(m[3], m[4], m[5]));
      })(m);
    }
    return o;
  case "Slice":
    if ($needsExternalization(t.elem)) {
      return $mapArray($sliceToArray(v), function(e) { return $externalize(e, t.elem); });
    }
    return $sliceToArray(v);
  case "String":
    var s = "", r, i, j = 0;
    for (i = 0; i < v.length; i += r[1], j++) {
      r = $decodeRune(v, i);
      s += String.fromCharCode(r[0]);
    }
    return s;
  case "Struct":
    var timePkg = $packages["time"];
    if (timePkg && v.constructor === timePkg.Time.Ptr) {
      var milli = $div64(v.UnixNano(), new $Int64(0, 1000000));
      return new Date($flatten64(milli));
    }
    var o = {}, i;
    for (i = 0; i < t.fields.length; i++) {
      var f = t.fields[i];
      if (f[2] !== "") { /* not exported */
        continue;
      }
      o[f[1]] = $externalize(v[f[0]], f[3]);
    }
    return o;
  }
  $panic(new $String("cannot externalize " + t.string));
};

var $internalize = function(v, t, recv) {
  switch (t.kind) {
  case "Bool":
    return !!v;
  case "Int":
    return parseInt(v);
  case "Int8":
    return parseInt(v) << 24 >> 24;
  case "Int16":
    return parseInt(v) << 16 >> 16;
  case "Int32":
    return parseInt(v) >> 0;
  case "Uint":
    return parseInt(v);
  case "Uint8":
    return parseInt(v) << 24 >>> 24;
  case "Uint16":
    return parseInt(v) << 16 >>> 16;
  case "Uint32":
  case "Uintptr":
    return parseInt(v) >>> 0;
  case "Int64":
  case "Uint64":
    return new t(0, v);
  case "Float32":
  case "Float64":
    return parseFloat(v);
  case "Array":
    if (v.length !== t.len) {
      $throwRuntimeError("got array with wrong size from JavaScript native");
    }
    return $mapArray(v, function(e) { return $internalize(e, t.elem); });
  case "Func":
    return function() {
      var args = [], i;
      for (i = 0; i < t.params.length; i++) {
        if (t.variadic && i === t.params.length - 1) {
          var vt = t.params[i].elem, varargs = arguments[i], j;
          for (j = 0; j < varargs.$length; j++) {
            args.push($externalize(varargs.$array[varargs.$offset + j], vt));
          }
          break;
        }
        args.push($externalize(arguments[i], t.params[i]));
      }
      var result = v.apply(recv, args);
      switch (t.results.length) {
      case 0:
        return;
      case 1:
        return $internalize(result, t.results[0]);
      default:
        for (i = 0; i < t.results.length; i++) {
          result[i] = $internalize(result[i], t.results[i]);
        }
        return result;
      }
    };
  case "Interface":
    if (v === null || t === $packages["github.com/gopherjs/gopherjs/js"].Object) {
      return v;
    }
    switch (v.constructor) {
    case Int8Array:
      return new ($sliceType($Int8))(v);
    case Int16Array:
      return new ($sliceType($Int16))(v);
    case Int32Array:
      return new ($sliceType($Int))(v);
    case Uint8Array:
      return new ($sliceType($Uint8))(v);
    case Uint16Array:
      return new ($sliceType($Uint16))(v);
    case Uint32Array:
      return new ($sliceType($Uint))(v);
    case Float32Array:
      return new ($sliceType($Float32))(v);
    case Float64Array:
      return new ($sliceType($Float64))(v);
    case Array:
      return $internalize(v, $sliceType($emptyInterface));
    case Boolean:
      return new $Bool(!!v);
    case Date:
      var timePkg = $packages["time"];
      if (timePkg) {
        return new timePkg.Time(timePkg.Unix(new $Int64(0, 0), new $Int64(0, v.getTime() * 1000000)));
      }
    case Function:
      var funcType = $funcType([$sliceType($emptyInterface)], [$packages["github.com/gopherjs/gopherjs/js"].Object], true);
      return new funcType($internalize(v, funcType));
    case Number:
      return new $Float64(parseFloat(v));
    case String:
      return new $String($internalize(v, $String));
    default:
      var mapType = $mapType($String, $emptyInterface);
      return new mapType($internalize(v, mapType));
    }
  case "Map":
    var m = new $Map();
    var keys = $keys(v), i;
    for (i = 0; i < keys.length; i++) {
      var key = $internalize(keys[i], t.key);
      m[key.$key ? key.$key() : key] = { k: key, v: $internalize(v[keys[i]], t.elem) };
    }
    return m;
  case "Slice":
    return new t($mapArray(v, function(e) { return $internalize(e, t.elem); }));
  case "String":
    v = String(v);
    var s = "", i;
    for (i = 0; i < v.length; i++) {
      s += $encodeRune(v.charCodeAt(i));
    }
    return s;
  default:
    $panic(new $String("cannot internalize " + t.string));
  }
};

$packages["github.com/gopherjs/gopherjs/js"] = (function() {
	var $pkg = {}, Object, Error, init;
	Object = $pkg.Object = $newType(8, "Interface", "js.Object", "Object", "github.com/gopherjs/gopherjs/js", null);
	Error = $pkg.Error = $newType(0, "Struct", "js.Error", "Error", "github.com/gopherjs/gopherjs/js", function(Object_) {
		this.$val = this;
		this.Object = Object_ !== undefined ? Object_ : null;
	});
	Error.Ptr.prototype.Error = function() {
		var err;
		err = this;
		return "JavaScript error: " + $internalize(err.Object.message, $String);
	};
	Error.prototype.Error = function() { return this.$val.Error(); };
	init = function() {
		var e;
		e = new Error.Ptr(null);
	};
	$pkg.$init = function() {
		Object.init([["Bool", "Bool", "", [], [$Bool], false], ["Call", "Call", "", [$String, ($sliceType($emptyInterface))], [Object], true], ["Delete", "Delete", "", [$String], [], false], ["Float", "Float", "", [], [$Float64], false], ["Get", "Get", "", [$String], [Object], false], ["Index", "Index", "", [$Int], [Object], false], ["Int", "Int", "", [], [$Int], false], ["Int64", "Int64", "", [], [$Int64], false], ["Interface", "Interface", "", [], [$emptyInterface], false], ["Invoke", "Invoke", "", [($sliceType($emptyInterface))], [Object], true], ["IsNull", "IsNull", "", [], [$Bool], false], ["IsUndefined", "IsUndefined", "", [], [$Bool], false], ["Length", "Length", "", [], [$Int], false], ["New", "New", "", [($sliceType($emptyInterface))], [Object], true], ["Set", "Set", "", [$String, $emptyInterface], [], false], ["SetIndex", "SetIndex", "", [$Int, $emptyInterface], [], false], ["Str", "Str", "", [], [$String], false], ["Uint64", "Uint64", "", [], [$Uint64], false], ["Unsafe", "Unsafe", "", [], [$Uintptr], false]]);
		Error.methods = [["Bool", "Bool", "", [], [$Bool], false, 0], ["Call", "Call", "", [$String, ($sliceType($emptyInterface))], [Object], true, 0], ["Delete", "Delete", "", [$String], [], false, 0], ["Float", "Float", "", [], [$Float64], false, 0], ["Get", "Get", "", [$String], [Object], false, 0], ["Index", "Index", "", [$Int], [Object], false, 0], ["Int", "Int", "", [], [$Int], false, 0], ["Int64", "Int64", "", [], [$Int64], false, 0], ["Interface", "Interface", "", [], [$emptyInterface], false, 0], ["Invoke", "Invoke", "", [($sliceType($emptyInterface))], [Object], true, 0], ["IsNull", "IsNull", "", [], [$Bool], false, 0], ["IsUndefined", "IsUndefined", "", [], [$Bool], false, 0], ["Length", "Length", "", [], [$Int], false, 0], ["New", "New", "", [($sliceType($emptyInterface))], [Object], true, 0], ["Set", "Set", "", [$String, $emptyInterface], [], false, 0], ["SetIndex", "SetIndex", "", [$Int, $emptyInterface], [], false, 0], ["Str", "Str", "", [], [$String], false, 0], ["Uint64", "Uint64", "", [], [$Uint64], false, 0], ["Unsafe", "Unsafe", "", [], [$Uintptr], false, 0]];
		($ptrType(Error)).methods = [["Bool", "Bool", "", [], [$Bool], false, 0], ["Call", "Call", "", [$String, ($sliceType($emptyInterface))], [Object], true, 0], ["Delete", "Delete", "", [$String], [], false, 0], ["Error", "Error", "", [], [$String], false, -1], ["Float", "Float", "", [], [$Float64], false, 0], ["Get", "Get", "", [$String], [Object], false, 0], ["Index", "Index", "", [$Int], [Object], false, 0], ["Int", "Int", "", [], [$Int], false, 0], ["Int64", "Int64", "", [], [$Int64], false, 0], ["Interface", "Interface", "", [], [$emptyInterface], false, 0], ["Invoke", "Invoke", "", [($sliceType($emptyInterface))], [Object], true, 0], ["IsNull", "IsNull", "", [], [$Bool], false, 0], ["IsUndefined", "IsUndefined", "", [], [$Bool], false, 0], ["Length", "Length", "", [], [$Int], false, 0], ["New", "New", "", [($sliceType($emptyInterface))], [Object], true, 0], ["Set", "Set", "", [$String, $emptyInterface], [], false, 0], ["SetIndex", "SetIndex", "", [$Int, $emptyInterface], [], false, 0], ["Str", "Str", "", [], [$String], false, 0], ["Uint64", "Uint64", "", [], [$Uint64], false, 0], ["Unsafe", "Unsafe", "", [], [$Uintptr], false, 0]];
		Error.init([["Object", "", "", Object, ""]]);
		init();
	};
	return $pkg;
})();
$packages["runtime"] = (function() {
	var $pkg = {}, js = $packages["github.com/gopherjs/gopherjs/js"], NotSupportedError, TypeAssertionError, errorString, MemStats, sizeof_C_MStats, init, init$1;
	NotSupportedError = $pkg.NotSupportedError = $newType(0, "Struct", "runtime.NotSupportedError", "NotSupportedError", "runtime", function(Feature_) {
		this.$val = this;
		this.Feature = Feature_ !== undefined ? Feature_ : "";
	});
	TypeAssertionError = $pkg.TypeAssertionError = $newType(0, "Struct", "runtime.TypeAssertionError", "TypeAssertionError", "runtime", function(interfaceString_, concreteString_, assertedString_, missingMethod_) {
		this.$val = this;
		this.interfaceString = interfaceString_ !== undefined ? interfaceString_ : "";
		this.concreteString = concreteString_ !== undefined ? concreteString_ : "";
		this.assertedString = assertedString_ !== undefined ? assertedString_ : "";
		this.missingMethod = missingMethod_ !== undefined ? missingMethod_ : "";
	});
	errorString = $pkg.errorString = $newType(8, "String", "runtime.errorString", "errorString", "runtime", null);
	MemStats = $pkg.MemStats = $newType(0, "Struct", "runtime.MemStats", "MemStats", "runtime", function(Alloc_, TotalAlloc_, Sys_, Lookups_, Mallocs_, Frees_, HeapAlloc_, HeapSys_, HeapIdle_, HeapInuse_, HeapReleased_, HeapObjects_, StackInuse_, StackSys_, MSpanInuse_, MSpanSys_, MCacheInuse_, MCacheSys_, BuckHashSys_, GCSys_, OtherSys_, NextGC_, LastGC_, PauseTotalNs_, PauseNs_, NumGC_, EnableGC_, DebugGC_, BySize_) {
		this.$val = this;
		this.Alloc = Alloc_ !== undefined ? Alloc_ : new $Uint64(0, 0);
		this.TotalAlloc = TotalAlloc_ !== undefined ? TotalAlloc_ : new $Uint64(0, 0);
		this.Sys = Sys_ !== undefined ? Sys_ : new $Uint64(0, 0);
		this.Lookups = Lookups_ !== undefined ? Lookups_ : new $Uint64(0, 0);
		this.Mallocs = Mallocs_ !== undefined ? Mallocs_ : new $Uint64(0, 0);
		this.Frees = Frees_ !== undefined ? Frees_ : new $Uint64(0, 0);
		this.HeapAlloc = HeapAlloc_ !== undefined ? HeapAlloc_ : new $Uint64(0, 0);
		this.HeapSys = HeapSys_ !== undefined ? HeapSys_ : new $Uint64(0, 0);
		this.HeapIdle = HeapIdle_ !== undefined ? HeapIdle_ : new $Uint64(0, 0);
		this.HeapInuse = HeapInuse_ !== undefined ? HeapInuse_ : new $Uint64(0, 0);
		this.HeapReleased = HeapReleased_ !== undefined ? HeapReleased_ : new $Uint64(0, 0);
		this.HeapObjects = HeapObjects_ !== undefined ? HeapObjects_ : new $Uint64(0, 0);
		this.StackInuse = StackInuse_ !== undefined ? StackInuse_ : new $Uint64(0, 0);
		this.StackSys = StackSys_ !== undefined ? StackSys_ : new $Uint64(0, 0);
		this.MSpanInuse = MSpanInuse_ !== undefined ? MSpanInuse_ : new $Uint64(0, 0);
		this.MSpanSys = MSpanSys_ !== undefined ? MSpanSys_ : new $Uint64(0, 0);
		this.MCacheInuse = MCacheInuse_ !== undefined ? MCacheInuse_ : new $Uint64(0, 0);
		this.MCacheSys = MCacheSys_ !== undefined ? MCacheSys_ : new $Uint64(0, 0);
		this.BuckHashSys = BuckHashSys_ !== undefined ? BuckHashSys_ : new $Uint64(0, 0);
		this.GCSys = GCSys_ !== undefined ? GCSys_ : new $Uint64(0, 0);
		this.OtherSys = OtherSys_ !== undefined ? OtherSys_ : new $Uint64(0, 0);
		this.NextGC = NextGC_ !== undefined ? NextGC_ : new $Uint64(0, 0);
		this.LastGC = LastGC_ !== undefined ? LastGC_ : new $Uint64(0, 0);
		this.PauseTotalNs = PauseTotalNs_ !== undefined ? PauseTotalNs_ : new $Uint64(0, 0);
		this.PauseNs = PauseNs_ !== undefined ? PauseNs_ : ($arrayType($Uint64, 256)).zero();
		this.NumGC = NumGC_ !== undefined ? NumGC_ : 0;
		this.EnableGC = EnableGC_ !== undefined ? EnableGC_ : false;
		this.DebugGC = DebugGC_ !== undefined ? DebugGC_ : false;
		this.BySize = BySize_ !== undefined ? BySize_ : ($arrayType(($structType([["Size", "Size", "", $Uint32, ""], ["Mallocs", "Mallocs", "", $Uint64, ""], ["Frees", "Frees", "", $Uint64, ""]])), 61)).zero();
	});
	NotSupportedError.Ptr.prototype.Error = function() {
		var err;
		err = this;
		return "not supported by GopherJS: " + err.Feature;
	};
	NotSupportedError.prototype.Error = function() { return this.$val.Error(); };
	init = function() {
		var e;
		$throwRuntimeError = $externalize((function(msg) {
			$panic(new errorString(msg));
		}), ($funcType([$String], [], false)));
		e = null;
		e = new TypeAssertionError.Ptr("", "", "", "");
		e = new NotSupportedError.Ptr("");
	};
	TypeAssertionError.Ptr.prototype.RuntimeError = function() {
	};
	TypeAssertionError.prototype.RuntimeError = function() { return this.$val.RuntimeError(); };
	TypeAssertionError.Ptr.prototype.Error = function() {
		var e, inter;
		e = this;
		inter = e.interfaceString;
		if (inter === "") {
			inter = "interface";
		}
		if (e.concreteString === "") {
			return "interface conversion: " + inter + " is nil, not " + e.assertedString;
		}
		if (e.missingMethod === "") {
			return "interface conversion: " + inter + " is " + e.concreteString + ", not " + e.assertedString;
		}
		return "interface conversion: " + e.concreteString + " is not " + e.assertedString + ": missing method " + e.missingMethod;
	};
	TypeAssertionError.prototype.Error = function() { return this.$val.Error(); };
	errorString.prototype.RuntimeError = function() {
		var e;
		e = this.$val !== undefined ? this.$val : this;
	};
	$ptrType(errorString).prototype.RuntimeError = function() { return new errorString(this.$get()).RuntimeError(); };
	errorString.prototype.Error = function() {
		var e;
		e = this.$val !== undefined ? this.$val : this;
		return "runtime error: " + e;
	};
	$ptrType(errorString).prototype.Error = function() { return new errorString(this.$get()).Error(); };
	init$1 = function() {
		var memStats;
		memStats = new MemStats.Ptr(); $copy(memStats, new MemStats.Ptr(), MemStats);
		if (!((sizeof_C_MStats === 3712))) {
			console.log(sizeof_C_MStats, 3712);
			$panic(new $String("MStats vs MemStatsType size mismatch"));
		}
	};
	$pkg.$init = function() {
		($ptrType(NotSupportedError)).methods = [["Error", "Error", "", [], [$String], false, -1]];
		NotSupportedError.init([["Feature", "Feature", "", $String, ""]]);
		($ptrType(TypeAssertionError)).methods = [["Error", "Error", "", [], [$String], false, -1], ["RuntimeError", "RuntimeError", "", [], [], false, -1]];
		TypeAssertionError.init([["interfaceString", "interfaceString", "runtime", $String, ""], ["concreteString", "concreteString", "runtime", $String, ""], ["assertedString", "assertedString", "runtime", $String, ""], ["missingMethod", "missingMethod", "runtime", $String, ""]]);
		errorString.methods = [["Error", "Error", "", [], [$String], false, -1], ["RuntimeError", "RuntimeError", "", [], [], false, -1]];
		($ptrType(errorString)).methods = [["Error", "Error", "", [], [$String], false, -1], ["RuntimeError", "RuntimeError", "", [], [], false, -1]];
		MemStats.init([["Alloc", "Alloc", "", $Uint64, ""], ["TotalAlloc", "TotalAlloc", "", $Uint64, ""], ["Sys", "Sys", "", $Uint64, ""], ["Lookups", "Lookups", "", $Uint64, ""], ["Mallocs", "Mallocs", "", $Uint64, ""], ["Frees", "Frees", "", $Uint64, ""], ["HeapAlloc", "HeapAlloc", "", $Uint64, ""], ["HeapSys", "HeapSys", "", $Uint64, ""], ["HeapIdle", "HeapIdle", "", $Uint64, ""], ["HeapInuse", "HeapInuse", "", $Uint64, ""], ["HeapReleased", "HeapReleased", "", $Uint64, ""], ["HeapObjects", "HeapObjects", "", $Uint64, ""], ["StackInuse", "StackInuse", "", $Uint64, ""], ["StackSys", "StackSys", "", $Uint64, ""], ["MSpanInuse", "MSpanInuse", "", $Uint64, ""], ["MSpanSys", "MSpanSys", "", $Uint64, ""], ["MCacheInuse", "MCacheInuse", "", $Uint64, ""], ["MCacheSys", "MCacheSys", "", $Uint64, ""], ["BuckHashSys", "BuckHashSys", "", $Uint64, ""], ["GCSys", "GCSys", "", $Uint64, ""], ["OtherSys", "OtherSys", "", $Uint64, ""], ["NextGC", "NextGC", "", $Uint64, ""], ["LastGC", "LastGC", "", $Uint64, ""], ["PauseTotalNs", "PauseTotalNs", "", $Uint64, ""], ["PauseNs", "PauseNs", "", ($arrayType($Uint64, 256)), ""], ["NumGC", "NumGC", "", $Uint32, ""], ["EnableGC", "EnableGC", "", $Bool, ""], ["DebugGC", "DebugGC", "", $Bool, ""], ["BySize", "BySize", "", ($arrayType(($structType([["Size", "Size", "", $Uint32, ""], ["Mallocs", "Mallocs", "", $Uint64, ""], ["Frees", "Frees", "", $Uint64, ""]])), 61)), ""]]);
		sizeof_C_MStats = 3712;
		init();
		init$1();
	};
	return $pkg;
})();
$packages["errors"] = (function() {
	var $pkg = {}, errorString, New;
	errorString = $pkg.errorString = $newType(0, "Struct", "errors.errorString", "errorString", "errors", function(s_) {
		this.$val = this;
		this.s = s_ !== undefined ? s_ : "";
	});
	New = $pkg.New = function(text) {
		return new errorString.Ptr(text);
	};
	errorString.Ptr.prototype.Error = function() {
		var e;
		e = this;
		return e.s;
	};
	errorString.prototype.Error = function() { return this.$val.Error(); };
	$pkg.$init = function() {
		($ptrType(errorString)).methods = [["Error", "Error", "", [], [$String], false, -1]];
		errorString.init([["s", "s", "errors", $String, ""]]);
	};
	return $pkg;
})();
$packages["github.com/gopherjs/webgl"] = (function() {
	var $pkg = {}, errors = $packages["errors"], js = $packages["github.com/gopherjs/gopherjs/js"], ContextAttributes, Context, DefaultAttributes, NewContext;
	ContextAttributes = $pkg.ContextAttributes = $newType(0, "Struct", "webgl.ContextAttributes", "ContextAttributes", "github.com/gopherjs/webgl", function(Alpha_, Depth_, Stencil_, Antialias_, PremultipliedAlpha_, PreserveDrawingBuffer_) {
		this.$val = this;
		this.Alpha = Alpha_ !== undefined ? Alpha_ : false;
		this.Depth = Depth_ !== undefined ? Depth_ : false;
		this.Stencil = Stencil_ !== undefined ? Stencil_ : false;
		this.Antialias = Antialias_ !== undefined ? Antialias_ : false;
		this.PremultipliedAlpha = PremultipliedAlpha_ !== undefined ? PremultipliedAlpha_ : false;
		this.PreserveDrawingBuffer = PreserveDrawingBuffer_ !== undefined ? PreserveDrawingBuffer_ : false;
	});
	Context = $pkg.Context = $newType(0, "Struct", "webgl.Context", "Context", "github.com/gopherjs/webgl", function(Object_, ARRAY_BUFFER_, ARRAY_BUFFER_BINDING_, ATTACHED_SHADERS_, BACK_, BLEND_, BLEND_COLOR_, BLEND_DST_ALPHA_, BLEND_DST_RGB_, BLEND_EQUATION_, BLEND_EQUATION_ALPHA_, BLEND_EQUATION_RGB_, BLEND_SRC_ALPHA_, BLEND_SRC_RGB_, BLUE_BITS_, BOOL_, BOOL_VEC2_, BOOL_VEC3_, BOOL_VEC4_, BROWSER_DEFAULT_WEBGL_, BUFFER_SIZE_, BUFFER_USAGE_, BYTE_, CCW_, CLAMP_TO_EDGE_, COLOR_ATTACHMENT0_, COLOR_BUFFER_BIT_, COLOR_CLEAR_VALUE_, COLOR_WRITEMASK_, COMPILE_STATUS_, COMPRESSED_TEXTURE_FORMATS_, CONSTANT_ALPHA_, CONSTANT_COLOR_, CONTEXT_LOST_WEBGL_, CULL_FACE_, CULL_FACE_MODE_, CURRENT_PROGRAM_, CURRENT_VERTEX_ATTRIB_, CW_, DECR_, DECR_WRAP_, DELETE_STATUS_, DEPTH_ATTACHMENT_, DEPTH_BITS_, DEPTH_BUFFER_BIT_, DEPTH_CLEAR_VALUE_, DEPTH_COMPONENT_, DEPTH_COMPONENT16_, DEPTH_FUNC_, DEPTH_RANGE_, DEPTH_STENCIL_, DEPTH_STENCIL_ATTACHMENT_, DEPTH_TEST_, DEPTH_WRITEMASK_, DITHER_, DONT_CARE_, DST_ALPHA_, DST_COLOR_, DYNAMIC_DRAW_, ELEMENT_ARRAY_BUFFER_, ELEMENT_ARRAY_BUFFER_BINDING_, EQUAL_, FASTEST_, FLOAT_, FLOAT_MAT2_, FLOAT_MAT3_, FLOAT_MAT4_, FLOAT_VEC2_, FLOAT_VEC3_, FLOAT_VEC4_, FRAGMENT_SHADER_, FRAMEBUFFER_, FRAMEBUFFER_ATTACHMENT_OBJECT_NAME_, FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE_, FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE_, FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL_, FRAMEBUFFER_BINDING_, FRAMEBUFFER_COMPLETE_, FRAMEBUFFER_INCOMPLETE_ATTACHMENT_, FRAMEBUFFER_INCOMPLETE_DIMENSIONS_, FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_, FRAMEBUFFER_UNSUPPORTED_, FRONT_, FRONT_AND_BACK_, FRONT_FACE_, FUNC_ADD_, FUNC_REVERSE_SUBTRACT_, FUNC_SUBTRACT_, GENERATE_MIPMAP_HINT_, GEQUAL_, GREATER_, GREEN_BITS_, HIGH_FLOAT_, HIGH_INT_, INCR_, INCR_WRAP_, INFO_LOG_LENGTH_, INT_, INT_VEC2_, INT_VEC3_, INT_VEC4_, INVALID_ENUM_, INVALID_FRAMEBUFFER_OPERATION_, INVALID_OPERATION_, INVALID_VALUE_, INVERT_, KEEP_, LEQUAL_, LESS_, LINEAR_, LINEAR_MIPMAP_LINEAR_, LINEAR_MIPMAP_NEAREST_, LINES_, LINE_LOOP_, LINE_STRIP_, LINE_WIDTH_, LINK_STATUS_, LOW_FLOAT_, LOW_INT_, LUMINANCE_, LUMINANCE_ALPHA_, MAX_COMBINED_TEXTURE_IMAGE_UNITS_, MAX_CUBE_MAP_TEXTURE_SIZE_, MAX_FRAGMENT_UNIFORM_VECTORS_, MAX_RENDERBUFFER_SIZE_, MAX_TEXTURE_IMAGE_UNITS_, MAX_TEXTURE_SIZE_, MAX_VARYING_VECTORS_, MAX_VERTEX_ATTRIBS_, MAX_VERTEX_TEXTURE_IMAGE_UNITS_, MAX_VERTEX_UNIFORM_VECTORS_, MAX_VIEWPORT_DIMS_, MEDIUM_FLOAT_, MEDIUM_INT_, MIRRORED_REPEAT_, NEAREST_, NEAREST_MIPMAP_LINEAR_, NEAREST_MIPMAP_NEAREST_, NEVER_, NICEST_, NONE_, NOTEQUAL_, NO_ERROR_, NUM_COMPRESSED_TEXTURE_FORMATS_, ONE_, ONE_MINUS_CONSTANT_ALPHA_, ONE_MINUS_CONSTANT_COLOR_, ONE_MINUS_DST_ALPHA_, ONE_MINUS_DST_COLOR_, ONE_MINUS_SRC_ALPHA_, ONE_MINUS_SRC_COLOR_, OUT_OF_MEMORY_, PACK_ALIGNMENT_, POINTS_, POLYGON_OFFSET_FACTOR_, POLYGON_OFFSET_FILL_, POLYGON_OFFSET_UNITS_, RED_BITS_, RENDERBUFFER_, RENDERBUFFER_ALPHA_SIZE_, RENDERBUFFER_BINDING_, RENDERBUFFER_BLUE_SIZE_, RENDERBUFFER_DEPTH_SIZE_, RENDERBUFFER_GREEN_SIZE_, RENDERBUFFER_HEIGHT_, RENDERBUFFER_INTERNAL_FORMAT_, RENDERBUFFER_RED_SIZE_, RENDERBUFFER_STENCIL_SIZE_, RENDERBUFFER_WIDTH_, RENDERER_, REPEAT_, REPLACE_, RGB_, RGB5_A1_, RGB565_, RGBA_, RGBA4_, SAMPLER_2D_, SAMPLER_CUBE_, SAMPLES_, SAMPLE_ALPHA_TO_COVERAGE_, SAMPLE_BUFFERS_, SAMPLE_COVERAGE_, SAMPLE_COVERAGE_INVERT_, SAMPLE_COVERAGE_VALUE_, SCISSOR_BOX_, SCISSOR_TEST_, SHADER_COMPILER_, SHADER_SOURCE_LENGTH_, SHADER_TYPE_, SHADING_LANGUAGE_VERSION_, SHORT_, SRC_ALPHA_, SRC_ALPHA_SATURATE_, SRC_COLOR_, STATIC_DRAW_, STENCIL_ATTACHMENT_, STENCIL_BACK_FAIL_, STENCIL_BACK_FUNC_, STENCIL_BACK_PASS_DEPTH_FAIL_, STENCIL_BACK_PASS_DEPTH_PASS_, STENCIL_BACK_REF_, STENCIL_BACK_VALUE_MASK_, STENCIL_BACK_WRITEMASK_, STENCIL_BITS_, STENCIL_BUFFER_BIT_, STENCIL_CLEAR_VALUE_, STENCIL_FAIL_, STENCIL_FUNC_, STENCIL_INDEX_, STENCIL_INDEX8_, STENCIL_PASS_DEPTH_FAIL_, STENCIL_PASS_DEPTH_PASS_, STENCIL_REF_, STENCIL_TEST_, STENCIL_VALUE_MASK_, STENCIL_WRITEMASK_, STREAM_DRAW_, SUBPIXEL_BITS_, TEXTURE_, TEXTURE0_, TEXTURE1_, TEXTURE2_, TEXTURE3_, TEXTURE4_, TEXTURE5_, TEXTURE6_, TEXTURE7_, TEXTURE8_, TEXTURE9_, TEXTURE10_, TEXTURE11_, TEXTURE12_, TEXTURE13_, TEXTURE14_, TEXTURE15_, TEXTURE16_, TEXTURE17_, TEXTURE18_, TEXTURE19_, TEXTURE20_, TEXTURE21_, TEXTURE22_, TEXTURE23_, TEXTURE24_, TEXTURE25_, TEXTURE26_, TEXTURE27_, TEXTURE28_, TEXTURE29_, TEXTURE30_, TEXTURE31_, TEXTURE_2D_, TEXTURE_BINDING_2D_, TEXTURE_BINDING_CUBE_MAP_, TEXTURE_CUBE_MAP_, TEXTURE_CUBE_MAP_NEGATIVE_X_, TEXTURE_CUBE_MAP_NEGATIVE_Y_, TEXTURE_CUBE_MAP_NEGATIVE_Z_, TEXTURE_CUBE_MAP_POSITIVE_X_, TEXTURE_CUBE_MAP_POSITIVE_Y_, TEXTURE_CUBE_MAP_POSITIVE_Z_, TEXTURE_MAG_FILTER_, TEXTURE_MIN_FILTER_, TEXTURE_WRAP_S_, TEXTURE_WRAP_T_, TRIANGLES_, TRIANGLE_FAN_, TRIANGLE_STRIP_, UNPACK_ALIGNMENT_, UNPACK_COLORSPACE_CONVERSION_WEBGL_, UNPACK_FLIP_Y_WEBGL_, UNPACK_PREMULTIPLY_ALPHA_WEBGL_, UNSIGNED_BYTE_, UNSIGNED_INT_, UNSIGNED_SHORT_, UNSIGNED_SHORT_4_4_4_4_, UNSIGNED_SHORT_5_5_5_1_, UNSIGNED_SHORT_5_6_5_, VALIDATE_STATUS_, VENDOR_, VERSION_, VERTEX_ATTRIB_ARRAY_BUFFER_BINDING_, VERTEX_ATTRIB_ARRAY_ENABLED_, VERTEX_ATTRIB_ARRAY_NORMALIZED_, VERTEX_ATTRIB_ARRAY_POINTER_, VERTEX_ATTRIB_ARRAY_SIZE_, VERTEX_ATTRIB_ARRAY_STRIDE_, VERTEX_ATTRIB_ARRAY_TYPE_, VERTEX_SHADER_, VIEWPORT_, ZERO_) {
		this.$val = this;
		this.Object = Object_ !== undefined ? Object_ : null;
		this.ARRAY_BUFFER = ARRAY_BUFFER_ !== undefined ? ARRAY_BUFFER_ : 0;
		this.ARRAY_BUFFER_BINDING = ARRAY_BUFFER_BINDING_ !== undefined ? ARRAY_BUFFER_BINDING_ : 0;
		this.ATTACHED_SHADERS = ATTACHED_SHADERS_ !== undefined ? ATTACHED_SHADERS_ : 0;
		this.BACK = BACK_ !== undefined ? BACK_ : 0;
		this.BLEND = BLEND_ !== undefined ? BLEND_ : 0;
		this.BLEND_COLOR = BLEND_COLOR_ !== undefined ? BLEND_COLOR_ : 0;
		this.BLEND_DST_ALPHA = BLEND_DST_ALPHA_ !== undefined ? BLEND_DST_ALPHA_ : 0;
		this.BLEND_DST_RGB = BLEND_DST_RGB_ !== undefined ? BLEND_DST_RGB_ : 0;
		this.BLEND_EQUATION = BLEND_EQUATION_ !== undefined ? BLEND_EQUATION_ : 0;
		this.BLEND_EQUATION_ALPHA = BLEND_EQUATION_ALPHA_ !== undefined ? BLEND_EQUATION_ALPHA_ : 0;
		this.BLEND_EQUATION_RGB = BLEND_EQUATION_RGB_ !== undefined ? BLEND_EQUATION_RGB_ : 0;
		this.BLEND_SRC_ALPHA = BLEND_SRC_ALPHA_ !== undefined ? BLEND_SRC_ALPHA_ : 0;
		this.BLEND_SRC_RGB = BLEND_SRC_RGB_ !== undefined ? BLEND_SRC_RGB_ : 0;
		this.BLUE_BITS = BLUE_BITS_ !== undefined ? BLUE_BITS_ : 0;
		this.BOOL = BOOL_ !== undefined ? BOOL_ : 0;
		this.BOOL_VEC2 = BOOL_VEC2_ !== undefined ? BOOL_VEC2_ : 0;
		this.BOOL_VEC3 = BOOL_VEC3_ !== undefined ? BOOL_VEC3_ : 0;
		this.BOOL_VEC4 = BOOL_VEC4_ !== undefined ? BOOL_VEC4_ : 0;
		this.BROWSER_DEFAULT_WEBGL = BROWSER_DEFAULT_WEBGL_ !== undefined ? BROWSER_DEFAULT_WEBGL_ : 0;
		this.BUFFER_SIZE = BUFFER_SIZE_ !== undefined ? BUFFER_SIZE_ : 0;
		this.BUFFER_USAGE = BUFFER_USAGE_ !== undefined ? BUFFER_USAGE_ : 0;
		this.BYTE = BYTE_ !== undefined ? BYTE_ : 0;
		this.CCW = CCW_ !== undefined ? CCW_ : 0;
		this.CLAMP_TO_EDGE = CLAMP_TO_EDGE_ !== undefined ? CLAMP_TO_EDGE_ : 0;
		this.COLOR_ATTACHMENT0 = COLOR_ATTACHMENT0_ !== undefined ? COLOR_ATTACHMENT0_ : 0;
		this.COLOR_BUFFER_BIT = COLOR_BUFFER_BIT_ !== undefined ? COLOR_BUFFER_BIT_ : 0;
		this.COLOR_CLEAR_VALUE = COLOR_CLEAR_VALUE_ !== undefined ? COLOR_CLEAR_VALUE_ : 0;
		this.COLOR_WRITEMASK = COLOR_WRITEMASK_ !== undefined ? COLOR_WRITEMASK_ : 0;
		this.COMPILE_STATUS = COMPILE_STATUS_ !== undefined ? COMPILE_STATUS_ : 0;
		this.COMPRESSED_TEXTURE_FORMATS = COMPRESSED_TEXTURE_FORMATS_ !== undefined ? COMPRESSED_TEXTURE_FORMATS_ : 0;
		this.CONSTANT_ALPHA = CONSTANT_ALPHA_ !== undefined ? CONSTANT_ALPHA_ : 0;
		this.CONSTANT_COLOR = CONSTANT_COLOR_ !== undefined ? CONSTANT_COLOR_ : 0;
		this.CONTEXT_LOST_WEBGL = CONTEXT_LOST_WEBGL_ !== undefined ? CONTEXT_LOST_WEBGL_ : 0;
		this.CULL_FACE = CULL_FACE_ !== undefined ? CULL_FACE_ : 0;
		this.CULL_FACE_MODE = CULL_FACE_MODE_ !== undefined ? CULL_FACE_MODE_ : 0;
		this.CURRENT_PROGRAM = CURRENT_PROGRAM_ !== undefined ? CURRENT_PROGRAM_ : 0;
		this.CURRENT_VERTEX_ATTRIB = CURRENT_VERTEX_ATTRIB_ !== undefined ? CURRENT_VERTEX_ATTRIB_ : 0;
		this.CW = CW_ !== undefined ? CW_ : 0;
		this.DECR = DECR_ !== undefined ? DECR_ : 0;
		this.DECR_WRAP = DECR_WRAP_ !== undefined ? DECR_WRAP_ : 0;
		this.DELETE_STATUS = DELETE_STATUS_ !== undefined ? DELETE_STATUS_ : 0;
		this.DEPTH_ATTACHMENT = DEPTH_ATTACHMENT_ !== undefined ? DEPTH_ATTACHMENT_ : 0;
		this.DEPTH_BITS = DEPTH_BITS_ !== undefined ? DEPTH_BITS_ : 0;
		this.DEPTH_BUFFER_BIT = DEPTH_BUFFER_BIT_ !== undefined ? DEPTH_BUFFER_BIT_ : 0;
		this.DEPTH_CLEAR_VALUE = DEPTH_CLEAR_VALUE_ !== undefined ? DEPTH_CLEAR_VALUE_ : 0;
		this.DEPTH_COMPONENT = DEPTH_COMPONENT_ !== undefined ? DEPTH_COMPONENT_ : 0;
		this.DEPTH_COMPONENT16 = DEPTH_COMPONENT16_ !== undefined ? DEPTH_COMPONENT16_ : 0;
		this.DEPTH_FUNC = DEPTH_FUNC_ !== undefined ? DEPTH_FUNC_ : 0;
		this.DEPTH_RANGE = DEPTH_RANGE_ !== undefined ? DEPTH_RANGE_ : 0;
		this.DEPTH_STENCIL = DEPTH_STENCIL_ !== undefined ? DEPTH_STENCIL_ : 0;
		this.DEPTH_STENCIL_ATTACHMENT = DEPTH_STENCIL_ATTACHMENT_ !== undefined ? DEPTH_STENCIL_ATTACHMENT_ : 0;
		this.DEPTH_TEST = DEPTH_TEST_ !== undefined ? DEPTH_TEST_ : 0;
		this.DEPTH_WRITEMASK = DEPTH_WRITEMASK_ !== undefined ? DEPTH_WRITEMASK_ : 0;
		this.DITHER = DITHER_ !== undefined ? DITHER_ : 0;
		this.DONT_CARE = DONT_CARE_ !== undefined ? DONT_CARE_ : 0;
		this.DST_ALPHA = DST_ALPHA_ !== undefined ? DST_ALPHA_ : 0;
		this.DST_COLOR = DST_COLOR_ !== undefined ? DST_COLOR_ : 0;
		this.DYNAMIC_DRAW = DYNAMIC_DRAW_ !== undefined ? DYNAMIC_DRAW_ : 0;
		this.ELEMENT_ARRAY_BUFFER = ELEMENT_ARRAY_BUFFER_ !== undefined ? ELEMENT_ARRAY_BUFFER_ : 0;
		this.ELEMENT_ARRAY_BUFFER_BINDING = ELEMENT_ARRAY_BUFFER_BINDING_ !== undefined ? ELEMENT_ARRAY_BUFFER_BINDING_ : 0;
		this.EQUAL = EQUAL_ !== undefined ? EQUAL_ : 0;
		this.FASTEST = FASTEST_ !== undefined ? FASTEST_ : 0;
		this.FLOAT = FLOAT_ !== undefined ? FLOAT_ : 0;
		this.FLOAT_MAT2 = FLOAT_MAT2_ !== undefined ? FLOAT_MAT2_ : 0;
		this.FLOAT_MAT3 = FLOAT_MAT3_ !== undefined ? FLOAT_MAT3_ : 0;
		this.FLOAT_MAT4 = FLOAT_MAT4_ !== undefined ? FLOAT_MAT4_ : 0;
		this.FLOAT_VEC2 = FLOAT_VEC2_ !== undefined ? FLOAT_VEC2_ : 0;
		this.FLOAT_VEC3 = FLOAT_VEC3_ !== undefined ? FLOAT_VEC3_ : 0;
		this.FLOAT_VEC4 = FLOAT_VEC4_ !== undefined ? FLOAT_VEC4_ : 0;
		this.FRAGMENT_SHADER = FRAGMENT_SHADER_ !== undefined ? FRAGMENT_SHADER_ : 0;
		this.FRAMEBUFFER = FRAMEBUFFER_ !== undefined ? FRAMEBUFFER_ : 0;
		this.FRAMEBUFFER_ATTACHMENT_OBJECT_NAME = FRAMEBUFFER_ATTACHMENT_OBJECT_NAME_ !== undefined ? FRAMEBUFFER_ATTACHMENT_OBJECT_NAME_ : 0;
		this.FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE = FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE_ !== undefined ? FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE_ : 0;
		this.FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE = FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE_ !== undefined ? FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE_ : 0;
		this.FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL = FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL_ !== undefined ? FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL_ : 0;
		this.FRAMEBUFFER_BINDING = FRAMEBUFFER_BINDING_ !== undefined ? FRAMEBUFFER_BINDING_ : 0;
		this.FRAMEBUFFER_COMPLETE = FRAMEBUFFER_COMPLETE_ !== undefined ? FRAMEBUFFER_COMPLETE_ : 0;
		this.FRAMEBUFFER_INCOMPLETE_ATTACHMENT = FRAMEBUFFER_INCOMPLETE_ATTACHMENT_ !== undefined ? FRAMEBUFFER_INCOMPLETE_ATTACHMENT_ : 0;
		this.FRAMEBUFFER_INCOMPLETE_DIMENSIONS = FRAMEBUFFER_INCOMPLETE_DIMENSIONS_ !== undefined ? FRAMEBUFFER_INCOMPLETE_DIMENSIONS_ : 0;
		this.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT = FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_ !== undefined ? FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_ : 0;
		this.FRAMEBUFFER_UNSUPPORTED = FRAMEBUFFER_UNSUPPORTED_ !== undefined ? FRAMEBUFFER_UNSUPPORTED_ : 0;
		this.FRONT = FRONT_ !== undefined ? FRONT_ : 0;
		this.FRONT_AND_BACK = FRONT_AND_BACK_ !== undefined ? FRONT_AND_BACK_ : 0;
		this.FRONT_FACE = FRONT_FACE_ !== undefined ? FRONT_FACE_ : 0;
		this.FUNC_ADD = FUNC_ADD_ !== undefined ? FUNC_ADD_ : 0;
		this.FUNC_REVERSE_SUBTRACT = FUNC_REVERSE_SUBTRACT_ !== undefined ? FUNC_REVERSE_SUBTRACT_ : 0;
		this.FUNC_SUBTRACT = FUNC_SUBTRACT_ !== undefined ? FUNC_SUBTRACT_ : 0;
		this.GENERATE_MIPMAP_HINT = GENERATE_MIPMAP_HINT_ !== undefined ? GENERATE_MIPMAP_HINT_ : 0;
		this.GEQUAL = GEQUAL_ !== undefined ? GEQUAL_ : 0;
		this.GREATER = GREATER_ !== undefined ? GREATER_ : 0;
		this.GREEN_BITS = GREEN_BITS_ !== undefined ? GREEN_BITS_ : 0;
		this.HIGH_FLOAT = HIGH_FLOAT_ !== undefined ? HIGH_FLOAT_ : 0;
		this.HIGH_INT = HIGH_INT_ !== undefined ? HIGH_INT_ : 0;
		this.INCR = INCR_ !== undefined ? INCR_ : 0;
		this.INCR_WRAP = INCR_WRAP_ !== undefined ? INCR_WRAP_ : 0;
		this.INFO_LOG_LENGTH = INFO_LOG_LENGTH_ !== undefined ? INFO_LOG_LENGTH_ : 0;
		this.INT = INT_ !== undefined ? INT_ : 0;
		this.INT_VEC2 = INT_VEC2_ !== undefined ? INT_VEC2_ : 0;
		this.INT_VEC3 = INT_VEC3_ !== undefined ? INT_VEC3_ : 0;
		this.INT_VEC4 = INT_VEC4_ !== undefined ? INT_VEC4_ : 0;
		this.INVALID_ENUM = INVALID_ENUM_ !== undefined ? INVALID_ENUM_ : 0;
		this.INVALID_FRAMEBUFFER_OPERATION = INVALID_FRAMEBUFFER_OPERATION_ !== undefined ? INVALID_FRAMEBUFFER_OPERATION_ : 0;
		this.INVALID_OPERATION = INVALID_OPERATION_ !== undefined ? INVALID_OPERATION_ : 0;
		this.INVALID_VALUE = INVALID_VALUE_ !== undefined ? INVALID_VALUE_ : 0;
		this.INVERT = INVERT_ !== undefined ? INVERT_ : 0;
		this.KEEP = KEEP_ !== undefined ? KEEP_ : 0;
		this.LEQUAL = LEQUAL_ !== undefined ? LEQUAL_ : 0;
		this.LESS = LESS_ !== undefined ? LESS_ : 0;
		this.LINEAR = LINEAR_ !== undefined ? LINEAR_ : 0;
		this.LINEAR_MIPMAP_LINEAR = LINEAR_MIPMAP_LINEAR_ !== undefined ? LINEAR_MIPMAP_LINEAR_ : 0;
		this.LINEAR_MIPMAP_NEAREST = LINEAR_MIPMAP_NEAREST_ !== undefined ? LINEAR_MIPMAP_NEAREST_ : 0;
		this.LINES = LINES_ !== undefined ? LINES_ : 0;
		this.LINE_LOOP = LINE_LOOP_ !== undefined ? LINE_LOOP_ : 0;
		this.LINE_STRIP = LINE_STRIP_ !== undefined ? LINE_STRIP_ : 0;
		this.LINE_WIDTH = LINE_WIDTH_ !== undefined ? LINE_WIDTH_ : 0;
		this.LINK_STATUS = LINK_STATUS_ !== undefined ? LINK_STATUS_ : 0;
		this.LOW_FLOAT = LOW_FLOAT_ !== undefined ? LOW_FLOAT_ : 0;
		this.LOW_INT = LOW_INT_ !== undefined ? LOW_INT_ : 0;
		this.LUMINANCE = LUMINANCE_ !== undefined ? LUMINANCE_ : 0;
		this.LUMINANCE_ALPHA = LUMINANCE_ALPHA_ !== undefined ? LUMINANCE_ALPHA_ : 0;
		this.MAX_COMBINED_TEXTURE_IMAGE_UNITS = MAX_COMBINED_TEXTURE_IMAGE_UNITS_ !== undefined ? MAX_COMBINED_TEXTURE_IMAGE_UNITS_ : 0;
		this.MAX_CUBE_MAP_TEXTURE_SIZE = MAX_CUBE_MAP_TEXTURE_SIZE_ !== undefined ? MAX_CUBE_MAP_TEXTURE_SIZE_ : 0;
		this.MAX_FRAGMENT_UNIFORM_VECTORS = MAX_FRAGMENT_UNIFORM_VECTORS_ !== undefined ? MAX_FRAGMENT_UNIFORM_VECTORS_ : 0;
		this.MAX_RENDERBUFFER_SIZE = MAX_RENDERBUFFER_SIZE_ !== undefined ? MAX_RENDERBUFFER_SIZE_ : 0;
		this.MAX_TEXTURE_IMAGE_UNITS = MAX_TEXTURE_IMAGE_UNITS_ !== undefined ? MAX_TEXTURE_IMAGE_UNITS_ : 0;
		this.MAX_TEXTURE_SIZE = MAX_TEXTURE_SIZE_ !== undefined ? MAX_TEXTURE_SIZE_ : 0;
		this.MAX_VARYING_VECTORS = MAX_VARYING_VECTORS_ !== undefined ? MAX_VARYING_VECTORS_ : 0;
		this.MAX_VERTEX_ATTRIBS = MAX_VERTEX_ATTRIBS_ !== undefined ? MAX_VERTEX_ATTRIBS_ : 0;
		this.MAX_VERTEX_TEXTURE_IMAGE_UNITS = MAX_VERTEX_TEXTURE_IMAGE_UNITS_ !== undefined ? MAX_VERTEX_TEXTURE_IMAGE_UNITS_ : 0;
		this.MAX_VERTEX_UNIFORM_VECTORS = MAX_VERTEX_UNIFORM_VECTORS_ !== undefined ? MAX_VERTEX_UNIFORM_VECTORS_ : 0;
		this.MAX_VIEWPORT_DIMS = MAX_VIEWPORT_DIMS_ !== undefined ? MAX_VIEWPORT_DIMS_ : 0;
		this.MEDIUM_FLOAT = MEDIUM_FLOAT_ !== undefined ? MEDIUM_FLOAT_ : 0;
		this.MEDIUM_INT = MEDIUM_INT_ !== undefined ? MEDIUM_INT_ : 0;
		this.MIRRORED_REPEAT = MIRRORED_REPEAT_ !== undefined ? MIRRORED_REPEAT_ : 0;
		this.NEAREST = NEAREST_ !== undefined ? NEAREST_ : 0;
		this.NEAREST_MIPMAP_LINEAR = NEAREST_MIPMAP_LINEAR_ !== undefined ? NEAREST_MIPMAP_LINEAR_ : 0;
		this.NEAREST_MIPMAP_NEAREST = NEAREST_MIPMAP_NEAREST_ !== undefined ? NEAREST_MIPMAP_NEAREST_ : 0;
		this.NEVER = NEVER_ !== undefined ? NEVER_ : 0;
		this.NICEST = NICEST_ !== undefined ? NICEST_ : 0;
		this.NONE = NONE_ !== undefined ? NONE_ : 0;
		this.NOTEQUAL = NOTEQUAL_ !== undefined ? NOTEQUAL_ : 0;
		this.NO_ERROR = NO_ERROR_ !== undefined ? NO_ERROR_ : 0;
		this.NUM_COMPRESSED_TEXTURE_FORMATS = NUM_COMPRESSED_TEXTURE_FORMATS_ !== undefined ? NUM_COMPRESSED_TEXTURE_FORMATS_ : 0;
		this.ONE = ONE_ !== undefined ? ONE_ : 0;
		this.ONE_MINUS_CONSTANT_ALPHA = ONE_MINUS_CONSTANT_ALPHA_ !== undefined ? ONE_MINUS_CONSTANT_ALPHA_ : 0;
		this.ONE_MINUS_CONSTANT_COLOR = ONE_MINUS_CONSTANT_COLOR_ !== undefined ? ONE_MINUS_CONSTANT_COLOR_ : 0;
		this.ONE_MINUS_DST_ALPHA = ONE_MINUS_DST_ALPHA_ !== undefined ? ONE_MINUS_DST_ALPHA_ : 0;
		this.ONE_MINUS_DST_COLOR = ONE_MINUS_DST_COLOR_ !== undefined ? ONE_MINUS_DST_COLOR_ : 0;
		this.ONE_MINUS_SRC_ALPHA = ONE_MINUS_SRC_ALPHA_ !== undefined ? ONE_MINUS_SRC_ALPHA_ : 0;
		this.ONE_MINUS_SRC_COLOR = ONE_MINUS_SRC_COLOR_ !== undefined ? ONE_MINUS_SRC_COLOR_ : 0;
		this.OUT_OF_MEMORY = OUT_OF_MEMORY_ !== undefined ? OUT_OF_MEMORY_ : 0;
		this.PACK_ALIGNMENT = PACK_ALIGNMENT_ !== undefined ? PACK_ALIGNMENT_ : 0;
		this.POINTS = POINTS_ !== undefined ? POINTS_ : 0;
		this.POLYGON_OFFSET_FACTOR = POLYGON_OFFSET_FACTOR_ !== undefined ? POLYGON_OFFSET_FACTOR_ : 0;
		this.POLYGON_OFFSET_FILL = POLYGON_OFFSET_FILL_ !== undefined ? POLYGON_OFFSET_FILL_ : 0;
		this.POLYGON_OFFSET_UNITS = POLYGON_OFFSET_UNITS_ !== undefined ? POLYGON_OFFSET_UNITS_ : 0;
		this.RED_BITS = RED_BITS_ !== undefined ? RED_BITS_ : 0;
		this.RENDERBUFFER = RENDERBUFFER_ !== undefined ? RENDERBUFFER_ : 0;
		this.RENDERBUFFER_ALPHA_SIZE = RENDERBUFFER_ALPHA_SIZE_ !== undefined ? RENDERBUFFER_ALPHA_SIZE_ : 0;
		this.RENDERBUFFER_BINDING = RENDERBUFFER_BINDING_ !== undefined ? RENDERBUFFER_BINDING_ : 0;
		this.RENDERBUFFER_BLUE_SIZE = RENDERBUFFER_BLUE_SIZE_ !== undefined ? RENDERBUFFER_BLUE_SIZE_ : 0;
		this.RENDERBUFFER_DEPTH_SIZE = RENDERBUFFER_DEPTH_SIZE_ !== undefined ? RENDERBUFFER_DEPTH_SIZE_ : 0;
		this.RENDERBUFFER_GREEN_SIZE = RENDERBUFFER_GREEN_SIZE_ !== undefined ? RENDERBUFFER_GREEN_SIZE_ : 0;
		this.RENDERBUFFER_HEIGHT = RENDERBUFFER_HEIGHT_ !== undefined ? RENDERBUFFER_HEIGHT_ : 0;
		this.RENDERBUFFER_INTERNAL_FORMAT = RENDERBUFFER_INTERNAL_FORMAT_ !== undefined ? RENDERBUFFER_INTERNAL_FORMAT_ : 0;
		this.RENDERBUFFER_RED_SIZE = RENDERBUFFER_RED_SIZE_ !== undefined ? RENDERBUFFER_RED_SIZE_ : 0;
		this.RENDERBUFFER_STENCIL_SIZE = RENDERBUFFER_STENCIL_SIZE_ !== undefined ? RENDERBUFFER_STENCIL_SIZE_ : 0;
		this.RENDERBUFFER_WIDTH = RENDERBUFFER_WIDTH_ !== undefined ? RENDERBUFFER_WIDTH_ : 0;
		this.RENDERER = RENDERER_ !== undefined ? RENDERER_ : 0;
		this.REPEAT = REPEAT_ !== undefined ? REPEAT_ : 0;
		this.REPLACE = REPLACE_ !== undefined ? REPLACE_ : 0;
		this.RGB = RGB_ !== undefined ? RGB_ : 0;
		this.RGB5_A1 = RGB5_A1_ !== undefined ? RGB5_A1_ : 0;
		this.RGB565 = RGB565_ !== undefined ? RGB565_ : 0;
		this.RGBA = RGBA_ !== undefined ? RGBA_ : 0;
		this.RGBA4 = RGBA4_ !== undefined ? RGBA4_ : 0;
		this.SAMPLER_2D = SAMPLER_2D_ !== undefined ? SAMPLER_2D_ : 0;
		this.SAMPLER_CUBE = SAMPLER_CUBE_ !== undefined ? SAMPLER_CUBE_ : 0;
		this.SAMPLES = SAMPLES_ !== undefined ? SAMPLES_ : 0;
		this.SAMPLE_ALPHA_TO_COVERAGE = SAMPLE_ALPHA_TO_COVERAGE_ !== undefined ? SAMPLE_ALPHA_TO_COVERAGE_ : 0;
		this.SAMPLE_BUFFERS = SAMPLE_BUFFERS_ !== undefined ? SAMPLE_BUFFERS_ : 0;
		this.SAMPLE_COVERAGE = SAMPLE_COVERAGE_ !== undefined ? SAMPLE_COVERAGE_ : 0;
		this.SAMPLE_COVERAGE_INVERT = SAMPLE_COVERAGE_INVERT_ !== undefined ? SAMPLE_COVERAGE_INVERT_ : 0;
		this.SAMPLE_COVERAGE_VALUE = SAMPLE_COVERAGE_VALUE_ !== undefined ? SAMPLE_COVERAGE_VALUE_ : 0;
		this.SCISSOR_BOX = SCISSOR_BOX_ !== undefined ? SCISSOR_BOX_ : 0;
		this.SCISSOR_TEST = SCISSOR_TEST_ !== undefined ? SCISSOR_TEST_ : 0;
		this.SHADER_COMPILER = SHADER_COMPILER_ !== undefined ? SHADER_COMPILER_ : 0;
		this.SHADER_SOURCE_LENGTH = SHADER_SOURCE_LENGTH_ !== undefined ? SHADER_SOURCE_LENGTH_ : 0;
		this.SHADER_TYPE = SHADER_TYPE_ !== undefined ? SHADER_TYPE_ : 0;
		this.SHADING_LANGUAGE_VERSION = SHADING_LANGUAGE_VERSION_ !== undefined ? SHADING_LANGUAGE_VERSION_ : 0;
		this.SHORT = SHORT_ !== undefined ? SHORT_ : 0;
		this.SRC_ALPHA = SRC_ALPHA_ !== undefined ? SRC_ALPHA_ : 0;
		this.SRC_ALPHA_SATURATE = SRC_ALPHA_SATURATE_ !== undefined ? SRC_ALPHA_SATURATE_ : 0;
		this.SRC_COLOR = SRC_COLOR_ !== undefined ? SRC_COLOR_ : 0;
		this.STATIC_DRAW = STATIC_DRAW_ !== undefined ? STATIC_DRAW_ : 0;
		this.STENCIL_ATTACHMENT = STENCIL_ATTACHMENT_ !== undefined ? STENCIL_ATTACHMENT_ : 0;
		this.STENCIL_BACK_FAIL = STENCIL_BACK_FAIL_ !== undefined ? STENCIL_BACK_FAIL_ : 0;
		this.STENCIL_BACK_FUNC = STENCIL_BACK_FUNC_ !== undefined ? STENCIL_BACK_FUNC_ : 0;
		this.STENCIL_BACK_PASS_DEPTH_FAIL = STENCIL_BACK_PASS_DEPTH_FAIL_ !== undefined ? STENCIL_BACK_PASS_DEPTH_FAIL_ : 0;
		this.STENCIL_BACK_PASS_DEPTH_PASS = STENCIL_BACK_PASS_DEPTH_PASS_ !== undefined ? STENCIL_BACK_PASS_DEPTH_PASS_ : 0;
		this.STENCIL_BACK_REF = STENCIL_BACK_REF_ !== undefined ? STENCIL_BACK_REF_ : 0;
		this.STENCIL_BACK_VALUE_MASK = STENCIL_BACK_VALUE_MASK_ !== undefined ? STENCIL_BACK_VALUE_MASK_ : 0;
		this.STENCIL_BACK_WRITEMASK = STENCIL_BACK_WRITEMASK_ !== undefined ? STENCIL_BACK_WRITEMASK_ : 0;
		this.STENCIL_BITS = STENCIL_BITS_ !== undefined ? STENCIL_BITS_ : 0;
		this.STENCIL_BUFFER_BIT = STENCIL_BUFFER_BIT_ !== undefined ? STENCIL_BUFFER_BIT_ : 0;
		this.STENCIL_CLEAR_VALUE = STENCIL_CLEAR_VALUE_ !== undefined ? STENCIL_CLEAR_VALUE_ : 0;
		this.STENCIL_FAIL = STENCIL_FAIL_ !== undefined ? STENCIL_FAIL_ : 0;
		this.STENCIL_FUNC = STENCIL_FUNC_ !== undefined ? STENCIL_FUNC_ : 0;
		this.STENCIL_INDEX = STENCIL_INDEX_ !== undefined ? STENCIL_INDEX_ : 0;
		this.STENCIL_INDEX8 = STENCIL_INDEX8_ !== undefined ? STENCIL_INDEX8_ : 0;
		this.STENCIL_PASS_DEPTH_FAIL = STENCIL_PASS_DEPTH_FAIL_ !== undefined ? STENCIL_PASS_DEPTH_FAIL_ : 0;
		this.STENCIL_PASS_DEPTH_PASS = STENCIL_PASS_DEPTH_PASS_ !== undefined ? STENCIL_PASS_DEPTH_PASS_ : 0;
		this.STENCIL_REF = STENCIL_REF_ !== undefined ? STENCIL_REF_ : 0;
		this.STENCIL_TEST = STENCIL_TEST_ !== undefined ? STENCIL_TEST_ : 0;
		this.STENCIL_VALUE_MASK = STENCIL_VALUE_MASK_ !== undefined ? STENCIL_VALUE_MASK_ : 0;
		this.STENCIL_WRITEMASK = STENCIL_WRITEMASK_ !== undefined ? STENCIL_WRITEMASK_ : 0;
		this.STREAM_DRAW = STREAM_DRAW_ !== undefined ? STREAM_DRAW_ : 0;
		this.SUBPIXEL_BITS = SUBPIXEL_BITS_ !== undefined ? SUBPIXEL_BITS_ : 0;
		this.TEXTURE = TEXTURE_ !== undefined ? TEXTURE_ : 0;
		this.TEXTURE0 = TEXTURE0_ !== undefined ? TEXTURE0_ : 0;
		this.TEXTURE1 = TEXTURE1_ !== undefined ? TEXTURE1_ : 0;
		this.TEXTURE2 = TEXTURE2_ !== undefined ? TEXTURE2_ : 0;
		this.TEXTURE3 = TEXTURE3_ !== undefined ? TEXTURE3_ : 0;
		this.TEXTURE4 = TEXTURE4_ !== undefined ? TEXTURE4_ : 0;
		this.TEXTURE5 = TEXTURE5_ !== undefined ? TEXTURE5_ : 0;
		this.TEXTURE6 = TEXTURE6_ !== undefined ? TEXTURE6_ : 0;
		this.TEXTURE7 = TEXTURE7_ !== undefined ? TEXTURE7_ : 0;
		this.TEXTURE8 = TEXTURE8_ !== undefined ? TEXTURE8_ : 0;
		this.TEXTURE9 = TEXTURE9_ !== undefined ? TEXTURE9_ : 0;
		this.TEXTURE10 = TEXTURE10_ !== undefined ? TEXTURE10_ : 0;
		this.TEXTURE11 = TEXTURE11_ !== undefined ? TEXTURE11_ : 0;
		this.TEXTURE12 = TEXTURE12_ !== undefined ? TEXTURE12_ : 0;
		this.TEXTURE13 = TEXTURE13_ !== undefined ? TEXTURE13_ : 0;
		this.TEXTURE14 = TEXTURE14_ !== undefined ? TEXTURE14_ : 0;
		this.TEXTURE15 = TEXTURE15_ !== undefined ? TEXTURE15_ : 0;
		this.TEXTURE16 = TEXTURE16_ !== undefined ? TEXTURE16_ : 0;
		this.TEXTURE17 = TEXTURE17_ !== undefined ? TEXTURE17_ : 0;
		this.TEXTURE18 = TEXTURE18_ !== undefined ? TEXTURE18_ : 0;
		this.TEXTURE19 = TEXTURE19_ !== undefined ? TEXTURE19_ : 0;
		this.TEXTURE20 = TEXTURE20_ !== undefined ? TEXTURE20_ : 0;
		this.TEXTURE21 = TEXTURE21_ !== undefined ? TEXTURE21_ : 0;
		this.TEXTURE22 = TEXTURE22_ !== undefined ? TEXTURE22_ : 0;
		this.TEXTURE23 = TEXTURE23_ !== undefined ? TEXTURE23_ : 0;
		this.TEXTURE24 = TEXTURE24_ !== undefined ? TEXTURE24_ : 0;
		this.TEXTURE25 = TEXTURE25_ !== undefined ? TEXTURE25_ : 0;
		this.TEXTURE26 = TEXTURE26_ !== undefined ? TEXTURE26_ : 0;
		this.TEXTURE27 = TEXTURE27_ !== undefined ? TEXTURE27_ : 0;
		this.TEXTURE28 = TEXTURE28_ !== undefined ? TEXTURE28_ : 0;
		this.TEXTURE29 = TEXTURE29_ !== undefined ? TEXTURE29_ : 0;
		this.TEXTURE30 = TEXTURE30_ !== undefined ? TEXTURE30_ : 0;
		this.TEXTURE31 = TEXTURE31_ !== undefined ? TEXTURE31_ : 0;
		this.TEXTURE_2D = TEXTURE_2D_ !== undefined ? TEXTURE_2D_ : 0;
		this.TEXTURE_BINDING_2D = TEXTURE_BINDING_2D_ !== undefined ? TEXTURE_BINDING_2D_ : 0;
		this.TEXTURE_BINDING_CUBE_MAP = TEXTURE_BINDING_CUBE_MAP_ !== undefined ? TEXTURE_BINDING_CUBE_MAP_ : 0;
		this.TEXTURE_CUBE_MAP = TEXTURE_CUBE_MAP_ !== undefined ? TEXTURE_CUBE_MAP_ : 0;
		this.TEXTURE_CUBE_MAP_NEGATIVE_X = TEXTURE_CUBE_MAP_NEGATIVE_X_ !== undefined ? TEXTURE_CUBE_MAP_NEGATIVE_X_ : 0;
		this.TEXTURE_CUBE_MAP_NEGATIVE_Y = TEXTURE_CUBE_MAP_NEGATIVE_Y_ !== undefined ? TEXTURE_CUBE_MAP_NEGATIVE_Y_ : 0;
		this.TEXTURE_CUBE_MAP_NEGATIVE_Z = TEXTURE_CUBE_MAP_NEGATIVE_Z_ !== undefined ? TEXTURE_CUBE_MAP_NEGATIVE_Z_ : 0;
		this.TEXTURE_CUBE_MAP_POSITIVE_X = TEXTURE_CUBE_MAP_POSITIVE_X_ !== undefined ? TEXTURE_CUBE_MAP_POSITIVE_X_ : 0;
		this.TEXTURE_CUBE_MAP_POSITIVE_Y = TEXTURE_CUBE_MAP_POSITIVE_Y_ !== undefined ? TEXTURE_CUBE_MAP_POSITIVE_Y_ : 0;
		this.TEXTURE_CUBE_MAP_POSITIVE_Z = TEXTURE_CUBE_MAP_POSITIVE_Z_ !== undefined ? TEXTURE_CUBE_MAP_POSITIVE_Z_ : 0;
		this.TEXTURE_MAG_FILTER = TEXTURE_MAG_FILTER_ !== undefined ? TEXTURE_MAG_FILTER_ : 0;
		this.TEXTURE_MIN_FILTER = TEXTURE_MIN_FILTER_ !== undefined ? TEXTURE_MIN_FILTER_ : 0;
		this.TEXTURE_WRAP_S = TEXTURE_WRAP_S_ !== undefined ? TEXTURE_WRAP_S_ : 0;
		this.TEXTURE_WRAP_T = TEXTURE_WRAP_T_ !== undefined ? TEXTURE_WRAP_T_ : 0;
		this.TRIANGLES = TRIANGLES_ !== undefined ? TRIANGLES_ : 0;
		this.TRIANGLE_FAN = TRIANGLE_FAN_ !== undefined ? TRIANGLE_FAN_ : 0;
		this.TRIANGLE_STRIP = TRIANGLE_STRIP_ !== undefined ? TRIANGLE_STRIP_ : 0;
		this.UNPACK_ALIGNMENT = UNPACK_ALIGNMENT_ !== undefined ? UNPACK_ALIGNMENT_ : 0;
		this.UNPACK_COLORSPACE_CONVERSION_WEBGL = UNPACK_COLORSPACE_CONVERSION_WEBGL_ !== undefined ? UNPACK_COLORSPACE_CONVERSION_WEBGL_ : 0;
		this.UNPACK_FLIP_Y_WEBGL = UNPACK_FLIP_Y_WEBGL_ !== undefined ? UNPACK_FLIP_Y_WEBGL_ : 0;
		this.UNPACK_PREMULTIPLY_ALPHA_WEBGL = UNPACK_PREMULTIPLY_ALPHA_WEBGL_ !== undefined ? UNPACK_PREMULTIPLY_ALPHA_WEBGL_ : 0;
		this.UNSIGNED_BYTE = UNSIGNED_BYTE_ !== undefined ? UNSIGNED_BYTE_ : 0;
		this.UNSIGNED_INT = UNSIGNED_INT_ !== undefined ? UNSIGNED_INT_ : 0;
		this.UNSIGNED_SHORT = UNSIGNED_SHORT_ !== undefined ? UNSIGNED_SHORT_ : 0;
		this.UNSIGNED_SHORT_4_4_4_4 = UNSIGNED_SHORT_4_4_4_4_ !== undefined ? UNSIGNED_SHORT_4_4_4_4_ : 0;
		this.UNSIGNED_SHORT_5_5_5_1 = UNSIGNED_SHORT_5_5_5_1_ !== undefined ? UNSIGNED_SHORT_5_5_5_1_ : 0;
		this.UNSIGNED_SHORT_5_6_5 = UNSIGNED_SHORT_5_6_5_ !== undefined ? UNSIGNED_SHORT_5_6_5_ : 0;
		this.VALIDATE_STATUS = VALIDATE_STATUS_ !== undefined ? VALIDATE_STATUS_ : 0;
		this.VENDOR = VENDOR_ !== undefined ? VENDOR_ : 0;
		this.VERSION = VERSION_ !== undefined ? VERSION_ : 0;
		this.VERTEX_ATTRIB_ARRAY_BUFFER_BINDING = VERTEX_ATTRIB_ARRAY_BUFFER_BINDING_ !== undefined ? VERTEX_ATTRIB_ARRAY_BUFFER_BINDING_ : 0;
		this.VERTEX_ATTRIB_ARRAY_ENABLED = VERTEX_ATTRIB_ARRAY_ENABLED_ !== undefined ? VERTEX_ATTRIB_ARRAY_ENABLED_ : 0;
		this.VERTEX_ATTRIB_ARRAY_NORMALIZED = VERTEX_ATTRIB_ARRAY_NORMALIZED_ !== undefined ? VERTEX_ATTRIB_ARRAY_NORMALIZED_ : 0;
		this.VERTEX_ATTRIB_ARRAY_POINTER = VERTEX_ATTRIB_ARRAY_POINTER_ !== undefined ? VERTEX_ATTRIB_ARRAY_POINTER_ : 0;
		this.VERTEX_ATTRIB_ARRAY_SIZE = VERTEX_ATTRIB_ARRAY_SIZE_ !== undefined ? VERTEX_ATTRIB_ARRAY_SIZE_ : 0;
		this.VERTEX_ATTRIB_ARRAY_STRIDE = VERTEX_ATTRIB_ARRAY_STRIDE_ !== undefined ? VERTEX_ATTRIB_ARRAY_STRIDE_ : 0;
		this.VERTEX_ATTRIB_ARRAY_TYPE = VERTEX_ATTRIB_ARRAY_TYPE_ !== undefined ? VERTEX_ATTRIB_ARRAY_TYPE_ : 0;
		this.VERTEX_SHADER = VERTEX_SHADER_ !== undefined ? VERTEX_SHADER_ : 0;
		this.VIEWPORT = VIEWPORT_ !== undefined ? VIEWPORT_ : 0;
		this.ZERO = ZERO_ !== undefined ? ZERO_ : 0;
	});
	DefaultAttributes = $pkg.DefaultAttributes = function() {
		return new ContextAttributes.Ptr(true, true, false, true, true, false);
	};
	NewContext = $pkg.NewContext = function(canvas, ca) {
		var attrs, _map, _key, gl, ctx;
		if ($global.WebGLRenderingContext === undefined) {
			return [($ptrType(Context)).nil, errors.New("Your browser doesn't appear to support webgl.")];
		}
		if (ca === ($ptrType(ContextAttributes)).nil) {
			ca = DefaultAttributes();
		}
		attrs = (_map = new $Map(), _key = "alpha", _map[_key] = { k: _key, v: ca.Alpha }, _key = "depth", _map[_key] = { k: _key, v: ca.Depth }, _key = "stencil", _map[_key] = { k: _key, v: ca.Stencil }, _key = "antialias", _map[_key] = { k: _key, v: ca.Antialias }, _key = "premultipliedAlpha", _map[_key] = { k: _key, v: ca.PremultipliedAlpha }, _key = "preserveDrawingBuffer", _map[_key] = { k: _key, v: ca.PreserveDrawingBuffer }, _map);
		gl = canvas.getContext($externalize("webgl", $String), $externalize(attrs, ($mapType($String, $Bool))));
		if (gl === null) {
			gl = canvas.getContext($externalize("experimental-webgl", $String), $externalize(attrs, ($mapType($String, $Bool))));
			if (gl === null) {
				return [($ptrType(Context)).nil, errors.New("Creating a webgl context has failed.")];
			}
		}
		ctx = new Context.Ptr();
		ctx.Object = gl;
		return [ctx, null];
	};
	Context.Ptr.prototype.GetContextAttributes = function() {
		var c, ca;
		c = this;
		ca = c.Object.getContextAttributes();
		return new ContextAttributes.Ptr(!!(ca.alpha), !!(ca.depth), !!(ca.stencil), !!(ca.antialias), !!(ca.premultipliedAlpha), !!(ca.preservedDrawingBuffer));
	};
	Context.prototype.GetContextAttributes = function() { return this.$val.GetContextAttributes(); };
	Context.Ptr.prototype.ActiveTexture = function(texture) {
		var c;
		c = this;
		c.Object.activeTexture(texture);
	};
	Context.prototype.ActiveTexture = function(texture) { return this.$val.ActiveTexture(texture); };
	Context.Ptr.prototype.AttachShader = function(program, shader) {
		var c;
		c = this;
		c.Object.attachShader(program, shader);
	};
	Context.prototype.AttachShader = function(program, shader) { return this.$val.AttachShader(program, shader); };
	Context.Ptr.prototype.BindAttribLocation = function(program, index, name) {
		var c;
		c = this;
		c.Object.bindAttribLocation(program, index, $externalize(name, $String));
	};
	Context.prototype.BindAttribLocation = function(program, index, name) { return this.$val.BindAttribLocation(program, index, name); };
	Context.Ptr.prototype.BindBuffer = function(target, buffer) {
		var c;
		c = this;
		c.Object.bindBuffer(target, buffer);
	};
	Context.prototype.BindBuffer = function(target, buffer) { return this.$val.BindBuffer(target, buffer); };
	Context.Ptr.prototype.BindFramebuffer = function(target, framebuffer) {
		var c;
		c = this;
		c.Object.bindFramebuffer(target, framebuffer);
	};
	Context.prototype.BindFramebuffer = function(target, framebuffer) { return this.$val.BindFramebuffer(target, framebuffer); };
	Context.Ptr.prototype.BindRenderbuffer = function(target, renderbuffer) {
		var c;
		c = this;
		c.Object.bindRenderbuffer(target, renderbuffer);
	};
	Context.prototype.BindRenderbuffer = function(target, renderbuffer) { return this.$val.BindRenderbuffer(target, renderbuffer); };
	Context.Ptr.prototype.BindTexture = function(target, texture) {
		var c;
		c = this;
		c.Object.bindTexture(target, texture);
	};
	Context.prototype.BindTexture = function(target, texture) { return this.$val.BindTexture(target, texture); };
	Context.Ptr.prototype.BlendColor = function(r, g, b, a) {
		var c;
		c = this;
		c.Object.blendColor(r, g, b, a);
	};
	Context.prototype.BlendColor = function(r, g, b, a) { return this.$val.BlendColor(r, g, b, a); };
	Context.Ptr.prototype.BlendEquation = function(mode) {
		var c;
		c = this;
		c.Object.blendEquation(mode);
	};
	Context.prototype.BlendEquation = function(mode) { return this.$val.BlendEquation(mode); };
	Context.Ptr.prototype.BlendEquationSeparate = function(modeRGB, modeAlpha) {
		var c;
		c = this;
		c.Object.blendEquationSeparate(modeRGB, modeAlpha);
	};
	Context.prototype.BlendEquationSeparate = function(modeRGB, modeAlpha) { return this.$val.BlendEquationSeparate(modeRGB, modeAlpha); };
	Context.Ptr.prototype.BlendFunc = function(sfactor, dfactor) {
		var c;
		c = this;
		c.Object.blendFunc(sfactor, dfactor);
	};
	Context.prototype.BlendFunc = function(sfactor, dfactor) { return this.$val.BlendFunc(sfactor, dfactor); };
	Context.Ptr.prototype.BlendFuncSeparate = function(srcRGB, dstRGB, srcAlpha, dstAlpha) {
		var c;
		c = this;
		c.Object.blendFuncSeparate(srcRGB, dstRGB, srcAlpha, dstAlpha);
	};
	Context.prototype.BlendFuncSeparate = function(srcRGB, dstRGB, srcAlpha, dstAlpha) { return this.$val.BlendFuncSeparate(srcRGB, dstRGB, srcAlpha, dstAlpha); };
	Context.Ptr.prototype.BufferData = function(target, data, usage) {
		var c;
		c = this;
		c.Object.bufferData(target, data, usage);
	};
	Context.prototype.BufferData = function(target, data, usage) { return this.$val.BufferData(target, data, usage); };
	Context.Ptr.prototype.BufferSubData = function(target, offset, data) {
		var c;
		c = this;
		c.Object.bufferSubData(target, offset, data);
	};
	Context.prototype.BufferSubData = function(target, offset, data) { return this.$val.BufferSubData(target, offset, data); };
	Context.Ptr.prototype.CheckFramebufferStatus = function(target) {
		var c;
		c = this;
		return $parseInt(c.Object.checkFramebufferStatus(target)) >> 0;
	};
	Context.prototype.CheckFramebufferStatus = function(target) { return this.$val.CheckFramebufferStatus(target); };
	Context.Ptr.prototype.Clear = function(flags) {
		var c;
		c = this;
		c.Object.clear(flags);
	};
	Context.prototype.Clear = function(flags) { return this.$val.Clear(flags); };
	Context.Ptr.prototype.ClearColor = function(r, g, b, a) {
		var c;
		c = this;
		c.Object.clearColor(r, g, b, a);
	};
	Context.prototype.ClearColor = function(r, g, b, a) { return this.$val.ClearColor(r, g, b, a); };
	Context.Ptr.prototype.ClearDepth = function(depth) {
		var c;
		c = this;
		c.Object.clearDepth(depth);
	};
	Context.prototype.ClearDepth = function(depth) { return this.$val.ClearDepth(depth); };
	Context.Ptr.prototype.ClearStencil = function(s) {
		var c;
		c = this;
		c.Object.clearStencil(s);
	};
	Context.prototype.ClearStencil = function(s) { return this.$val.ClearStencil(s); };
	Context.Ptr.prototype.ColorMask = function(r, g, b, a) {
		var c;
		c = this;
		c.Object.colorMask($externalize(r, $Bool), $externalize(g, $Bool), $externalize(b, $Bool), $externalize(a, $Bool));
	};
	Context.prototype.ColorMask = function(r, g, b, a) { return this.$val.ColorMask(r, g, b, a); };
	Context.Ptr.prototype.CompileShader = function(shader) {
		var c;
		c = this;
		c.Object.compileShader(shader);
	};
	Context.prototype.CompileShader = function(shader) { return this.$val.CompileShader(shader); };
	Context.Ptr.prototype.CopyTexImage2D = function(target, level, internal, x, y, w, h, border) {
		var c;
		c = this;
		c.Object.copyTexImage2D(target, level, internal, x, y, w, h, border);
	};
	Context.prototype.CopyTexImage2D = function(target, level, internal, x, y, w, h, border) { return this.$val.CopyTexImage2D(target, level, internal, x, y, w, h, border); };
	Context.Ptr.prototype.CopyTexSubImage2D = function(target, level, xoffset, yoffset, x, y, w, h) {
		var c;
		c = this;
		c.Object.copyTexSubImage2D(target, level, xoffset, yoffset, x, y, w, h);
	};
	Context.prototype.CopyTexSubImage2D = function(target, level, xoffset, yoffset, x, y, w, h) { return this.$val.CopyTexSubImage2D(target, level, xoffset, yoffset, x, y, w, h); };
	Context.Ptr.prototype.CreateBuffer = function() {
		var c;
		c = this;
		return c.Object.createBuffer();
	};
	Context.prototype.CreateBuffer = function() { return this.$val.CreateBuffer(); };
	Context.Ptr.prototype.CreateFramebuffer = function() {
		var c;
		c = this;
		return c.Object.createFramebuffer();
	};
	Context.prototype.CreateFramebuffer = function() { return this.$val.CreateFramebuffer(); };
	Context.Ptr.prototype.CreateProgram = function() {
		var c;
		c = this;
		return c.Object.createProgram();
	};
	Context.prototype.CreateProgram = function() { return this.$val.CreateProgram(); };
	Context.Ptr.prototype.CreateRenderbuffer = function() {
		var c;
		c = this;
		return c.Object.createRenderbuffer();
	};
	Context.prototype.CreateRenderbuffer = function() { return this.$val.CreateRenderbuffer(); };
	Context.Ptr.prototype.CreateShader = function(typ) {
		var c;
		c = this;
		return c.Object.createShader(typ);
	};
	Context.prototype.CreateShader = function(typ) { return this.$val.CreateShader(typ); };
	Context.Ptr.prototype.CreateTexture = function() {
		var c;
		c = this;
		return c.Object.createTexture();
	};
	Context.prototype.CreateTexture = function() { return this.$val.CreateTexture(); };
	Context.Ptr.prototype.CullFace = function(mode) {
		var c;
		c = this;
		c.Object.cullFace(mode);
	};
	Context.prototype.CullFace = function(mode) { return this.$val.CullFace(mode); };
	Context.Ptr.prototype.DeleteBuffer = function(buffer) {
		var c;
		c = this;
		c.Object.deleteBuffer(buffer);
	};
	Context.prototype.DeleteBuffer = function(buffer) { return this.$val.DeleteBuffer(buffer); };
	Context.Ptr.prototype.DeleteFramebuffer = function(framebuffer) {
		var c;
		c = this;
		c.Object.deleteFramebuffer(framebuffer);
	};
	Context.prototype.DeleteFramebuffer = function(framebuffer) { return this.$val.DeleteFramebuffer(framebuffer); };
	Context.Ptr.prototype.DeleteProgram = function(program) {
		var c;
		c = this;
		c.Object.deleteProgram(program);
	};
	Context.prototype.DeleteProgram = function(program) { return this.$val.DeleteProgram(program); };
	Context.Ptr.prototype.DeleteRenderbuffer = function(renderbuffer) {
		var c;
		c = this;
		c.Object.deleteRenderbuffer(renderbuffer);
	};
	Context.prototype.DeleteRenderbuffer = function(renderbuffer) { return this.$val.DeleteRenderbuffer(renderbuffer); };
	Context.Ptr.prototype.DeleteShader = function(shader) {
		var c;
		c = this;
		c.Object.deleteShader(shader);
	};
	Context.prototype.DeleteShader = function(shader) { return this.$val.DeleteShader(shader); };
	Context.Ptr.prototype.DeleteTexture = function(texture) {
		var c;
		c = this;
		c.Object.deleteTexture(texture);
	};
	Context.prototype.DeleteTexture = function(texture) { return this.$val.DeleteTexture(texture); };
	Context.Ptr.prototype.DepthFunc = function(fun) {
		var c;
		c = this;
		c.Object.depthFunc(fun);
	};
	Context.prototype.DepthFunc = function(fun) { return this.$val.DepthFunc(fun); };
	Context.Ptr.prototype.DepthMask = function(flag) {
		var c;
		c = this;
		c.Object.depthMask($externalize(flag, $Bool));
	};
	Context.prototype.DepthMask = function(flag) { return this.$val.DepthMask(flag); };
	Context.Ptr.prototype.DepthRange = function(zNear, zFar) {
		var c;
		c = this;
		c.Object.depthRange(zNear, zFar);
	};
	Context.prototype.DepthRange = function(zNear, zFar) { return this.$val.DepthRange(zNear, zFar); };
	Context.Ptr.prototype.DetachShader = function(program, shader) {
		var c;
		c = this;
		c.Object.detachShader(program, shader);
	};
	Context.prototype.DetachShader = function(program, shader) { return this.$val.DetachShader(program, shader); };
	Context.Ptr.prototype.Disable = function(cap) {
		var c;
		c = this;
		c.Object.disable(cap);
	};
	Context.prototype.Disable = function(cap) { return this.$val.Disable(cap); };
	Context.Ptr.prototype.DisableVertexAttribArray = function(index) {
		var c;
		c = this;
		c.Object.disableVertexAttribArray(index);
	};
	Context.prototype.DisableVertexAttribArray = function(index) { return this.$val.DisableVertexAttribArray(index); };
	Context.Ptr.prototype.DrawArrays = function(mode, first, count) {
		var c;
		c = this;
		c.Object.drawArrays(mode, first, count);
	};
	Context.prototype.DrawArrays = function(mode, first, count) { return this.$val.DrawArrays(mode, first, count); };
	Context.Ptr.prototype.DrawElements = function(mode, count, typ, offset) {
		var c;
		c = this;
		c.Object.drawElements(mode, count, typ, offset);
	};
	Context.prototype.DrawElements = function(mode, count, typ, offset) { return this.$val.DrawElements(mode, count, typ, offset); };
	Context.Ptr.prototype.Enable = function(cap) {
		var c;
		c = this;
		c.Object.enable(cap);
	};
	Context.prototype.Enable = function(cap) { return this.$val.Enable(cap); };
	Context.Ptr.prototype.EnableVertexAttribArray = function(index) {
		var c;
		c = this;
		c.Object.enableVertexAttribArray(index);
	};
	Context.prototype.EnableVertexAttribArray = function(index) { return this.$val.EnableVertexAttribArray(index); };
	Context.Ptr.prototype.Finish = function() {
		var c;
		c = this;
		c.Object.finish();
	};
	Context.prototype.Finish = function() { return this.$val.Finish(); };
	Context.Ptr.prototype.Flush = function() {
		var c;
		c = this;
		c.Object.flush();
	};
	Context.prototype.Flush = function() { return this.$val.Flush(); };
	Context.Ptr.prototype.FrameBufferRenderBuffer = function(target, attachment, renderbufferTarget, renderbuffer) {
		var c;
		c = this;
		c.Object.framebufferRenderBuffer(target, attachment, renderbufferTarget, renderbuffer);
	};
	Context.prototype.FrameBufferRenderBuffer = function(target, attachment, renderbufferTarget, renderbuffer) { return this.$val.FrameBufferRenderBuffer(target, attachment, renderbufferTarget, renderbuffer); };
	Context.Ptr.prototype.FramebufferTexture2D = function(target, attachment, textarget, texture, level) {
		var c;
		c = this;
		c.Object.framebufferTexture2D(target, attachment, textarget, texture, level);
	};
	Context.prototype.FramebufferTexture2D = function(target, attachment, textarget, texture, level) { return this.$val.FramebufferTexture2D(target, attachment, textarget, texture, level); };
	Context.Ptr.prototype.FrontFace = function(mode) {
		var c;
		c = this;
		c.Object.frontFace(mode);
	};
	Context.prototype.FrontFace = function(mode) { return this.$val.FrontFace(mode); };
	Context.Ptr.prototype.GenerateMipmap = function(target) {
		var c;
		c = this;
		c.Object.generateMipmap(target);
	};
	Context.prototype.GenerateMipmap = function(target) { return this.$val.GenerateMipmap(target); };
	Context.Ptr.prototype.GetActiveAttrib = function(program, index) {
		var c;
		c = this;
		return c.Object.getActiveAttrib(program, index);
	};
	Context.prototype.GetActiveAttrib = function(program, index) { return this.$val.GetActiveAttrib(program, index); };
	Context.Ptr.prototype.GetActiveUniform = function(program, index) {
		var c;
		c = this;
		return c.Object.getActiveUniform(program, index);
	};
	Context.prototype.GetActiveUniform = function(program, index) { return this.$val.GetActiveUniform(program, index); };
	Context.Ptr.prototype.GetAttachedShaders = function(program) {
		var c, objs, shaders, i;
		c = this;
		objs = c.Object.getAttachedShaders(program);
		shaders = ($sliceType(js.Object)).make($parseInt(objs.length));
		i = 0;
		while (i < $parseInt(objs.length)) {
			(i < 0 || i >= shaders.$length) ? $throwRuntimeError("index out of range") : shaders.$array[shaders.$offset + i] = objs[i];
			i = i + (1) >> 0;
		}
		return shaders;
	};
	Context.prototype.GetAttachedShaders = function(program) { return this.$val.GetAttachedShaders(program); };
	Context.Ptr.prototype.GetAttribLocation = function(program, name) {
		var c;
		c = this;
		return $parseInt(c.Object.getAttribLocation(program, $externalize(name, $String))) >> 0;
	};
	Context.prototype.GetAttribLocation = function(program, name) { return this.$val.GetAttribLocation(program, name); };
	Context.Ptr.prototype.GetBufferParameter = function(target, pname) {
		var c;
		c = this;
		return c.Object.getBufferParameter(target, pname);
	};
	Context.prototype.GetBufferParameter = function(target, pname) { return this.$val.GetBufferParameter(target, pname); };
	Context.Ptr.prototype.GetParameter = function(pname) {
		var c;
		c = this;
		return c.Object.getParameter(pname);
	};
	Context.prototype.GetParameter = function(pname) { return this.$val.GetParameter(pname); };
	Context.Ptr.prototype.GetError = function() {
		var c;
		c = this;
		return $parseInt(c.Object.getError()) >> 0;
	};
	Context.prototype.GetError = function() { return this.$val.GetError(); };
	Context.Ptr.prototype.GetExtension = function(name) {
		var c;
		c = this;
		return c.Object.getExtension($externalize(name, $String));
	};
	Context.prototype.GetExtension = function(name) { return this.$val.GetExtension(name); };
	Context.Ptr.prototype.GetFramebufferAttachmentParameter = function(target, attachment, pname) {
		var c;
		c = this;
		return c.Object.getFramebufferAttachmentParameter(target, attachment, pname);
	};
	Context.prototype.GetFramebufferAttachmentParameter = function(target, attachment, pname) { return this.$val.GetFramebufferAttachmentParameter(target, attachment, pname); };
	Context.Ptr.prototype.GetProgramParameteri = function(program, pname) {
		var c;
		c = this;
		return $parseInt(c.Object.getProgramParameter(program, pname)) >> 0;
	};
	Context.prototype.GetProgramParameteri = function(program, pname) { return this.$val.GetProgramParameteri(program, pname); };
	Context.Ptr.prototype.GetProgramParameterb = function(program, pname) {
		var c;
		c = this;
		return !!(c.Object.getProgramParameter(program, pname));
	};
	Context.prototype.GetProgramParameterb = function(program, pname) { return this.$val.GetProgramParameterb(program, pname); };
	Context.Ptr.prototype.GetProgramInfoLog = function(program) {
		var c;
		c = this;
		return $internalize(c.Object.getProgramInfoLog(program), $String);
	};
	Context.prototype.GetProgramInfoLog = function(program) { return this.$val.GetProgramInfoLog(program); };
	Context.Ptr.prototype.GetRenderbufferParameter = function(target, pname) {
		var c;
		c = this;
		return c.Object.getRenderbufferParameter(target, pname);
	};
	Context.prototype.GetRenderbufferParameter = function(target, pname) { return this.$val.GetRenderbufferParameter(target, pname); };
	Context.Ptr.prototype.GetShaderParameter = function(shader, pname) {
		var c;
		c = this;
		return c.Object.getShaderParameter(shader, pname);
	};
	Context.prototype.GetShaderParameter = function(shader, pname) { return this.$val.GetShaderParameter(shader, pname); };
	Context.Ptr.prototype.GetShaderParameterb = function(shader, pname) {
		var c;
		c = this;
		return !!(c.Object.getShaderParameter(shader, pname));
	};
	Context.prototype.GetShaderParameterb = function(shader, pname) { return this.$val.GetShaderParameterb(shader, pname); };
	Context.Ptr.prototype.GetShaderInfoLog = function(shader) {
		var c;
		c = this;
		return $internalize(c.Object.getShaderInfoLog(shader), $String);
	};
	Context.prototype.GetShaderInfoLog = function(shader) { return this.$val.GetShaderInfoLog(shader); };
	Context.Ptr.prototype.GetShaderSource = function(shader) {
		var c;
		c = this;
		return $internalize(c.Object.getShaderSource(shader), $String);
	};
	Context.prototype.GetShaderSource = function(shader) { return this.$val.GetShaderSource(shader); };
	Context.Ptr.prototype.GetSupportedExtensions = function() {
		var c, ext, extensions, i;
		c = this;
		ext = c.Object.getSupportedExtensions();
		extensions = ($sliceType($String)).make($parseInt(ext.length));
		i = 0;
		while (i < $parseInt(ext.length)) {
			(i < 0 || i >= extensions.$length) ? $throwRuntimeError("index out of range") : extensions.$array[extensions.$offset + i] = $internalize(ext[i], $String);
			i = i + (1) >> 0;
		}
		return extensions;
	};
	Context.prototype.GetSupportedExtensions = function() { return this.$val.GetSupportedExtensions(); };
	Context.Ptr.prototype.GetTexParameter = function(target, pname) {
		var c;
		c = this;
		return c.Object.getTexParameter(target, pname);
	};
	Context.prototype.GetTexParameter = function(target, pname) { return this.$val.GetTexParameter(target, pname); };
	Context.Ptr.prototype.GetUniform = function(program, location) {
		var c;
		c = this;
		return c.Object.getUniform(program, location);
	};
	Context.prototype.GetUniform = function(program, location) { return this.$val.GetUniform(program, location); };
	Context.Ptr.prototype.GetUniformLocation = function(program, name) {
		var c;
		c = this;
		return c.Object.getUniformLocation(program, $externalize(name, $String));
	};
	Context.prototype.GetUniformLocation = function(program, name) { return this.$val.GetUniformLocation(program, name); };
	Context.Ptr.prototype.GetVertexAttrib = function(index, pname) {
		var c;
		c = this;
		return c.Object.getVertexAttrib(index, pname);
	};
	Context.prototype.GetVertexAttrib = function(index, pname) { return this.$val.GetVertexAttrib(index, pname); };
	Context.Ptr.prototype.GetVertexAttribOffset = function(index, pname) {
		var c;
		c = this;
		return $parseInt(c.Object.getVertexAttribOffset(index, pname)) >> 0;
	};
	Context.prototype.GetVertexAttribOffset = function(index, pname) { return this.$val.GetVertexAttribOffset(index, pname); };
	Context.Ptr.prototype.IsBuffer = function(buffer) {
		var c;
		c = this;
		return !!(c.Object.isBuffer(buffer));
	};
	Context.prototype.IsBuffer = function(buffer) { return this.$val.IsBuffer(buffer); };
	Context.Ptr.prototype.IsContextLost = function() {
		var c;
		c = this;
		return !!(c.Object.isContextLost());
	};
	Context.prototype.IsContextLost = function() { return this.$val.IsContextLost(); };
	Context.Ptr.prototype.IsFramebuffer = function(framebuffer) {
		var c;
		c = this;
		return !!(c.Object.isFramebuffer(framebuffer));
	};
	Context.prototype.IsFramebuffer = function(framebuffer) { return this.$val.IsFramebuffer(framebuffer); };
	Context.Ptr.prototype.IsProgram = function(program) {
		var c;
		c = this;
		return !!(c.Object.isProgram(program));
	};
	Context.prototype.IsProgram = function(program) { return this.$val.IsProgram(program); };
	Context.Ptr.prototype.IsRenderbuffer = function(renderbuffer) {
		var c;
		c = this;
		return !!(c.Object.isRenderbuffer(renderbuffer));
	};
	Context.prototype.IsRenderbuffer = function(renderbuffer) { return this.$val.IsRenderbuffer(renderbuffer); };
	Context.Ptr.prototype.IsShader = function(shader) {
		var c;
		c = this;
		return !!(c.Object.isShader(shader));
	};
	Context.prototype.IsShader = function(shader) { return this.$val.IsShader(shader); };
	Context.Ptr.prototype.IsTexture = function(texture) {
		var c;
		c = this;
		return !!(c.Object.isTexture(texture));
	};
	Context.prototype.IsTexture = function(texture) { return this.$val.IsTexture(texture); };
	Context.Ptr.prototype.IsEnabled = function(capability) {
		var c;
		c = this;
		return !!(c.Object.isEnabled(capability));
	};
	Context.prototype.IsEnabled = function(capability) { return this.$val.IsEnabled(capability); };
	Context.Ptr.prototype.LineWidth = function(width) {
		var c;
		c = this;
		c.Object.lineWidth(width);
	};
	Context.prototype.LineWidth = function(width) { return this.$val.LineWidth(width); };
	Context.Ptr.prototype.LinkProgram = function(program) {
		var c;
		c = this;
		c.Object.linkProgram(program);
	};
	Context.prototype.LinkProgram = function(program) { return this.$val.LinkProgram(program); };
	Context.Ptr.prototype.PixelStorei = function(pname, param) {
		var c;
		c = this;
		c.Object.pixelStorei(pname, param);
	};
	Context.prototype.PixelStorei = function(pname, param) { return this.$val.PixelStorei(pname, param); };
	Context.Ptr.prototype.PolygonOffset = function(factor, units) {
		var c;
		c = this;
		c.Object.polygonOffset(factor, units);
	};
	Context.prototype.PolygonOffset = function(factor, units) { return this.$val.PolygonOffset(factor, units); };
	Context.Ptr.prototype.ReadPixels = function(x, y, width, height, format, typ, pixels) {
		var c;
		c = this;
		c.Object.readPixels(x, y, width, height, format, typ, pixels);
	};
	Context.prototype.ReadPixels = function(x, y, width, height, format, typ, pixels) { return this.$val.ReadPixels(x, y, width, height, format, typ, pixels); };
	Context.Ptr.prototype.RenderbufferStorage = function(target, internalFormat, width, height) {
		var c;
		c = this;
		c.Object.renderbufferStorage(target, internalFormat, width, height);
	};
	Context.prototype.RenderbufferStorage = function(target, internalFormat, width, height) { return this.$val.RenderbufferStorage(target, internalFormat, width, height); };
	Context.Ptr.prototype.Scissor = function(x, y, width, height) {
		var c;
		c = this;
		c.Object.scissor(x, y, width, height);
	};
	Context.prototype.Scissor = function(x, y, width, height) { return this.$val.Scissor(x, y, width, height); };
	Context.Ptr.prototype.ShaderSource = function(shader, source) {
		var c;
		c = this;
		c.Object.shaderSource(shader, $externalize(source, $String));
	};
	Context.prototype.ShaderSource = function(shader, source) { return this.$val.ShaderSource(shader, source); };
	Context.Ptr.prototype.TexImage2D = function(target, level, internalFormat, format, kind, image) {
		var c;
		c = this;
		c.Object.texImage2D(target, level, internalFormat, format, kind, image);
	};
	Context.prototype.TexImage2D = function(target, level, internalFormat, format, kind, image) { return this.$val.TexImage2D(target, level, internalFormat, format, kind, image); };
	Context.Ptr.prototype.TexParameteri = function(target, pname, param) {
		var c;
		c = this;
		c.Object.texParameteri(target, pname, param);
	};
	Context.prototype.TexParameteri = function(target, pname, param) { return this.$val.TexParameteri(target, pname, param); };
	Context.Ptr.prototype.TexSubImage2D = function(target, level, xoffset, yoffset, format, typ, image) {
		var c;
		c = this;
		c.Object.texSubImage2D(target, level, xoffset, yoffset, format, typ, image);
	};
	Context.prototype.TexSubImage2D = function(target, level, xoffset, yoffset, format, typ, image) { return this.$val.TexSubImage2D(target, level, xoffset, yoffset, format, typ, image); };
	Context.Ptr.prototype.Uniform1f = function(location, x) {
		var c;
		c = this;
		c.Object.uniform1f(location, x);
	};
	Context.prototype.Uniform1f = function(location, x) { return this.$val.Uniform1f(location, x); };
	Context.Ptr.prototype.Uniform1i = function(location, x) {
		var c;
		c = this;
		c.Object.uniform1i(location, x);
	};
	Context.prototype.Uniform1i = function(location, x) { return this.$val.Uniform1i(location, x); };
	Context.Ptr.prototype.Uniform2f = function(location, x, y) {
		var c;
		c = this;
		c.Object.uniform2f(location, x, y);
	};
	Context.prototype.Uniform2f = function(location, x, y) { return this.$val.Uniform2f(location, x, y); };
	Context.Ptr.prototype.Uniform2i = function(location, x, y) {
		var c;
		c = this;
		c.Object.uniform2i(location, x, y);
	};
	Context.prototype.Uniform2i = function(location, x, y) { return this.$val.Uniform2i(location, x, y); };
	Context.Ptr.prototype.Uniform3f = function(location, x, y, z) {
		var c;
		c = this;
		c.Object.uniform3f(location, x, y, z);
	};
	Context.prototype.Uniform3f = function(location, x, y, z) { return this.$val.Uniform3f(location, x, y, z); };
	Context.Ptr.prototype.Uniform3i = function(location, x, y, z) {
		var c;
		c = this;
		c.Object.uniform3i(location, x, y, z);
	};
	Context.prototype.Uniform3i = function(location, x, y, z) { return this.$val.Uniform3i(location, x, y, z); };
	Context.Ptr.prototype.Uniform4f = function(location, x, y, z, w) {
		var c;
		c = this;
		c.Object.uniform4f(location, x, y, z, w);
	};
	Context.prototype.Uniform4f = function(location, x, y, z, w) { return this.$val.Uniform4f(location, x, y, z, w); };
	Context.Ptr.prototype.Uniform4i = function(location, x, y, z, w) {
		var c;
		c = this;
		c.Object.uniform4i(location, x, y, z, w);
	};
	Context.prototype.Uniform4i = function(location, x, y, z, w) { return this.$val.Uniform4i(location, x, y, z, w); };
	Context.Ptr.prototype.UniformMatrix2fv = function(location, transpose, value) {
		var c;
		c = this;
		c.Object.uniformMatrix2fv(location, $externalize(transpose, $Bool), $externalize(value, ($sliceType($Float32))));
	};
	Context.prototype.UniformMatrix2fv = function(location, transpose, value) { return this.$val.UniformMatrix2fv(location, transpose, value); };
	Context.Ptr.prototype.UniformMatrix3fv = function(location, transpose, value) {
		var c;
		c = this;
		c.Object.uniformMatrix3fv(location, $externalize(transpose, $Bool), $externalize(value, ($sliceType($Float32))));
	};
	Context.prototype.UniformMatrix3fv = function(location, transpose, value) { return this.$val.UniformMatrix3fv(location, transpose, value); };
	Context.Ptr.prototype.UniformMatrix4fv = function(location, transpose, value) {
		var c;
		c = this;
		c.Object.uniformMatrix4fv(location, $externalize(transpose, $Bool), $externalize(value, ($sliceType($Float32))));
	};
	Context.prototype.UniformMatrix4fv = function(location, transpose, value) { return this.$val.UniformMatrix4fv(location, transpose, value); };
	Context.Ptr.prototype.UseProgram = function(program) {
		var c;
		c = this;
		c.Object.useProgram(program);
	};
	Context.prototype.UseProgram = function(program) { return this.$val.UseProgram(program); };
	Context.Ptr.prototype.ValidateProgram = function(program) {
		var c;
		c = this;
		c.Object.validateProgram(program);
	};
	Context.prototype.ValidateProgram = function(program) { return this.$val.ValidateProgram(program); };
	Context.Ptr.prototype.VertexAttribPointer = function(index, size, typ, normal, stride, offset) {
		var c;
		c = this;
		c.Object.vertexAttribPointer(index, size, typ, $externalize(normal, $Bool), stride, offset);
	};
	Context.prototype.VertexAttribPointer = function(index, size, typ, normal, stride, offset) { return this.$val.VertexAttribPointer(index, size, typ, normal, stride, offset); };
	Context.Ptr.prototype.Viewport = function(x, y, width, height) {
		var c;
		c = this;
		c.Object.viewport(x, y, width, height);
	};
	Context.prototype.Viewport = function(x, y, width, height) { return this.$val.Viewport(x, y, width, height); };
	$pkg.$init = function() {
		ContextAttributes.init([["Alpha", "Alpha", "", $Bool, ""], ["Depth", "Depth", "", $Bool, ""], ["Stencil", "Stencil", "", $Bool, ""], ["Antialias", "Antialias", "", $Bool, ""], ["PremultipliedAlpha", "PremultipliedAlpha", "", $Bool, ""], ["PreserveDrawingBuffer", "PreserveDrawingBuffer", "", $Bool, ""]]);
		Context.methods = [["Bool", "Bool", "", [], [$Bool], false, 0], ["Call", "Call", "", [$String, ($sliceType($emptyInterface))], [js.Object], true, 0], ["Delete", "Delete", "", [$String], [], false, 0], ["Float", "Float", "", [], [$Float64], false, 0], ["Get", "Get", "", [$String], [js.Object], false, 0], ["Index", "Index", "", [$Int], [js.Object], false, 0], ["Int", "Int", "", [], [$Int], false, 0], ["Int64", "Int64", "", [], [$Int64], false, 0], ["Interface", "Interface", "", [], [$emptyInterface], false, 0], ["Invoke", "Invoke", "", [($sliceType($emptyInterface))], [js.Object], true, 0], ["IsNull", "IsNull", "", [], [$Bool], false, 0], ["IsUndefined", "IsUndefined", "", [], [$Bool], false, 0], ["Length", "Length", "", [], [$Int], false, 0], ["New", "New", "", [($sliceType($emptyInterface))], [js.Object], true, 0], ["Set", "Set", "", [$String, $emptyInterface], [], false, 0], ["SetIndex", "SetIndex", "", [$Int, $emptyInterface], [], false, 0], ["Str", "Str", "", [], [$String], false, 0], ["Uint64", "Uint64", "", [], [$Uint64], false, 0], ["Unsafe", "Unsafe", "", [], [$Uintptr], false, 0]];
		($ptrType(Context)).methods = [["ActiveTexture", "ActiveTexture", "", [$Int], [], false, -1], ["AttachShader", "AttachShader", "", [js.Object, js.Object], [], false, -1], ["BindAttribLocation", "BindAttribLocation", "", [js.Object, $Int, $String], [], false, -1], ["BindBuffer", "BindBuffer", "", [$Int, js.Object], [], false, -1], ["BindFramebuffer", "BindFramebuffer", "", [$Int, js.Object], [], false, -1], ["BindRenderbuffer", "BindRenderbuffer", "", [$Int, js.Object], [], false, -1], ["BindTexture", "BindTexture", "", [$Int, js.Object], [], false, -1], ["BlendColor", "BlendColor", "", [$Float64, $Float64, $Float64, $Float64], [], false, -1], ["BlendEquation", "BlendEquation", "", [$Int], [], false, -1], ["BlendEquationSeparate", "BlendEquationSeparate", "", [$Int, $Int], [], false, -1], ["BlendFunc", "BlendFunc", "", [$Int, $Int], [], false, -1], ["BlendFuncSeparate", "BlendFuncSeparate", "", [$Int, $Int, $Int, $Int], [], false, -1], ["Bool", "Bool", "", [], [$Bool], false, 0], ["BufferData", "BufferData", "", [$Int, js.Object, $Int], [], false, -1], ["BufferSubData", "BufferSubData", "", [$Int, $Int, js.Object], [], false, -1], ["Call", "Call", "", [$String, ($sliceType($emptyInterface))], [js.Object], true, 0], ["CheckFramebufferStatus", "CheckFramebufferStatus", "", [$Int], [$Int], false, -1], ["Clear", "Clear", "", [$Int], [], false, -1], ["ClearColor", "ClearColor", "", [$Float32, $Float32, $Float32, $Float32], [], false, -1], ["ClearDepth", "ClearDepth", "", [$Float64], [], false, -1], ["ClearStencil", "ClearStencil", "", [$Int], [], false, -1], ["ColorMask", "ColorMask", "", [$Bool, $Bool, $Bool, $Bool], [], false, -1], ["CompileShader", "CompileShader", "", [js.Object], [], false, -1], ["CopyTexImage2D", "CopyTexImage2D", "", [$Int, $Int, $Int, $Int, $Int, $Int, $Int, $Int], [], false, -1], ["CopyTexSubImage2D", "CopyTexSubImage2D", "", [$Int, $Int, $Int, $Int, $Int, $Int, $Int, $Int], [], false, -1], ["CreateBuffer", "CreateBuffer", "", [], [js.Object], false, -1], ["CreateFramebuffer", "CreateFramebuffer", "", [], [js.Object], false, -1], ["CreateProgram", "CreateProgram", "", [], [js.Object], false, -1], ["CreateRenderbuffer", "CreateRenderbuffer", "", [], [js.Object], false, -1], ["CreateShader", "CreateShader", "", [$Int], [js.Object], false, -1], ["CreateTexture", "CreateTexture", "", [], [js.Object], false, -1], ["CullFace", "CullFace", "", [$Int], [], false, -1], ["Delete", "Delete", "", [$String], [], false, 0], ["DeleteBuffer", "DeleteBuffer", "", [js.Object], [], false, -1], ["DeleteFramebuffer", "DeleteFramebuffer", "", [js.Object], [], false, -1], ["DeleteProgram", "DeleteProgram", "", [js.Object], [], false, -1], ["DeleteRenderbuffer", "DeleteRenderbuffer", "", [js.Object], [], false, -1], ["DeleteShader", "DeleteShader", "", [js.Object], [], false, -1], ["DeleteTexture", "DeleteTexture", "", [js.Object], [], false, -1], ["DepthFunc", "DepthFunc", "", [$Int], [], false, -1], ["DepthMask", "DepthMask", "", [$Bool], [], false, -1], ["DepthRange", "DepthRange", "", [$Float64, $Float64], [], false, -1], ["DetachShader", "DetachShader", "", [js.Object, js.Object], [], false, -1], ["Disable", "Disable", "", [$Int], [], false, -1], ["DisableVertexAttribArray", "DisableVertexAttribArray", "", [$Int], [], false, -1], ["DrawArrays", "DrawArrays", "", [$Int, $Int, $Int], [], false, -1], ["DrawElements", "DrawElements", "", [$Int, $Int, $Int, $Int], [], false, -1], ["Enable", "Enable", "", [$Int], [], false, -1], ["EnableVertexAttribArray", "EnableVertexAttribArray", "", [$Int], [], false, -1], ["Finish", "Finish", "", [], [], false, -1], ["Float", "Float", "", [], [$Float64], false, 0], ["Flush", "Flush", "", [], [], false, -1], ["FrameBufferRenderBuffer", "FrameBufferRenderBuffer", "", [$Int, $Int, $Int, js.Object], [], false, -1], ["FramebufferTexture2D", "FramebufferTexture2D", "", [$Int, $Int, $Int, js.Object, $Int], [], false, -1], ["FrontFace", "FrontFace", "", [$Int], [], false, -1], ["GenerateMipmap", "GenerateMipmap", "", [$Int], [], false, -1], ["Get", "Get", "", [$String], [js.Object], false, 0], ["GetActiveAttrib", "GetActiveAttrib", "", [js.Object, $Int], [js.Object], false, -1], ["GetActiveUniform", "GetActiveUniform", "", [js.Object, $Int], [js.Object], false, -1], ["GetAttachedShaders", "GetAttachedShaders", "", [js.Object], [($sliceType(js.Object))], false, -1], ["GetAttribLocation", "GetAttribLocation", "", [js.Object, $String], [$Int], false, -1], ["GetBufferParameter", "GetBufferParameter", "", [$Int, $Int], [js.Object], false, -1], ["GetContextAttributes", "GetContextAttributes", "", [], [ContextAttributes], false, -1], ["GetError", "GetError", "", [], [$Int], false, -1], ["GetExtension", "GetExtension", "", [$String], [js.Object], false, -1], ["GetFramebufferAttachmentParameter", "GetFramebufferAttachmentParameter", "", [$Int, $Int, $Int], [js.Object], false, -1], ["GetParameter", "GetParameter", "", [$Int], [js.Object], false, -1], ["GetProgramInfoLog", "GetProgramInfoLog", "", [js.Object], [$String], false, -1], ["GetProgramParameterb", "GetProgramParameterb", "", [js.Object, $Int], [$Bool], false, -1], ["GetProgramParameteri", "GetProgramParameteri", "", [js.Object, $Int], [$Int], false, -1], ["GetRenderbufferParameter", "GetRenderbufferParameter", "", [$Int, $Int], [js.Object], false, -1], ["GetShaderInfoLog", "GetShaderInfoLog", "", [js.Object], [$String], false, -1], ["GetShaderParameter", "GetShaderParameter", "", [js.Object, $Int], [js.Object], false, -1], ["GetShaderParameterb", "GetShaderParameterb", "", [js.Object, $Int], [$Bool], false, -1], ["GetShaderSource", "GetShaderSource", "", [js.Object], [$String], false, -1], ["GetSupportedExtensions", "GetSupportedExtensions", "", [], [($sliceType($String))], false, -1], ["GetTexParameter", "GetTexParameter", "", [$Int, $Int], [js.Object], false, -1], ["GetUniform", "GetUniform", "", [js.Object, js.Object], [js.Object], false, -1], ["GetUniformLocation", "GetUniformLocation", "", [js.Object, $String], [js.Object], false, -1], ["GetVertexAttrib", "GetVertexAttrib", "", [$Int, $Int], [js.Object], false, -1], ["GetVertexAttribOffset", "GetVertexAttribOffset", "", [$Int, $Int], [$Int], false, -1], ["Index", "Index", "", [$Int], [js.Object], false, 0], ["Int", "Int", "", [], [$Int], false, 0], ["Int64", "Int64", "", [], [$Int64], false, 0], ["Interface", "Interface", "", [], [$emptyInterface], false, 0], ["Invoke", "Invoke", "", [($sliceType($emptyInterface))], [js.Object], true, 0], ["IsBuffer", "IsBuffer", "", [js.Object], [$Bool], false, -1], ["IsContextLost", "IsContextLost", "", [], [$Bool], false, -1], ["IsEnabled", "IsEnabled", "", [$Int], [$Bool], false, -1], ["IsFramebuffer", "IsFramebuffer", "", [js.Object], [$Bool], false, -1], ["IsNull", "IsNull", "", [], [$Bool], false, 0], ["IsProgram", "IsProgram", "", [js.Object], [$Bool], false, -1], ["IsRenderbuffer", "IsRenderbuffer", "", [js.Object], [$Bool], false, -1], ["IsShader", "IsShader", "", [js.Object], [$Bool], false, -1], ["IsTexture", "IsTexture", "", [js.Object], [$Bool], false, -1], ["IsUndefined", "IsUndefined", "", [], [$Bool], false, 0], ["Length", "Length", "", [], [$Int], false, 0], ["LineWidth", "LineWidth", "", [$Float64], [], false, -1], ["LinkProgram", "LinkProgram", "", [js.Object], [], false, -1], ["New", "New", "", [($sliceType($emptyInterface))], [js.Object], true, 0], ["PixelStorei", "PixelStorei", "", [$Int, $Int], [], false, -1], ["PolygonOffset", "PolygonOffset", "", [$Float64, $Float64], [], false, -1], ["ReadPixels", "ReadPixels", "", [$Int, $Int, $Int, $Int, $Int, $Int, js.Object], [], false, -1], ["RenderbufferStorage", "RenderbufferStorage", "", [$Int, $Int, $Int, $Int], [], false, -1], ["Scissor", "Scissor", "", [$Int, $Int, $Int, $Int], [], false, -1], ["Set", "Set", "", [$String, $emptyInterface], [], false, 0], ["SetIndex", "SetIndex", "", [$Int, $emptyInterface], [], false, 0], ["ShaderSource", "ShaderSource", "", [js.Object, $String], [], false, -1], ["Str", "Str", "", [], [$String], false, 0], ["TexImage2D", "TexImage2D", "", [$Int, $Int, $Int, $Int, $Int, js.Object], [], false, -1], ["TexParameteri", "TexParameteri", "", [$Int, $Int, $Int], [], false, -1], ["TexSubImage2D", "TexSubImage2D", "", [$Int, $Int, $Int, $Int, $Int, $Int, js.Object], [], false, -1], ["Uint64", "Uint64", "", [], [$Uint64], false, 0], ["Uniform1f", "Uniform1f", "", [js.Object, $Float32], [], false, -1], ["Uniform1i", "Uniform1i", "", [js.Object, $Int], [], false, -1], ["Uniform2f", "Uniform2f", "", [js.Object, $Float32, $Float32], [], false, -1], ["Uniform2i", "Uniform2i", "", [js.Object, $Int, $Int], [], false, -1], ["Uniform3f", "Uniform3f", "", [js.Object, $Float32, $Float32, $Float32], [], false, -1], ["Uniform3i", "Uniform3i", "", [js.Object, $Int, $Int, $Int], [], false, -1], ["Uniform4f", "Uniform4f", "", [js.Object, $Float32, $Float32, $Float32, $Float32], [], false, -1], ["Uniform4i", "Uniform4i", "", [js.Object, $Int, $Int, $Int, $Int], [], false, -1], ["UniformMatrix2fv", "UniformMatrix2fv", "", [js.Object, $Bool, ($sliceType($Float32))], [], false, -1], ["UniformMatrix3fv", "UniformMatrix3fv", "", [js.Object, $Bool, ($sliceType($Float32))], [], false, -1], ["UniformMatrix4fv", "UniformMatrix4fv", "", [js.Object, $Bool, ($sliceType($Float32))], [], false, -1], ["Unsafe", "Unsafe", "", [], [$Uintptr], false, 0], ["UseProgram", "UseProgram", "", [js.Object], [], false, -1], ["ValidateProgram", "ValidateProgram", "", [js.Object], [], false, -1], ["VertexAttribPointer", "VertexAttribPointer", "", [$Int, $Int, $Int, $Bool, $Int, $Int], [], false, -1], ["Viewport", "Viewport", "", [$Int, $Int, $Int, $Int], [], false, -1]];
		Context.init([["Object", "", "", js.Object, ""], ["ARRAY_BUFFER", "ARRAY_BUFFER", "", $Int, "js:\"ARRAY_BUFFER\""], ["ARRAY_BUFFER_BINDING", "ARRAY_BUFFER_BINDING", "", $Int, "js:\"ARRAY_BUFFER_BINDING\""], ["ATTACHED_SHADERS", "ATTACHED_SHADERS", "", $Int, "js:\"ATTACHED_SHADERS\""], ["BACK", "BACK", "", $Int, "js:\"BACK\""], ["BLEND", "BLEND", "", $Int, "js:\"BLEND\""], ["BLEND_COLOR", "BLEND_COLOR", "", $Int, "js:\"BLEND_COLOR\""], ["BLEND_DST_ALPHA", "BLEND_DST_ALPHA", "", $Int, "js:\"BLEND_DST_ALPHA\""], ["BLEND_DST_RGB", "BLEND_DST_RGB", "", $Int, "js:\"BLEND_DST_RGB\""], ["BLEND_EQUATION", "BLEND_EQUATION", "", $Int, "js:\"BLEND_EQUATION\""], ["BLEND_EQUATION_ALPHA", "BLEND_EQUATION_ALPHA", "", $Int, "js:\"BLEND_EQUATION_ALPHA\""], ["BLEND_EQUATION_RGB", "BLEND_EQUATION_RGB", "", $Int, "js:\"BLEND_EQUATION_RGB\""], ["BLEND_SRC_ALPHA", "BLEND_SRC_ALPHA", "", $Int, "js:\"BLEND_SRC_ALPHA\""], ["BLEND_SRC_RGB", "BLEND_SRC_RGB", "", $Int, "js:\"BLEND_SRC_RGB\""], ["BLUE_BITS", "BLUE_BITS", "", $Int, "js:\"BLUE_BITS\""], ["BOOL", "BOOL", "", $Int, "js:\"BOOL\""], ["BOOL_VEC2", "BOOL_VEC2", "", $Int, "js:\"BOOL_VEC2\""], ["BOOL_VEC3", "BOOL_VEC3", "", $Int, "js:\"BOOL_VEC3\""], ["BOOL_VEC4", "BOOL_VEC4", "", $Int, "js:\"BOOL_VEC4\""], ["BROWSER_DEFAULT_WEBGL", "BROWSER_DEFAULT_WEBGL", "", $Int, "js:\"BROWSER_DEFAULT_WEBGL\""], ["BUFFER_SIZE", "BUFFER_SIZE", "", $Int, "js:\"BUFFER_SIZE\""], ["BUFFER_USAGE", "BUFFER_USAGE", "", $Int, "js:\"BUFFER_USAGE\""], ["BYTE", "BYTE", "", $Int, "js:\"BYTE\""], ["CCW", "CCW", "", $Int, "js:\"CCW\""], ["CLAMP_TO_EDGE", "CLAMP_TO_EDGE", "", $Int, "js:\"CLAMP_TO_EDGE\""], ["COLOR_ATTACHMENT0", "COLOR_ATTACHMENT0", "", $Int, "js:\"COLOR_ATTACHMENT0\""], ["COLOR_BUFFER_BIT", "COLOR_BUFFER_BIT", "", $Int, "js:\"COLOR_BUFFER_BIT\""], ["COLOR_CLEAR_VALUE", "COLOR_CLEAR_VALUE", "", $Int, "js:\"COLOR_CLEAR_VALUE\""], ["COLOR_WRITEMASK", "COLOR_WRITEMASK", "", $Int, "js:\"COLOR_WRITEMASK\""], ["COMPILE_STATUS", "COMPILE_STATUS", "", $Int, "js:\"COMPILE_STATUS\""], ["COMPRESSED_TEXTURE_FORMATS", "COMPRESSED_TEXTURE_FORMATS", "", $Int, "js:\"COMPRESSED_TEXTURE_FORMATS\""], ["CONSTANT_ALPHA", "CONSTANT_ALPHA", "", $Int, "js:\"CONSTANT_ALPHA\""], ["CONSTANT_COLOR", "CONSTANT_COLOR", "", $Int, "js:\"CONSTANT_COLOR\""], ["CONTEXT_LOST_WEBGL", "CONTEXT_LOST_WEBGL", "", $Int, "js:\"CONTEXT_LOST_WEBGL\""], ["CULL_FACE", "CULL_FACE", "", $Int, "js:\"CULL_FACE\""], ["CULL_FACE_MODE", "CULL_FACE_MODE", "", $Int, "js:\"CULL_FACE_MODE\""], ["CURRENT_PROGRAM", "CURRENT_PROGRAM", "", $Int, "js:\"CURRENT_PROGRAM\""], ["CURRENT_VERTEX_ATTRIB", "CURRENT_VERTEX_ATTRIB", "", $Int, "js:\"CURRENT_VERTEX_ATTRIB\""], ["CW", "CW", "", $Int, "js:\"CW\""], ["DECR", "DECR", "", $Int, "js:\"DECR\""], ["DECR_WRAP", "DECR_WRAP", "", $Int, "js:\"DECR_WRAP\""], ["DELETE_STATUS", "DELETE_STATUS", "", $Int, "js:\"DELETE_STATUS\""], ["DEPTH_ATTACHMENT", "DEPTH_ATTACHMENT", "", $Int, "js:\"DEPTH_ATTACHMENT\""], ["DEPTH_BITS", "DEPTH_BITS", "", $Int, "js:\"DEPTH_BITS\""], ["DEPTH_BUFFER_BIT", "DEPTH_BUFFER_BIT", "", $Int, "js:\"DEPTH_BUFFER_BIT\""], ["DEPTH_CLEAR_VALUE", "DEPTH_CLEAR_VALUE", "", $Int, "js:\"DEPTH_CLEAR_VALUE\""], ["DEPTH_COMPONENT", "DEPTH_COMPONENT", "", $Int, "js:\"DEPTH_COMPONENT\""], ["DEPTH_COMPONENT16", "DEPTH_COMPONENT16", "", $Int, "js:\"DEPTH_COMPONENT16\""], ["DEPTH_FUNC", "DEPTH_FUNC", "", $Int, "js:\"DEPTH_FUNC\""], ["DEPTH_RANGE", "DEPTH_RANGE", "", $Int, "js:\"DEPTH_RANGE\""], ["DEPTH_STENCIL", "DEPTH_STENCIL", "", $Int, "js:\"DEPTH_STENCIL\""], ["DEPTH_STENCIL_ATTACHMENT", "DEPTH_STENCIL_ATTACHMENT", "", $Int, "js:\"DEPTH_STENCIL_ATTACHMENT\""], ["DEPTH_TEST", "DEPTH_TEST", "", $Int, "js:\"DEPTH_TEST\""], ["DEPTH_WRITEMASK", "DEPTH_WRITEMASK", "", $Int, "js:\"DEPTH_WRITEMASK\""], ["DITHER", "DITHER", "", $Int, "js:\"DITHER\""], ["DONT_CARE", "DONT_CARE", "", $Int, "js:\"DONT_CARE\""], ["DST_ALPHA", "DST_ALPHA", "", $Int, "js:\"DST_ALPHA\""], ["DST_COLOR", "DST_COLOR", "", $Int, "js:\"DST_COLOR\""], ["DYNAMIC_DRAW", "DYNAMIC_DRAW", "", $Int, "js:\"DYNAMIC_DRAW\""], ["ELEMENT_ARRAY_BUFFER", "ELEMENT_ARRAY_BUFFER", "", $Int, "js:\"ELEMENT_ARRAY_BUFFER\""], ["ELEMENT_ARRAY_BUFFER_BINDING", "ELEMENT_ARRAY_BUFFER_BINDING", "", $Int, "js:\"ELEMENT_ARRAY_BUFFER_BINDING\""], ["EQUAL", "EQUAL", "", $Int, "js:\"EQUAL\""], ["FASTEST", "FASTEST", "", $Int, "js:\"FASTEST\""], ["FLOAT", "FLOAT", "", $Int, "js:\"FLOAT\""], ["FLOAT_MAT2", "FLOAT_MAT2", "", $Int, "js:\"FLOAT_MAT2\""], ["FLOAT_MAT3", "FLOAT_MAT3", "", $Int, "js:\"FLOAT_MAT3\""], ["FLOAT_MAT4", "FLOAT_MAT4", "", $Int, "js:\"FLOAT_MAT4\""], ["FLOAT_VEC2", "FLOAT_VEC2", "", $Int, "js:\"FLOAT_VEC2\""], ["FLOAT_VEC3", "FLOAT_VEC3", "", $Int, "js:\"FLOAT_VEC3\""], ["FLOAT_VEC4", "FLOAT_VEC4", "", $Int, "js:\"FLOAT_VEC4\""], ["FRAGMENT_SHADER", "FRAGMENT_SHADER", "", $Int, "js:\"FRAGMENT_SHADER\""], ["FRAMEBUFFER", "FRAMEBUFFER", "", $Int, "js:\"FRAMEBUFFER\""], ["FRAMEBUFFER_ATTACHMENT_OBJECT_NAME", "FRAMEBUFFER_ATTACHMENT_OBJECT_NAME", "", $Int, "js:\"FRAMEBUFFER_ATTACHMENT_OBJECT_NAME\""], ["FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE", "FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE", "", $Int, "js:\"FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE\""], ["FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE", "FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE", "", $Int, "js:\"FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE\""], ["FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL", "FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL", "", $Int, "js:\"FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL\""], ["FRAMEBUFFER_BINDING", "FRAMEBUFFER_BINDING", "", $Int, "js:\"FRAMEBUFFER_BINDING\""], ["FRAMEBUFFER_COMPLETE", "FRAMEBUFFER_COMPLETE", "", $Int, "js:\"FRAMEBUFFER_COMPLETE\""], ["FRAMEBUFFER_INCOMPLETE_ATTACHMENT", "FRAMEBUFFER_INCOMPLETE_ATTACHMENT", "", $Int, "js:\"FRAMEBUFFER_INCOMPLETE_ATTACHMENT\""], ["FRAMEBUFFER_INCOMPLETE_DIMENSIONS", "FRAMEBUFFER_INCOMPLETE_DIMENSIONS", "", $Int, "js:\"FRAMEBUFFER_INCOMPLETE_DIMENSIONS\""], ["FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT", "FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT", "", $Int, "js:\"FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT\""], ["FRAMEBUFFER_UNSUPPORTED", "FRAMEBUFFER_UNSUPPORTED", "", $Int, "js:\"FRAMEBUFFER_UNSUPPORTED\""], ["FRONT", "FRONT", "", $Int, "js:\"FRONT\""], ["FRONT_AND_BACK", "FRONT_AND_BACK", "", $Int, "js:\"FRONT_AND_BACK\""], ["FRONT_FACE", "FRONT_FACE", "", $Int, "js:\"FRONT_FACE\""], ["FUNC_ADD", "FUNC_ADD", "", $Int, "js:\"FUNC_ADD\""], ["FUNC_REVERSE_SUBTRACT", "FUNC_REVERSE_SUBTRACT", "", $Int, "js:\"FUNC_REVERSE_SUBTRACT\""], ["FUNC_SUBTRACT", "FUNC_SUBTRACT", "", $Int, "js:\"FUNC_SUBTRACT\""], ["GENERATE_MIPMAP_HINT", "GENERATE_MIPMAP_HINT", "", $Int, "js:\"GENERATE_MIPMAP_HINT\""], ["GEQUAL", "GEQUAL", "", $Int, "js:\"GEQUAL\""], ["GREATER", "GREATER", "", $Int, "js:\"GREATER\""], ["GREEN_BITS", "GREEN_BITS", "", $Int, "js:\"GREEN_BITS\""], ["HIGH_FLOAT", "HIGH_FLOAT", "", $Int, "js:\"HIGH_FLOAT\""], ["HIGH_INT", "HIGH_INT", "", $Int, "js:\"HIGH_INT\""], ["INCR", "INCR", "", $Int, "js:\"INCR\""], ["INCR_WRAP", "INCR_WRAP", "", $Int, "js:\"INCR_WRAP\""], ["INFO_LOG_LENGTH", "INFO_LOG_LENGTH", "", $Int, "js:\"INFO_LOG_LENGTH\""], ["INT", "INT", "", $Int, "js:\"INT\""], ["INT_VEC2", "INT_VEC2", "", $Int, "js:\"INT_VEC2\""], ["INT_VEC3", "INT_VEC3", "", $Int, "js:\"INT_VEC3\""], ["INT_VEC4", "INT_VEC4", "", $Int, "js:\"INT_VEC4\""], ["INVALID_ENUM", "INVALID_ENUM", "", $Int, "js:\"INVALID_ENUM\""], ["INVALID_FRAMEBUFFER_OPERATION", "INVALID_FRAMEBUFFER_OPERATION", "", $Int, "js:\"INVALID_FRAMEBUFFER_OPERATION\""], ["INVALID_OPERATION", "INVALID_OPERATION", "", $Int, "js:\"INVALID_OPERATION\""], ["INVALID_VALUE", "INVALID_VALUE", "", $Int, "js:\"INVALID_VALUE\""], ["INVERT", "INVERT", "", $Int, "js:\"INVERT\""], ["KEEP", "KEEP", "", $Int, "js:\"KEEP\""], ["LEQUAL", "LEQUAL", "", $Int, "js:\"LEQUAL\""], ["LESS", "LESS", "", $Int, "js:\"LESS\""], ["LINEAR", "LINEAR", "", $Int, "js:\"LINEAR\""], ["LINEAR_MIPMAP_LINEAR", "LINEAR_MIPMAP_LINEAR", "", $Int, "js:\"LINEAR_MIPMAP_LINEAR\""], ["LINEAR_MIPMAP_NEAREST", "LINEAR_MIPMAP_NEAREST", "", $Int, "js:\"LINEAR_MIPMAP_NEAREST\""], ["LINES", "LINES", "", $Int, "js:\"LINES\""], ["LINE_LOOP", "LINE_LOOP", "", $Int, "js:\"LINE_LOOP\""], ["LINE_STRIP", "LINE_STRIP", "", $Int, "js:\"LINE_STRIP\""], ["LINE_WIDTH", "LINE_WIDTH", "", $Int, "js:\"LINE_WIDTH\""], ["LINK_STATUS", "LINK_STATUS", "", $Int, "js:\"LINK_STATUS\""], ["LOW_FLOAT", "LOW_FLOAT", "", $Int, "js:\"LOW_FLOAT\""], ["LOW_INT", "LOW_INT", "", $Int, "js:\"LOW_INT\""], ["LUMINANCE", "LUMINANCE", "", $Int, "js:\"LUMINANCE\""], ["LUMINANCE_ALPHA", "LUMINANCE_ALPHA", "", $Int, "js:\"LUMINANCE_ALPHA\""], ["MAX_COMBINED_TEXTURE_IMAGE_UNITS", "MAX_COMBINED_TEXTURE_IMAGE_UNITS", "", $Int, "js:\"MAX_COMBINED_TEXTURE_IMAGE_UNITS\""], ["MAX_CUBE_MAP_TEXTURE_SIZE", "MAX_CUBE_MAP_TEXTURE_SIZE", "", $Int, "js:\"MAX_CUBE_MAP_TEXTURE_SIZE\""], ["MAX_FRAGMENT_UNIFORM_VECTORS", "MAX_FRAGMENT_UNIFORM_VECTORS", "", $Int, "js:\"MAX_FRAGMENT_UNIFORM_VECTORS\""], ["MAX_RENDERBUFFER_SIZE", "MAX_RENDERBUFFER_SIZE", "", $Int, "js:\"MAX_RENDERBUFFER_SIZE\""], ["MAX_TEXTURE_IMAGE_UNITS", "MAX_TEXTURE_IMAGE_UNITS", "", $Int, "js:\"MAX_TEXTURE_IMAGE_UNITS\""], ["MAX_TEXTURE_SIZE", "MAX_TEXTURE_SIZE", "", $Int, "js:\"MAX_TEXTURE_SIZE\""], ["MAX_VARYING_VECTORS", "MAX_VARYING_VECTORS", "", $Int, "js:\"MAX_VARYING_VECTORS\""], ["MAX_VERTEX_ATTRIBS", "MAX_VERTEX_ATTRIBS", "", $Int, "js:\"MAX_VERTEX_ATTRIBS\""], ["MAX_VERTEX_TEXTURE_IMAGE_UNITS", "MAX_VERTEX_TEXTURE_IMAGE_UNITS", "", $Int, "js:\"MAX_VERTEX_TEXTURE_IMAGE_UNITS\""], ["MAX_VERTEX_UNIFORM_VECTORS", "MAX_VERTEX_UNIFORM_VECTORS", "", $Int, "js:\"MAX_VERTEX_UNIFORM_VECTORS\""], ["MAX_VIEWPORT_DIMS", "MAX_VIEWPORT_DIMS", "", $Int, "js:\"MAX_VIEWPORT_DIMS\""], ["MEDIUM_FLOAT", "MEDIUM_FLOAT", "", $Int, "js:\"MEDIUM_FLOAT\""], ["MEDIUM_INT", "MEDIUM_INT", "", $Int, "js:\"MEDIUM_INT\""], ["MIRRORED_REPEAT", "MIRRORED_REPEAT", "", $Int, "js:\"MIRRORED_REPEAT\""], ["NEAREST", "NEAREST", "", $Int, "js:\"NEAREST\""], ["NEAREST_MIPMAP_LINEAR", "NEAREST_MIPMAP_LINEAR", "", $Int, "js:\"NEAREST_MIPMAP_LINEAR\""], ["NEAREST_MIPMAP_NEAREST", "NEAREST_MIPMAP_NEAREST", "", $Int, "js:\"NEAREST_MIPMAP_NEAREST\""], ["NEVER", "NEVER", "", $Int, "js:\"NEVER\""], ["NICEST", "NICEST", "", $Int, "js:\"NICEST\""], ["NONE", "NONE", "", $Int, "js:\"NONE\""], ["NOTEQUAL", "NOTEQUAL", "", $Int, "js:\"NOTEQUAL\""], ["NO_ERROR", "NO_ERROR", "", $Int, "js:\"NO_ERROR\""], ["NUM_COMPRESSED_TEXTURE_FORMATS", "NUM_COMPRESSED_TEXTURE_FORMATS", "", $Int, "js:\"NUM_COMPRESSED_TEXTURE_FORMATS\""], ["ONE", "ONE", "", $Int, "js:\"ONE\""], ["ONE_MINUS_CONSTANT_ALPHA", "ONE_MINUS_CONSTANT_ALPHA", "", $Int, "js:\"ONE_MINUS_CONSTANT_ALPHA\""], ["ONE_MINUS_CONSTANT_COLOR", "ONE_MINUS_CONSTANT_COLOR", "", $Int, "js:\"ONE_MINUS_CONSTANT_COLOR\""], ["ONE_MINUS_DST_ALPHA", "ONE_MINUS_DST_ALPHA", "", $Int, "js:\"ONE_MINUS_DST_ALPHA\""], ["ONE_MINUS_DST_COLOR", "ONE_MINUS_DST_COLOR", "", $Int, "js:\"ONE_MINUS_DST_COLOR\""], ["ONE_MINUS_SRC_ALPHA", "ONE_MINUS_SRC_ALPHA", "", $Int, "js:\"ONE_MINUS_SRC_ALPHA\""], ["ONE_MINUS_SRC_COLOR", "ONE_MINUS_SRC_COLOR", "", $Int, "js:\"ONE_MINUS_SRC_COLOR\""], ["OUT_OF_MEMORY", "OUT_OF_MEMORY", "", $Int, "js:\"OUT_OF_MEMORY\""], ["PACK_ALIGNMENT", "PACK_ALIGNMENT", "", $Int, "js:\"PACK_ALIGNMENT\""], ["POINTS", "POINTS", "", $Int, "js:\"POINTS\""], ["POLYGON_OFFSET_FACTOR", "POLYGON_OFFSET_FACTOR", "", $Int, "js:\"POLYGON_OFFSET_FACTOR\""], ["POLYGON_OFFSET_FILL", "POLYGON_OFFSET_FILL", "", $Int, "js:\"POLYGON_OFFSET_FILL\""], ["POLYGON_OFFSET_UNITS", "POLYGON_OFFSET_UNITS", "", $Int, "js:\"POLYGON_OFFSET_UNITS\""], ["RED_BITS", "RED_BITS", "", $Int, "js:\"RED_BITS\""], ["RENDERBUFFER", "RENDERBUFFER", "", $Int, "js:\"RENDERBUFFER\""], ["RENDERBUFFER_ALPHA_SIZE", "RENDERBUFFER_ALPHA_SIZE", "", $Int, "js:\"RENDERBUFFER_ALPHA_SIZE\""], ["RENDERBUFFER_BINDING", "RENDERBUFFER_BINDING", "", $Int, "js:\"RENDERBUFFER_BINDING\""], ["RENDERBUFFER_BLUE_SIZE", "RENDERBUFFER_BLUE_SIZE", "", $Int, "js:\"RENDERBUFFER_BLUE_SIZE\""], ["RENDERBUFFER_DEPTH_SIZE", "RENDERBUFFER_DEPTH_SIZE", "", $Int, "js:\"RENDERBUFFER_DEPTH_SIZE\""], ["RENDERBUFFER_GREEN_SIZE", "RENDERBUFFER_GREEN_SIZE", "", $Int, "js:\"RENDERBUFFER_GREEN_SIZE\""], ["RENDERBUFFER_HEIGHT", "RENDERBUFFER_HEIGHT", "", $Int, "js:\"RENDERBUFFER_HEIGHT\""], ["RENDERBUFFER_INTERNAL_FORMAT", "RENDERBUFFER_INTERNAL_FORMAT", "", $Int, "js:\"RENDERBUFFER_INTERNAL_FORMAT\""], ["RENDERBUFFER_RED_SIZE", "RENDERBUFFER_RED_SIZE", "", $Int, "js:\"RENDERBUFFER_RED_SIZE\""], ["RENDERBUFFER_STENCIL_SIZE", "RENDERBUFFER_STENCIL_SIZE", "", $Int, "js:\"RENDERBUFFER_STENCIL_SIZE\""], ["RENDERBUFFER_WIDTH", "RENDERBUFFER_WIDTH", "", $Int, "js:\"RENDERBUFFER_WIDTH\""], ["RENDERER", "RENDERER", "", $Int, "js:\"RENDERER\""], ["REPEAT", "REPEAT", "", $Int, "js:\"REPEAT\""], ["REPLACE", "REPLACE", "", $Int, "js:\"REPLACE\""], ["RGB", "RGB", "", $Int, "js:\"RGB\""], ["RGB5_A1", "RGB5_A1", "", $Int, "js:\"RGB5_A1\""], ["RGB565", "RGB565", "", $Int, "js:\"RGB565\""], ["RGBA", "RGBA", "", $Int, "js:\"RGBA\""], ["RGBA4", "RGBA4", "", $Int, "js:\"RGBA4\""], ["SAMPLER_2D", "SAMPLER_2D", "", $Int, "js:\"SAMPLER_2D\""], ["SAMPLER_CUBE", "SAMPLER_CUBE", "", $Int, "js:\"SAMPLER_CUBE\""], ["SAMPLES", "SAMPLES", "", $Int, "js:\"SAMPLES\""], ["SAMPLE_ALPHA_TO_COVERAGE", "SAMPLE_ALPHA_TO_COVERAGE", "", $Int, "js:\"SAMPLE_ALPHA_TO_COVERAGE\""], ["SAMPLE_BUFFERS", "SAMPLE_BUFFERS", "", $Int, "js:\"SAMPLE_BUFFERS\""], ["SAMPLE_COVERAGE", "SAMPLE_COVERAGE", "", $Int, "js:\"SAMPLE_COVERAGE\""], ["SAMPLE_COVERAGE_INVERT", "SAMPLE_COVERAGE_INVERT", "", $Int, "js:\"SAMPLE_COVERAGE_INVERT\""], ["SAMPLE_COVERAGE_VALUE", "SAMPLE_COVERAGE_VALUE", "", $Int, "js:\"SAMPLE_COVERAGE_VALUE\""], ["SCISSOR_BOX", "SCISSOR_BOX", "", $Int, "js:\"SCISSOR_BOX\""], ["SCISSOR_TEST", "SCISSOR_TEST", "", $Int, "js:\"SCISSOR_TEST\""], ["SHADER_COMPILER", "SHADER_COMPILER", "", $Int, "js:\"SHADER_COMPILER\""], ["SHADER_SOURCE_LENGTH", "SHADER_SOURCE_LENGTH", "", $Int, "js:\"SHADER_SOURCE_LENGTH\""], ["SHADER_TYPE", "SHADER_TYPE", "", $Int, "js:\"SHADER_TYPE\""], ["SHADING_LANGUAGE_VERSION", "SHADING_LANGUAGE_VERSION", "", $Int, "js:\"SHADING_LANGUAGE_VERSION\""], ["SHORT", "SHORT", "", $Int, "js:\"SHORT\""], ["SRC_ALPHA", "SRC_ALPHA", "", $Int, "js:\"SRC_ALPHA\""], ["SRC_ALPHA_SATURATE", "SRC_ALPHA_SATURATE", "", $Int, "js:\"SRC_ALPHA_SATURATE\""], ["SRC_COLOR", "SRC_COLOR", "", $Int, "js:\"SRC_COLOR\""], ["STATIC_DRAW", "STATIC_DRAW", "", $Int, "js:\"STATIC_DRAW\""], ["STENCIL_ATTACHMENT", "STENCIL_ATTACHMENT", "", $Int, "js:\"STENCIL_ATTACHMENT\""], ["STENCIL_BACK_FAIL", "STENCIL_BACK_FAIL", "", $Int, "js:\"STENCIL_BACK_FAIL\""], ["STENCIL_BACK_FUNC", "STENCIL_BACK_FUNC", "", $Int, "js:\"STENCIL_BACK_FUNC\""], ["STENCIL_BACK_PASS_DEPTH_FAIL", "STENCIL_BACK_PASS_DEPTH_FAIL", "", $Int, "js:\"STENCIL_BACK_PASS_DEPTH_FAIL\""], ["STENCIL_BACK_PASS_DEPTH_PASS", "STENCIL_BACK_PASS_DEPTH_PASS", "", $Int, "js:\"STENCIL_BACK_PASS_DEPTH_PASS\""], ["STENCIL_BACK_REF", "STENCIL_BACK_REF", "", $Int, "js:\"STENCIL_BACK_REF\""], ["STENCIL_BACK_VALUE_MASK", "STENCIL_BACK_VALUE_MASK", "", $Int, "js:\"STENCIL_BACK_VALUE_MASK\""], ["STENCIL_BACK_WRITEMASK", "STENCIL_BACK_WRITEMASK", "", $Int, "js:\"STENCIL_BACK_WRITEMASK\""], ["STENCIL_BITS", "STENCIL_BITS", "", $Int, "js:\"STENCIL_BITS\""], ["STENCIL_BUFFER_BIT", "STENCIL_BUFFER_BIT", "", $Int, "js:\"STENCIL_BUFFER_BIT\""], ["STENCIL_CLEAR_VALUE", "STENCIL_CLEAR_VALUE", "", $Int, "js:\"STENCIL_CLEAR_VALUE\""], ["STENCIL_FAIL", "STENCIL_FAIL", "", $Int, "js:\"STENCIL_FAIL\""], ["STENCIL_FUNC", "STENCIL_FUNC", "", $Int, "js:\"STENCIL_FUNC\""], ["STENCIL_INDEX", "STENCIL_INDEX", "", $Int, "js:\"STENCIL_INDEX\""], ["STENCIL_INDEX8", "STENCIL_INDEX8", "", $Int, "js:\"STENCIL_INDEX8\""], ["STENCIL_PASS_DEPTH_FAIL", "STENCIL_PASS_DEPTH_FAIL", "", $Int, "js:\"STENCIL_PASS_DEPTH_FAIL\""], ["STENCIL_PASS_DEPTH_PASS", "STENCIL_PASS_DEPTH_PASS", "", $Int, "js:\"STENCIL_PASS_DEPTH_PASS\""], ["STENCIL_REF", "STENCIL_REF", "", $Int, "js:\"STENCIL_REF\""], ["STENCIL_TEST", "STENCIL_TEST", "", $Int, "js:\"STENCIL_TEST\""], ["STENCIL_VALUE_MASK", "STENCIL_VALUE_MASK", "", $Int, "js:\"STENCIL_VALUE_MASK\""], ["STENCIL_WRITEMASK", "STENCIL_WRITEMASK", "", $Int, "js:\"STENCIL_WRITEMASK\""], ["STREAM_DRAW", "STREAM_DRAW", "", $Int, "js:\"STREAM_DRAW\""], ["SUBPIXEL_BITS", "SUBPIXEL_BITS", "", $Int, "js:\"SUBPIXEL_BITS\""], ["TEXTURE", "TEXTURE", "", $Int, "js:\"TEXTURE\""], ["TEXTURE0", "TEXTURE0", "", $Int, "js:\"TEXTURE0\""], ["TEXTURE1", "TEXTURE1", "", $Int, "js:\"TEXTURE1\""], ["TEXTURE2", "TEXTURE2", "", $Int, "js:\"TEXTURE2\""], ["TEXTURE3", "TEXTURE3", "", $Int, "js:\"TEXTURE3\""], ["TEXTURE4", "TEXTURE4", "", $Int, "js:\"TEXTURE4\""], ["TEXTURE5", "TEXTURE5", "", $Int, "js:\"TEXTURE5\""], ["TEXTURE6", "TEXTURE6", "", $Int, "js:\"TEXTURE6\""], ["TEXTURE7", "TEXTURE7", "", $Int, "js:\"TEXTURE7\""], ["TEXTURE8", "TEXTURE8", "", $Int, "js:\"TEXTURE8\""], ["TEXTURE9", "TEXTURE9", "", $Int, "js:\"TEXTURE9\""], ["TEXTURE10", "TEXTURE10", "", $Int, "js:\"TEXTURE10\""], ["TEXTURE11", "TEXTURE11", "", $Int, "js:\"TEXTURE11\""], ["TEXTURE12", "TEXTURE12", "", $Int, "js:\"TEXTURE12\""], ["TEXTURE13", "TEXTURE13", "", $Int, "js:\"TEXTURE13\""], ["TEXTURE14", "TEXTURE14", "", $Int, "js:\"TEXTURE14\""], ["TEXTURE15", "TEXTURE15", "", $Int, "js:\"TEXTURE15\""], ["TEXTURE16", "TEXTURE16", "", $Int, "js:\"TEXTURE16\""], ["TEXTURE17", "TEXTURE17", "", $Int, "js:\"TEXTURE17\""], ["TEXTURE18", "TEXTURE18", "", $Int, "js:\"TEXTURE18\""], ["TEXTURE19", "TEXTURE19", "", $Int, "js:\"TEXTURE19\""], ["TEXTURE20", "TEXTURE20", "", $Int, "js:\"TEXTURE20\""], ["TEXTURE21", "TEXTURE21", "", $Int, "js:\"TEXTURE21\""], ["TEXTURE22", "TEXTURE22", "", $Int, "js:\"TEXTURE22\""], ["TEXTURE23", "TEXTURE23", "", $Int, "js:\"TEXTURE23\""], ["TEXTURE24", "TEXTURE24", "", $Int, "js:\"TEXTURE24\""], ["TEXTURE25", "TEXTURE25", "", $Int, "js:\"TEXTURE25\""], ["TEXTURE26", "TEXTURE26", "", $Int, "js:\"TEXTURE26\""], ["TEXTURE27", "TEXTURE27", "", $Int, "js:\"TEXTURE27\""], ["TEXTURE28", "TEXTURE28", "", $Int, "js:\"TEXTURE28\""], ["TEXTURE29", "TEXTURE29", "", $Int, "js:\"TEXTURE29\""], ["TEXTURE30", "TEXTURE30", "", $Int, "js:\"TEXTURE30\""], ["TEXTURE31", "TEXTURE31", "", $Int, "js:\"TEXTURE31\""], ["TEXTURE_2D", "TEXTURE_2D", "", $Int, "js:\"TEXTURE_2D\""], ["TEXTURE_BINDING_2D", "TEXTURE_BINDING_2D", "", $Int, "js:\"TEXTURE_BINDING_2D\""], ["TEXTURE_BINDING_CUBE_MAP", "TEXTURE_BINDING_CUBE_MAP", "", $Int, "js:\"TEXTURE_BINDING_CUBE_MAP\""], ["TEXTURE_CUBE_MAP", "TEXTURE_CUBE_MAP", "", $Int, "js:\"TEXTURE_CUBE_MAP\""], ["TEXTURE_CUBE_MAP_NEGATIVE_X", "TEXTURE_CUBE_MAP_NEGATIVE_X", "", $Int, "js:\"TEXTURE_CUBE_MAP_NEGATIVE_X\""], ["TEXTURE_CUBE_MAP_NEGATIVE_Y", "TEXTURE_CUBE_MAP_NEGATIVE_Y", "", $Int, "js:\"TEXTURE_CUBE_MAP_NEGATIVE_Y\""], ["TEXTURE_CUBE_MAP_NEGATIVE_Z", "TEXTURE_CUBE_MAP_NEGATIVE_Z", "", $Int, "js:\"TEXTURE_CUBE_MAP_NEGATIVE_Z\""], ["TEXTURE_CUBE_MAP_POSITIVE_X", "TEXTURE_CUBE_MAP_POSITIVE_X", "", $Int, "js:\"TEXTURE_CUBE_MAP_POSITIVE_X\""], ["TEXTURE_CUBE_MAP_POSITIVE_Y", "TEXTURE_CUBE_MAP_POSITIVE_Y", "", $Int, "js:\"TEXTURE_CUBE_MAP_POSITIVE_Y\""], ["TEXTURE_CUBE_MAP_POSITIVE_Z", "TEXTURE_CUBE_MAP_POSITIVE_Z", "", $Int, "js:\"TEXTURE_CUBE_MAP_POSITIVE_Z\""], ["TEXTURE_MAG_FILTER", "TEXTURE_MAG_FILTER", "", $Int, "js:\"TEXTURE_MAG_FILTER\""], ["TEXTURE_MIN_FILTER", "TEXTURE_MIN_FILTER", "", $Int, "js:\"TEXTURE_MIN_FILTER\""], ["TEXTURE_WRAP_S", "TEXTURE_WRAP_S", "", $Int, "js:\"TEXTURE_WRAP_S\""], ["TEXTURE_WRAP_T", "TEXTURE_WRAP_T", "", $Int, "js:\"TEXTURE_WRAP_T\""], ["TRIANGLES", "TRIANGLES", "", $Int, "js:\"TRIANGLES\""], ["TRIANGLE_FAN", "TRIANGLE_FAN", "", $Int, "js:\"TRIANGLE_FAN\""], ["TRIANGLE_STRIP", "TRIANGLE_STRIP", "", $Int, "js:\"TRIANGLE_STRIP\""], ["UNPACK_ALIGNMENT", "UNPACK_ALIGNMENT", "", $Int, "js:\"UNPACK_ALIGNMENT\""], ["UNPACK_COLORSPACE_CONVERSION_WEBGL", "UNPACK_COLORSPACE_CONVERSION_WEBGL", "", $Int, "js:\"UNPACK_COLORSPACE_CONVERSION_WEBGL\""], ["UNPACK_FLIP_Y_WEBGL", "UNPACK_FLIP_Y_WEBGL", "", $Int, "js:\"UNPACK_FLIP_Y_WEBGL\""], ["UNPACK_PREMULTIPLY_ALPHA_WEBGL", "UNPACK_PREMULTIPLY_ALPHA_WEBGL", "", $Int, "js:\"UNPACK_PREMULTIPLY_ALPHA_WEBGL\""], ["UNSIGNED_BYTE", "UNSIGNED_BYTE", "", $Int, "js:\"UNSIGNED_BYTE\""], ["UNSIGNED_INT", "UNSIGNED_INT", "", $Int, "js:\"UNSIGNED_INT\""], ["UNSIGNED_SHORT", "UNSIGNED_SHORT", "", $Int, "js:\"UNSIGNED_SHORT\""], ["UNSIGNED_SHORT_4_4_4_4", "UNSIGNED_SHORT_4_4_4_4", "", $Int, "js:\"UNSIGNED_SHORT_4_4_4_4\""], ["UNSIGNED_SHORT_5_5_5_1", "UNSIGNED_SHORT_5_5_5_1", "", $Int, "js:\"UNSIGNED_SHORT_5_5_5_1\""], ["UNSIGNED_SHORT_5_6_5", "UNSIGNED_SHORT_5_6_5", "", $Int, "js:\"UNSIGNED_SHORT_5_6_5\""], ["VALIDATE_STATUS", "VALIDATE_STATUS", "", $Int, "js:\"VALIDATE_STATUS\""], ["VENDOR", "VENDOR", "", $Int, "js:\"VENDOR\""], ["VERSION", "VERSION", "", $Int, "js:\"VERSION\""], ["VERTEX_ATTRIB_ARRAY_BUFFER_BINDING", "VERTEX_ATTRIB_ARRAY_BUFFER_BINDING", "", $Int, "js:\"VERTEX_ATTRIB_ARRAY_BUFFER_BINDING\""], ["VERTEX_ATTRIB_ARRAY_ENABLED", "VERTEX_ATTRIB_ARRAY_ENABLED", "", $Int, "js:\"VERTEX_ATTRIB_ARRAY_ENABLED\""], ["VERTEX_ATTRIB_ARRAY_NORMALIZED", "VERTEX_ATTRIB_ARRAY_NORMALIZED", "", $Int, "js:\"VERTEX_ATTRIB_ARRAY_NORMALIZED\""], ["VERTEX_ATTRIB_ARRAY_POINTER", "VERTEX_ATTRIB_ARRAY_POINTER", "", $Int, "js:\"VERTEX_ATTRIB_ARRAY_POINTER\""], ["VERTEX_ATTRIB_ARRAY_SIZE", "VERTEX_ATTRIB_ARRAY_SIZE", "", $Int, "js:\"VERTEX_ATTRIB_ARRAY_SIZE\""], ["VERTEX_ATTRIB_ARRAY_STRIDE", "VERTEX_ATTRIB_ARRAY_STRIDE", "", $Int, "js:\"VERTEX_ATTRIB_ARRAY_STRIDE\""], ["VERTEX_ATTRIB_ARRAY_TYPE", "VERTEX_ATTRIB_ARRAY_TYPE", "", $Int, "js:\"VERTEX_ATTRIB_ARRAY_TYPE\""], ["VERTEX_SHADER", "VERTEX_SHADER", "", $Int, "js:\"VERTEX_SHADER\""], ["VIEWPORT", "VIEWPORT", "", $Int, "js:\"VIEWPORT\""], ["ZERO", "ZERO", "", $Int, "js:\"ZERO\""]]);
	};
	return $pkg;
})();
$packages["math"] = (function() {
	var $pkg = {}, js = $packages["github.com/gopherjs/gopherjs/js"], math, zero, posInf, negInf, nan, pow10tab, _sin, _cos, init, Abs, Cos, Exp, Inf, IsInf, IsNaN, Ldexp, Log, Max, NaN, Signbit, Sin, Float32bits, Float32frombits, abs, max, init$1, cos, sin;
	init = function() {
		Float32bits(0);
		Float32frombits(0);
	};
	Abs = $pkg.Abs = function(x) {
		return abs(x);
	};
	Cos = $pkg.Cos = function(x) {
		return cos(x);
	};
	Exp = $pkg.Exp = function(x) {
		return $parseFloat(math.exp(x));
	};
	Inf = $pkg.Inf = function(sign) {
		if (sign >= 0) {
			return posInf;
		} else {
			return negInf;
		}
	};
	IsInf = $pkg.IsInf = function(f, sign) {
		if (f === posInf) {
			return sign >= 0;
		}
		if (f === negInf) {
			return sign <= 0;
		}
		return false;
	};
	IsNaN = $pkg.IsNaN = function(f) {
		var is = false;
		is = !((f === f));
		return is;
	};
	Ldexp = $pkg.Ldexp = function(frac, exp$1) {
		if (frac === 0) {
			return frac;
		}
		if (exp$1 >= 1024) {
			return frac * $parseFloat(math.pow(2, 1023)) * $parseFloat(math.pow(2, exp$1 - 1023 >> 0));
		}
		if (exp$1 <= -1024) {
			return frac * $parseFloat(math.pow(2, -1023)) * $parseFloat(math.pow(2, exp$1 + 1023 >> 0));
		}
		return frac * $parseFloat(math.pow(2, exp$1));
	};
	Log = $pkg.Log = function(x) {
		if (!((x === x))) {
			return nan;
		}
		return $parseFloat(math.log(x));
	};
	Max = $pkg.Max = function(x, y) {
		return max(x, y);
	};
	NaN = $pkg.NaN = function() {
		return nan;
	};
	Signbit = $pkg.Signbit = function(x) {
		return x < 0 || (1 / x === negInf);
	};
	Sin = $pkg.Sin = function(x) {
		return sin(x);
	};
	Float32bits = $pkg.Float32bits = function(f) {
		var s, e, r;
		if ($float32IsEqual(f, 0)) {
			if ($float32IsEqual(1 / f, negInf)) {
				return 2147483648;
			}
			return 0;
		}
		if (!(($float32IsEqual(f, f)))) {
			return 2143289344;
		}
		s = 0;
		if (f < 0) {
			s = 2147483648;
			f = -f;
		}
		e = 150;
		while (f >= 1.6777216e+07) {
			f = f / (2);
			if (e === 255) {
				break;
			}
			e = e + (1) >>> 0;
		}
		while (f < 8.388608e+06) {
			e = e - (1) >>> 0;
			if (e === 0) {
				break;
			}
			f = f * (2);
		}
		r = $parseFloat($mod(f, 2));
		if ((r > 0.5 && r < 1) || r >= 1.5) {
			f = f + (1);
		}
		return (((s | (e << 23 >>> 0)) >>> 0) | (((f >> 0) & ~8388608))) >>> 0;
	};
	Float32frombits = $pkg.Float32frombits = function(b) {
		var s, e, m;
		s = 1;
		if (!((((b & 2147483648) >>> 0) === 0))) {
			s = -1;
		}
		e = (((b >>> 23 >>> 0)) & 255) >>> 0;
		m = (b & 8388607) >>> 0;
		if (e === 255) {
			if (m === 0) {
				return s / 0;
			}
			return nan;
		}
		if (!((e === 0))) {
			m = m + (8388608) >>> 0;
		}
		if (e === 0) {
			e = 1;
		}
		return Ldexp(m, ((e >> 0) - 127 >> 0) - 23 >> 0) * s;
	};
	abs = function(x) {
		if (x < 0) {
			return -x;
		} else if (x === 0) {
			return 0;
		}
		return x;
	};
	max = function(x, y) {
		if (IsInf(x, 1) || IsInf(y, 1)) {
			return Inf(1);
		} else if (IsNaN(x) || IsNaN(y)) {
			return NaN();
		} else if ((x === 0) && (x === y)) {
			if (Signbit(x)) {
				return y;
			}
			return x;
		}
		if (x > y) {
			return x;
		}
		return y;
	};
	init$1 = function() {
		var i, _q, m, x;
		pow10tab[0] = 1;
		pow10tab[1] = 10;
		i = 2;
		while (i < 70) {
			m = (_q = i / 2, (_q === _q && _q !== 1/0 && _q !== -1/0) ? _q >> 0 : $throwRuntimeError("integer divide by zero"));
			(i < 0 || i >= pow10tab.length) ? $throwRuntimeError("index out of range") : pow10tab[i] = ((m < 0 || m >= pow10tab.length) ? $throwRuntimeError("index out of range") : pow10tab[m]) * (x = i - m >> 0, ((x < 0 || x >= pow10tab.length) ? $throwRuntimeError("index out of range") : pow10tab[x]));
			i = i + (1) >> 0;
		}
	};
	cos = function(x) {
		var sign, j, y, x$1, x$2, x$3, x$4, z, zz;
		if (IsNaN(x) || IsInf(x, 0)) {
			return NaN();
		}
		sign = false;
		if (x < 0) {
			x = -x;
		}
		j = new $Int64(0, x * 1.2732395447351625);
		y = $flatten64(j);
		if ((x$1 = new $Int64(j.$high & 0, (j.$low & 1) >>> 0), (x$1.$high === 0 && x$1.$low === 1))) {
			j = (x$2 = new $Int64(0, 1), new $Int64(j.$high + x$2.$high, j.$low + x$2.$low));
			y = y + (1);
		}
		j = (x$3 = new $Int64(0, 7), new $Int64(j.$high & x$3.$high, (j.$low & x$3.$low) >>> 0));
		if ((j.$high > 0 || (j.$high === 0 && j.$low > 3))) {
			j = (x$4 = new $Int64(0, 4), new $Int64(j.$high - x$4.$high, j.$low - x$4.$low));
			sign = !sign;
		}
		if ((j.$high > 0 || (j.$high === 0 && j.$low > 1))) {
			sign = !sign;
		}
		z = ((x - y * 0.7853981256484985) - y * 3.774894707930798e-08) - y * 2.6951514290790595e-15;
		zz = z * z;
		if ((j.$high === 0 && j.$low === 1) || (j.$high === 0 && j.$low === 2)) {
			y = z + z * zz * ((((((_sin[0] * zz) + _sin[1]) * zz + _sin[2]) * zz + _sin[3]) * zz + _sin[4]) * zz + _sin[5]);
		} else {
			y = 1 - 0.5 * zz + zz * zz * ((((((_cos[0] * zz) + _cos[1]) * zz + _cos[2]) * zz + _cos[3]) * zz + _cos[4]) * zz + _cos[5]);
		}
		if (sign) {
			y = -y;
		}
		return y;
	};
	sin = function(x) {
		var sign, j, y, x$1, x$2, x$3, x$4, z, zz;
		if ((x === 0) || IsNaN(x)) {
			return x;
		} else if (IsInf(x, 0)) {
			return NaN();
		}
		sign = false;
		if (x < 0) {
			x = -x;
			sign = true;
		}
		j = new $Int64(0, x * 1.2732395447351625);
		y = $flatten64(j);
		if ((x$1 = new $Int64(j.$high & 0, (j.$low & 1) >>> 0), (x$1.$high === 0 && x$1.$low === 1))) {
			j = (x$2 = new $Int64(0, 1), new $Int64(j.$high + x$2.$high, j.$low + x$2.$low));
			y = y + (1);
		}
		j = (x$3 = new $Int64(0, 7), new $Int64(j.$high & x$3.$high, (j.$low & x$3.$low) >>> 0));
		if ((j.$high > 0 || (j.$high === 0 && j.$low > 3))) {
			sign = !sign;
			j = (x$4 = new $Int64(0, 4), new $Int64(j.$high - x$4.$high, j.$low - x$4.$low));
		}
		z = ((x - y * 0.7853981256484985) - y * 3.774894707930798e-08) - y * 2.6951514290790595e-15;
		zz = z * z;
		if ((j.$high === 0 && j.$low === 1) || (j.$high === 0 && j.$low === 2)) {
			y = 1 - 0.5 * zz + zz * zz * ((((((_cos[0] * zz) + _cos[1]) * zz + _cos[2]) * zz + _cos[3]) * zz + _cos[4]) * zz + _cos[5]);
		} else {
			y = z + z * zz * ((((((_sin[0] * zz) + _sin[1]) * zz + _sin[2]) * zz + _sin[3]) * zz + _sin[4]) * zz + _sin[5]);
		}
		if (sign) {
			y = -y;
		}
		return y;
	};
	$pkg.$init = function() {
		pow10tab = ($arrayType($Float64, 70)).zero();
		math = $global.Math;
		zero = 0;
		posInf = 1 / zero;
		negInf = -1 / zero;
		nan = 0 / zero;
		_sin = $toNativeArray("Float64", [1.5896230157654656e-10, -2.5050747762857807e-08, 2.7557313621385722e-06, -0.0001984126982958954, 0.008333333333322118, -0.1666666666666663]);
		_cos = $toNativeArray("Float64", [-1.1358536521387682e-11, 2.087570084197473e-09, -2.755731417929674e-07, 2.4801587288851704e-05, -0.0013888888888873056, 0.041666666666666595]);
		init();
		init$1();
	};
	return $pkg;
})();
$packages["github.com/ajhager/enj"] = (function() {
	var $pkg = {}, js = $packages["github.com/gopherjs/gopherjs/js"], webgl = $packages["github.com/gopherjs/webgl"], math = $packages["math"], App, Assets, Batch, Managed, Canvas, Responder, Game, Shader, Region, Texture, i8, i32, f32, batchVert, batchFrag, NewApp, NewAssets, colorToFloat, NewBatch, NewCanvas, init, rafPolyfill, RequestAnimationFrame, CancelAnimationFrame, NewShader, NewTexture;
	App = $pkg.App = $newType(0, "Struct", "enj.App", "App", "github.com/ajhager/enj", function(responder_, lastTime_, justResumed_, requestId_, Load_, Canvas_) {
		this.$val = this;
		this.responder = responder_ !== undefined ? responder_ : null;
		this.lastTime = lastTime_ !== undefined ? lastTime_ : 0;
		this.justResumed = justResumed_ !== undefined ? justResumed_ : false;
		this.requestId = requestId_ !== undefined ? requestId_ : 0;
		this.Load = Load_ !== undefined ? Load_ : ($ptrType(Assets)).nil;
		this.Canvas = Canvas_ !== undefined ? Canvas_ : ($ptrType(Canvas)).nil;
	});
	Assets = $pkg.Assets = $newType(0, "Struct", "enj.Assets", "Assets", "github.com/ajhager/enj", function(queue_, cache_, loads_, errors_) {
		this.$val = this;
		this.queue = queue_ !== undefined ? queue_ : ($sliceType($String)).nil;
		this.cache = cache_ !== undefined ? cache_ : false;
		this.loads = loads_ !== undefined ? loads_ : 0;
		this.errors = errors_ !== undefined ? errors_ : 0;
	});
	Batch = $pkg.Batch = $newType(0, "Struct", "enj.Batch", "Batch", "github.com/ajhager/enj", function(app_, drawing_, lastTexture_, color_, vertices_, vertexVBO_, indices_, indexVBO_, index_, shader_, customShader_, blendingDisabled_, blendSrcFunc_, blendDstFunc_, inPosition_, inColor_, inTexCoords_, ufProjection_, projX_, projY_) {
		this.$val = this;
		this.app = app_ !== undefined ? app_ : ($ptrType(App)).nil;
		this.drawing = drawing_ !== undefined ? drawing_ : false;
		this.lastTexture = lastTexture_ !== undefined ? lastTexture_ : ($ptrType(Texture)).nil;
		this.color = color_ !== undefined ? color_ : 0;
		this.vertices = vertices_ !== undefined ? vertices_ : null;
		this.vertexVBO = vertexVBO_ !== undefined ? vertexVBO_ : null;
		this.indices = indices_ !== undefined ? indices_ : null;
		this.indexVBO = indexVBO_ !== undefined ? indexVBO_ : null;
		this.index = index_ !== undefined ? index_ : 0;
		this.shader = shader_ !== undefined ? shader_ : ($ptrType(Shader)).nil;
		this.customShader = customShader_ !== undefined ? customShader_ : ($ptrType(Shader)).nil;
		this.blendingDisabled = blendingDisabled_ !== undefined ? blendingDisabled_ : false;
		this.blendSrcFunc = blendSrcFunc_ !== undefined ? blendSrcFunc_ : 0;
		this.blendDstFunc = blendDstFunc_ !== undefined ? blendDstFunc_ : 0;
		this.inPosition = inPosition_ !== undefined ? inPosition_ : 0;
		this.inColor = inColor_ !== undefined ? inColor_ : 0;
		this.inTexCoords = inTexCoords_ !== undefined ? inTexCoords_ : 0;
		this.ufProjection = ufProjection_ !== undefined ? ufProjection_ : null;
		this.projX = projX_ !== undefined ? projX_ : 0;
		this.projY = projY_ !== undefined ? projY_ : 0;
	});
	Managed = $pkg.Managed = $newType(8, "Interface", "enj.Managed", "Managed", "github.com/ajhager/enj", null);
	Canvas = $pkg.Canvas = $newType(0, "Struct", "enj.Canvas", "Canvas", "github.com/ajhager/enj", function(Object_, GL_, attrs_, width_, height_, pixelRatio_, fullscreen_, resources_) {
		this.$val = this;
		this.Object = Object_ !== undefined ? Object_ : null;
		this.GL = GL_ !== undefined ? GL_ : ($ptrType(webgl.Context)).nil;
		this.attrs = attrs_ !== undefined ? attrs_ : ($ptrType(webgl.ContextAttributes)).nil;
		this.width = width_ !== undefined ? width_ : 0;
		this.height = height_ !== undefined ? height_ : 0;
		this.pixelRatio = pixelRatio_ !== undefined ? pixelRatio_ : 0;
		this.fullscreen = fullscreen_ !== undefined ? fullscreen_ : false;
		this.resources = resources_ !== undefined ? resources_ : ($sliceType(Managed)).nil;
	});
	Responder = $pkg.Responder = $newType(8, "Interface", "enj.Responder", "Responder", "github.com/ajhager/enj", null);
	Game = $pkg.Game = $newType(0, "Struct", "enj.Game", "Game", "github.com/ajhager/enj", function() {
		this.$val = this;
	});
	Shader = $pkg.Shader = $newType(0, "Struct", "enj.Shader", "Shader", "github.com/ajhager/enj", function(Object_, app_) {
		this.$val = this;
		this.Object = Object_ !== undefined ? Object_ : null;
		this.app = app_ !== undefined ? app_ : ($ptrType(App)).nil;
	});
	Region = $pkg.Region = $newType(0, "Struct", "enj.Region", "Region", "github.com/ajhager/enj", function(texture_, u_, v_, u2_, v2_, width_, height_) {
		this.$val = this;
		this.texture = texture_ !== undefined ? texture_ : ($ptrType(Texture)).nil;
		this.u = u_ !== undefined ? u_ : 0;
		this.v = v_ !== undefined ? v_ : 0;
		this.u2 = u2_ !== undefined ? u2_ : 0;
		this.v2 = v2_ !== undefined ? v2_ : 0;
		this.width = width_ !== undefined ? width_ : 0;
		this.height = height_ !== undefined ? height_ : 0;
	});
	Texture = $pkg.Texture = $newType(0, "Struct", "enj.Texture", "Texture", "github.com/ajhager/enj", function(app_, tex_, img_, minFilter_, maxFilter_, uWrap_, vWrap_, mipmaps_) {
		this.$val = this;
		this.app = app_ !== undefined ? app_ : ($ptrType(App)).nil;
		this.tex = tex_ !== undefined ? tex_ : null;
		this.img = img_ !== undefined ? img_ : null;
		this.minFilter = minFilter_ !== undefined ? minFilter_ : 0;
		this.maxFilter = maxFilter_ !== undefined ? maxFilter_ : 0;
		this.uWrap = uWrap_ !== undefined ? uWrap_ : 0;
		this.vWrap = vWrap_ !== undefined ? vWrap_ : 0;
		this.mipmaps = mipmaps_ !== undefined ? mipmaps_ : false;
	});
	NewApp = $pkg.NewApp = function(width, height, fullscreen, container, responder) {
		var attrs, _tuple, canvas, err, app;
		attrs = webgl.DefaultAttributes();
		attrs.Alpha = false;
		attrs.Depth = false;
		attrs.PremultipliedAlpha = false;
		attrs.Antialias = false;
		_tuple = NewCanvas(width, height, fullscreen, container, attrs); canvas = _tuple[0]; err = _tuple[1];
		if (!($interfaceIsEqual(err, null))) {
			$panic(err);
		}
		app = new App.Ptr(responder, 0, false, 0, NewAssets(), canvas);
		canvas.OnLoss((function() {
			CancelAnimationFrame(app.requestId);
		}));
		canvas.OnRestored((function() {
			RequestAnimationFrame($methodVal(app, "animate"));
		}));
		canvas.Object.addEventListener($externalize("mousemove", $String), $externalize((function(ev) {
			var rect, x, x$1, x$2, x$3, x$4, y;
			rect = canvas.Object.getBoundingClientRect();
			x$2 = (x = (($parseInt(ev.clientX) >> 0) - ($parseInt(rect.left) >> 0) >> 0), x$1 = canvas.pixelRatio, (((x >>> 16 << 16) * x$1 >> 0) + (x << 16 >>> 16) * x$1) >> 0);
			y = (x$3 = (($parseInt(ev.clientY) >> 0) - ($parseInt(rect.top) >> 0) >> 0), x$4 = canvas.pixelRatio, (((x$3 >>> 16 << 16) * x$4 >> 0) + (x$3 << 16 >>> 16) * x$4) >> 0);
			responder.Mouse(x$2, y, 0);
		}), ($funcType([js.Object], [], false))), $externalize(false, $Bool));
		canvas.Object.addEventListener($externalize("mousedown", $String), $externalize((function(ev) {
			var rect, x, x$1, x$2, x$3, x$4, y;
			rect = canvas.Object.getBoundingClientRect();
			x$2 = (x = (($parseInt(ev.clientX) >> 0) - ($parseInt(rect.left) >> 0) >> 0), x$1 = canvas.pixelRatio, (((x >>> 16 << 16) * x$1 >> 0) + (x << 16 >>> 16) * x$1) >> 0);
			y = (x$3 = (($parseInt(ev.clientY) >> 0) - ($parseInt(rect.top) >> 0) >> 0), x$4 = canvas.pixelRatio, (((x$3 >>> 16 << 16) * x$4 >> 0) + (x$3 << 16 >>> 16) * x$4) >> 0);
			responder.Mouse(x$2, y, 1);
		}), ($funcType([js.Object], [], false))), $externalize(false, $Bool));
		canvas.Object.addEventListener($externalize("mouseup", $String), $externalize((function(ev) {
			var rect, x, x$1, x$2, x$3, x$4, y;
			rect = canvas.Object.getBoundingClientRect();
			x$2 = (x = (($parseInt(ev.clientX) >> 0) - ($parseInt(rect.left) >> 0) >> 0), x$1 = canvas.pixelRatio, (((x >>> 16 << 16) * x$1 >> 0) + (x << 16 >>> 16) * x$1) >> 0);
			y = (x$3 = (($parseInt(ev.clientY) >> 0) - ($parseInt(rect.top) >> 0) >> 0), x$4 = canvas.pixelRatio, (((x$3 >>> 16 << 16) * x$4 >> 0) + (x$3 << 16 >>> 16) * x$4) >> 0);
			responder.Mouse(x$2, y, 2);
		}), ($funcType([js.Object], [], false))), $externalize(false, $Bool));
		canvas.Object.addEventListener($externalize("touchstart", $String), $externalize((function(ev) {
			var rect, i, touch, x, x$1, x$2, x$3, x$4, y;
			rect = canvas.Object.getBoundingClientRect();
			i = 0;
			while (i < ($parseInt(ev.changedTouches.length) >> 0)) {
				touch = ev.changedTouches[i];
				x$2 = (x = (($parseInt(touch.clientX) >> 0) - ($parseInt(rect.left) >> 0) >> 0), x$1 = canvas.pixelRatio, (((x >>> 16 << 16) * x$1 >> 0) + (x << 16 >>> 16) * x$1) >> 0);
				y = (x$3 = (($parseInt(touch.clientY) >> 0) - ($parseInt(rect.top) >> 0) >> 0), x$4 = canvas.pixelRatio, (((x$3 >>> 16 << 16) * x$4 >> 0) + (x$3 << 16 >>> 16) * x$4) >> 0);
				responder.Mouse(x$2, y, 1);
				i = i + (1) >> 0;
			}
		}), ($funcType([js.Object], [], false))), $externalize(false, $Bool));
		canvas.Object.addEventListener($externalize("touchcancel", $String), $externalize((function(ev) {
			var rect, i, touch, x, x$1, x$2, x$3, x$4, y;
			rect = canvas.Object.getBoundingClientRect();
			i = 0;
			while (i < ($parseInt(ev.changedTouches.length) >> 0)) {
				touch = ev.changedTouches[i];
				x$2 = (x = (($parseInt(touch.clientX) >> 0) - ($parseInt(rect.left) >> 0) >> 0), x$1 = canvas.pixelRatio, (((x >>> 16 << 16) * x$1 >> 0) + (x << 16 >>> 16) * x$1) >> 0);
				y = (x$3 = (($parseInt(touch.clientY) >> 0) - ($parseInt(rect.top) >> 0) >> 0), x$4 = canvas.pixelRatio, (((x$3 >>> 16 << 16) * x$4 >> 0) + (x$3 << 16 >>> 16) * x$4) >> 0);
				responder.Mouse(x$2, y, 2);
				i = i + (1) >> 0;
			}
		}), ($funcType([js.Object], [], false))), $externalize(false, $Bool));
		canvas.Object.addEventListener($externalize("touchend", $String), $externalize((function(ev) {
			var rect, i, touch, x, x$1, x$2, x$3, x$4, y;
			rect = canvas.Object.getBoundingClientRect();
			i = 0;
			while (i < ($parseInt(ev.changedTouches.length) >> 0)) {
				touch = ev.changedTouches[i];
				x$2 = (x = (($parseInt(touch.clientX) >> 0) - ($parseInt(rect.left) >> 0) >> 0), x$1 = canvas.pixelRatio, (((x >>> 16 << 16) * x$1 >> 0) + (x << 16 >>> 16) * x$1) >> 0);
				y = (x$3 = (($parseInt(touch.clientY) >> 0) - ($parseInt(rect.top) >> 0) >> 0), x$4 = canvas.pixelRatio, (((x$3 >>> 16 << 16) * x$4 >> 0) + (x$3 << 16 >>> 16) * x$4) >> 0);
				responder.Mouse(x$2, y, 2);
				i = i + (1) >> 0;
			}
		}), ($funcType([js.Object], [], false))), $externalize(false, $Bool));
		canvas.Object.addEventListener($externalize("touchmove", $String), $externalize((function(ev) {
			var rect, i, touch, x, x$1, x$2, x$3, x$4, y;
			rect = canvas.Object.getBoundingClientRect();
			i = 0;
			while (i < ($parseInt(ev.changedTouches.length) >> 0)) {
				touch = ev.changedTouches[i];
				x$2 = (x = (($parseInt(touch.clientX) >> 0) - ($parseInt(rect.left) >> 0) >> 0), x$1 = canvas.pixelRatio, (((x >>> 16 << 16) * x$1 >> 0) + (x << 16 >>> 16) * x$1) >> 0);
				y = (x$3 = (($parseInt(touch.clientY) >> 0) - ($parseInt(rect.top) >> 0) >> 0), x$4 = canvas.pixelRatio, (((x$3 >>> 16 << 16) * x$4 >> 0) + (x$3 << 16 >>> 16) * x$4) >> 0);
				responder.Mouse(x$2, y, 0);
				i = i + (1) >> 0;
			}
		}), ($funcType([js.Object], [], false))), $externalize(false, $Bool));
		$global.addEventListener($externalize("keypress", $String), $externalize((function(ev) {
			responder.Key($parseInt(ev.charCode) >> 0, 3);
		}), ($funcType([js.Object], [], false))), $externalize(false, $Bool));
		$global.addEventListener($externalize("keydown", $String), $externalize((function(ev) {
			responder.Key($parseInt(ev.keyCode) >> 0, 4);
		}), ($funcType([js.Object], [], false))), $externalize(false, $Bool));
		$global.addEventListener($externalize("keyup", $String), $externalize((function(ev) {
			responder.Key($parseInt(ev.keyCode) >> 0, 5);
		}), ($funcType([js.Object], [], false))), $externalize(false, $Bool));
		$global.addEventListener($externalize("focus", $String), $externalize((function(ev) {
			app.justResumed = true;
		}), ($funcType([js.Object], [], false))), $externalize(false, $Bool));
		RequestAnimationFrame($methodVal(app, "create"));
		return app;
	};
	App.Ptr.prototype.create = function(dt) {
		var app;
		app = this;
		app.responder.Load();
		app.Load.Load((function() {
			app.responder.Setup();
			RequestAnimationFrame($methodVal(app, "animate"));
		}));
	};
	App.prototype.create = function(dt) { return this.$val.create(dt); };
	App.Ptr.prototype.animate = function(time) {
		var app;
		app = this;
		app.requestId = RequestAnimationFrame($methodVal(app, "animate"));
		if (app.justResumed) {
			app.lastTime = time;
			app.justResumed = false;
		}
		app.responder.Update((time - app.lastTime) / 1000);
		app.lastTime = time;
		app.Canvas.GL.Clear($parseInt(app.Canvas.GL.Object.COLOR_BUFFER_BIT) >> 0);
		app.responder.Draw();
	};
	App.prototype.animate = function(time) { return this.$val.animate(time); };
	App.Ptr.prototype.SetBgColor = function(r, g, b, a) {
		var app;
		app = this;
		app.Canvas.GL.ClearColor(r / 255, g / 255, b / 255, a / 255);
	};
	App.prototype.SetBgColor = function(r, g, b, a) { return this.$val.SetBgColor(r, g, b, a); };
	App.Ptr.prototype.NewTexture = function(path, mipmaps) {
		var app, _tuple, _entry, image, ok, texture;
		app = this;
		_tuple = (_entry = app.Load.cache[path], _entry !== undefined ? [_entry.v, true] : [null, false]); image = _tuple[0]; ok = _tuple[1];
		if (ok) {
			texture = NewTexture(app, image, mipmaps);
			return texture;
		}
		return ($ptrType(Texture)).nil;
	};
	App.prototype.NewTexture = function(path, mipmaps) { return this.$val.NewTexture(path, mipmaps); };
	App.Ptr.prototype.NewShader = function(vertSrc, fragSrc) {
		var app;
		app = this;
		return NewShader(app, vertSrc, fragSrc);
	};
	App.prototype.NewShader = function(vertSrc, fragSrc) { return this.$val.NewShader(vertSrc, fragSrc); };
	App.Ptr.prototype.NewBatch = function() {
		var app;
		app = this;
		return NewBatch(app);
	};
	App.prototype.NewBatch = function() { return this.$val.NewBatch(); };
	NewAssets = $pkg.NewAssets = function() {
		return new Assets.Ptr(($sliceType($String)).make(0), new $Map(), 0, 0);
	};
	Assets.Ptr.prototype.Image = function(path) {
		var a;
		a = this;
		a.queue = $append(a.queue, path);
	};
	Assets.prototype.Image = function(path) { return this.$val.Image(path); };
	Assets.Ptr.prototype.Load = function(onFinish) {
		var a, _ref, _i, path, img, _key;
		a = this;
		if (a.queue.$length === 0) {
			onFinish();
		} else {
			_ref = a.queue;
			_i = 0;
			while (_i < _ref.$length) {
				path = ((_i < 0 || _i >= _ref.$length) ? $throwRuntimeError("index out of range") : _ref.$array[_ref.$offset + _i]);
				img = new ($global.Image)();
				img.addEventListener($externalize("load", $String), $externalize((function() {
					a.loads = a.loads + (1) >> 0;
					if ((a.loads + a.errors >> 0) === a.queue.$length) {
						onFinish();
					}
				}), ($funcType([], [], false))), $externalize(false, $Bool));
				img.addEventListener($externalize("error", $String), $externalize((function() {
					a.errors = a.errors + (1) >> 0;
					if ((a.loads + a.errors >> 0) === a.queue.$length) {
						onFinish();
					}
				}), ($funcType([], [], false))), $externalize(false, $Bool));
				img.src = $externalize(path, $String);
				_key = path; (a.cache || $throwRuntimeError("assignment to entry in nil map"))[_key] = { k: _key, v: img };
				_i++;
			}
		}
	};
	Assets.prototype.Load = function(onFinish) { return this.$val.Load(onFinish); };
	colorToFloat = function(r, g, b, a) {
		var i;
		i = ((((((((((a >>> 0) << 24 >>> 0) | ((b >>> 0) << 16 >>> 0)) >>> 0) | ((g >>> 0) << 8 >>> 0)) >>> 0) | (r >>> 0)) >>> 0)) & 4278190079) >>> 0;
		i32[0] = i;
		return $parseFloat(f32[0]);
	};
	NewBatch = $pkg.NewBatch = function(app) {
		var batch, _tmp, _tmp$1, i, j, _tmp$2, _tmp$3, gl;
		batch = new Batch.Ptr();
		batch.app = app;
		batch.shader = app.NewShader(batchVert, batchFrag);
		batch.inPosition = batch.shader.GetAttrib("in_Position");
		batch.inColor = batch.shader.GetAttrib("in_Color");
		batch.inTexCoords = batch.shader.GetAttrib("in_TexCoords");
		batch.ufProjection = batch.shader.GetUniform("uf_Projection");
		batch.color = colorToFloat(255, 255, 255, 255);
		batch.vertices = new ($global.Float32Array)(200000);
		batch.indices = new ($global.Uint16Array)(60000);
		_tmp = 0; _tmp$1 = 0; i = _tmp; j = _tmp$1;
		while (i < 60000) {
			batch.indices[(i + 0 >> 0)] = j + 0 >> 0;
			batch.indices[(i + 1 >> 0)] = j + 1 >> 0;
			batch.indices[(i + 2 >> 0)] = j + 2 >> 0;
			batch.indices[(i + 3 >> 0)] = j + 2 >> 0;
			batch.indices[(i + 4 >> 0)] = j + 1 >> 0;
			batch.indices[(i + 5 >> 0)] = j + 3 >> 0;
			_tmp$2 = i + 6 >> 0; _tmp$3 = j + 4 >> 0; i = _tmp$2; j = _tmp$3;
		}
		gl = app.Canvas.GL;
		batch.vertexVBO = gl.CreateBuffer();
		batch.indexVBO = gl.CreateBuffer();
		gl.BindBuffer($parseInt(gl.Object.ELEMENT_ARRAY_BUFFER) >> 0, batch.indexVBO);
		gl.BufferData($parseInt(gl.Object.ELEMENT_ARRAY_BUFFER) >> 0, batch.indices, $parseInt(gl.Object.STATIC_DRAW) >> 0);
		gl.BindBuffer($parseInt(gl.Object.ARRAY_BUFFER) >> 0, batch.vertexVBO);
		gl.BufferData($parseInt(gl.Object.ARRAY_BUFFER) >> 0, batch.vertices, $parseInt(gl.Object.DYNAMIC_DRAW) >> 0);
		batch.projX = app.Canvas.Width() / 2;
		batch.projY = app.Canvas.Height() / 2;
		batch.blendingDisabled = false;
		batch.blendSrcFunc = $parseInt(gl.Object.SRC_ALPHA) >> 0;
		batch.blendDstFunc = $parseInt(gl.Object.ONE_MINUS_SRC_ALPHA) >> 0;
		return batch;
	};
	Batch.Ptr.prototype.Begin = function() {
		var b, shader;
		b = this;
		if (b.drawing) {
			$panic(new $String("Batch.End() must be called first"));
		}
		b.drawing = true;
		shader = b.shader;
		if (!(b.customShader === ($ptrType(Shader)).nil)) {
			shader = b.customShader;
		}
		shader.Bind();
	};
	Batch.prototype.Begin = function() { return this.$val.Begin(); };
	Batch.Ptr.prototype.End = function() {
		var b;
		b = this;
		if (!b.drawing) {
			$panic(new $String("Batch.Begin() must be called first"));
		}
		if (b.index > 0) {
			b.flush();
		}
		if (!b.blendingDisabled) {
			b.app.Canvas.GL.Disable($parseInt(b.app.Canvas.GL.Object.BLEND) >> 0);
		}
		b.drawing = false;
		b.app.Canvas.GL.BindBuffer($parseInt(b.app.Canvas.GL.Object.ARRAY_BUFFER) >> 0, null);
		b.app.Canvas.GL.UseProgram(null);
		b.lastTexture = ($ptrType(Texture)).nil;
	};
	Batch.prototype.End = function() { return this.$val.End(); };
	Batch.Ptr.prototype.flush = function() {
		var b, gl, x, view, x$1;
		b = this;
		if (b.lastTexture === ($ptrType(Texture)).nil) {
			return;
		}
		gl = b.app.Canvas.GL;
		if (b.blendingDisabled) {
			gl.Disable($parseInt(gl.Object.BLEND) >> 0);
		} else {
			gl.Enable($parseInt(gl.Object.BLEND) >> 0);
			gl.BlendFunc(b.blendSrcFunc, b.blendDstFunc);
		}
		b.lastTexture.Bind();
		gl.Uniform2f(b.ufProjection, b.projX, b.projY);
		gl.BindBuffer($parseInt(gl.Object.ARRAY_BUFFER) >> 0, b.vertexVBO);
		if (b.index > 5000) {
			gl.BufferSubData($parseInt(gl.Object.ARRAY_BUFFER) >> 0, 0, b.vertices);
		} else {
			view = b.vertices.subarray(0, (x = b.index, (((x >>> 16 << 16) * 20 >> 0) + (x << 16 >>> 16) * 20) >> 0));
			gl.BufferSubData($parseInt(gl.Object.ARRAY_BUFFER) >> 0, 0, view);
		}
		gl.EnableVertexAttribArray(b.inPosition);
		gl.EnableVertexAttribArray(b.inTexCoords);
		gl.EnableVertexAttribArray(b.inColor);
		gl.VertexAttribPointer(b.inPosition, 2, $parseInt(gl.Object.FLOAT) >> 0, false, 20, 0);
		gl.VertexAttribPointer(b.inTexCoords, 2, $parseInt(gl.Object.FLOAT) >> 0, false, 20, 8);
		gl.VertexAttribPointer(b.inColor, 4, $parseInt(gl.Object.UNSIGNED_BYTE) >> 0, true, 20, 16);
		gl.BindBuffer($parseInt(gl.Object.ELEMENT_ARRAY_BUFFER) >> 0, b.indexVBO);
		gl.DrawElements($parseInt(gl.Object.TRIANGLES) >> 0, (x$1 = b.index, (((x$1 >>> 16 << 16) * 6 >> 0) + (x$1 << 16 >>> 16) * 6) >> 0), $parseInt(gl.Object.UNSIGNED_SHORT) >> 0, 0);
		b.index = 0;
	};
	Batch.prototype.flush = function() { return this.$val.flush(); };
	Batch.Ptr.prototype.Draw = function(r, x, y, originX, originY, scaleX, scaleY, rotation) {
		var b, worldOriginX, worldOriginY, fx, fy, fx2, fy2, p1x, p1y, p2x, p2y, p3x, p3y, p4x, p4y, x1, y1, x2, y2, x3, y3, x4, y4, rot, cos, sin, x$1, idx;
		b = this;
		if (!b.drawing) {
			$panic(new $String("Batch.Begin() must be called first"));
		}
		if (!(r.texture === b.lastTexture)) {
			b.flush();
			b.lastTexture = r.texture;
		}
		worldOriginX = x + originX;
		worldOriginY = y + originY;
		fx = -originX;
		fy = -originY;
		fx2 = r.width - originX;
		fy2 = r.height - originY;
		if (!(($float32IsEqual(scaleX, 1))) || !(($float32IsEqual(scaleY, 1)))) {
			fx = fx * (scaleX);
			fy = fy * (scaleY);
			fx2 = fx2 * (scaleX);
			fy2 = fy2 * (scaleY);
		}
		p1x = fx;
		p1y = fy;
		p2x = fx;
		p2y = fy2;
		p3x = fx2;
		p3y = fy2;
		p4x = fx2;
		p4y = fy;
		x1 = 0;
		y1 = 0;
		x2 = 0;
		y2 = 0;
		x3 = 0;
		y3 = 0;
		x4 = 0;
		y4 = 0;
		if (!(($float32IsEqual(rotation, 0)))) {
			rot = $coerceFloat32(rotation * 0.01745329238474369);
			cos = math.Cos(rot);
			sin = math.Sin(rot);
			x1 = cos * p1x - sin * p1y;
			y1 = sin * p1x + cos * p1y;
			x2 = cos * p2x - sin * p2y;
			y2 = sin * p2x + cos * p2y;
			x3 = cos * p3x - sin * p3y;
			y3 = sin * p3x + cos * p3y;
			x4 = x1 + (x3 - x2);
			y4 = y3 - (y2 - y1);
		} else {
			x1 = p1x;
			y1 = p1y;
			x2 = p2x;
			y2 = p2y;
			x3 = p3x;
			y3 = p3y;
			x4 = p4x;
			y4 = p4y;
		}
		x1 = x1 + (worldOriginX);
		y1 = y1 + (worldOriginY);
		x2 = x2 + (worldOriginX);
		y2 = y2 + (worldOriginY);
		x3 = x3 + (worldOriginX);
		y3 = y3 + (worldOriginY);
		x4 = x4 + (worldOriginX);
		y4 = y4 + (worldOriginY);
		idx = (x$1 = b.index, (((x$1 >>> 16 << 16) * 20 >> 0) + (x$1 << 16 >>> 16) * 20) >> 0);
		b.vertices[(idx + 0 >> 0)] = x1;
		b.vertices[(idx + 1 >> 0)] = y1;
		b.vertices[(idx + 2 >> 0)] = r.u;
		b.vertices[(idx + 3 >> 0)] = r.v;
		b.vertices[(idx + 4 >> 0)] = b.color;
		b.vertices[(idx + 5 >> 0)] = x4;
		b.vertices[(idx + 6 >> 0)] = y4;
		b.vertices[(idx + 7 >> 0)] = r.u2;
		b.vertices[(idx + 8 >> 0)] = r.v;
		b.vertices[(idx + 9 >> 0)] = b.color;
		b.vertices[(idx + 10 >> 0)] = x2;
		b.vertices[(idx + 11 >> 0)] = y2;
		b.vertices[(idx + 12 >> 0)] = r.u;
		b.vertices[(idx + 13 >> 0)] = r.v2;
		b.vertices[(idx + 14 >> 0)] = b.color;
		b.vertices[(idx + 15 >> 0)] = x3;
		b.vertices[(idx + 16 >> 0)] = y3;
		b.vertices[(idx + 17 >> 0)] = r.u2;
		b.vertices[(idx + 18 >> 0)] = r.v2;
		b.vertices[(idx + 19 >> 0)] = b.color;
		b.index = b.index + (1) >> 0;
		if (b.index >= 10000) {
			b.flush();
		}
	};
	Batch.prototype.Draw = function(r, x, y, originX, originY, scaleX, scaleY, rotation) { return this.$val.Draw(r, x, y, originX, originY, scaleX, scaleY, rotation); };
	Batch.Ptr.prototype.SetColor = function(red, green, blue, alpha) {
		var b;
		b = this;
		b.color = colorToFloat(red, green, blue, alpha);
	};
	Batch.prototype.SetColor = function(red, green, blue, alpha) { return this.$val.SetColor(red, green, blue, alpha); };
	NewCanvas = $pkg.NewCanvas = function(width, height, fullscreen, container, ca) {
		var document, view, target, devicePixelRatio, _tuple, gl, err, canvas;
		document = $global.document;
		view = document.createElement($externalize("canvas", $String));
		target = document.getElementById($externalize(container, $String));
		if (target === null) {
			target = document.body;
		}
		target.appendChild(view);
		if (ca === ($ptrType(webgl.ContextAttributes)).nil) {
			ca = webgl.DefaultAttributes();
		}
		devicePixelRatio = $parseInt($global.devicePixelRatio) >> 0;
		if (devicePixelRatio < 1) {
			devicePixelRatio = 1;
		}
		_tuple = webgl.NewContext(view, ca); gl = _tuple[0]; err = _tuple[1];
		if (!($interfaceIsEqual(err, null))) {
			return [($ptrType(Canvas)).nil, err];
		}
		gl.Disable($parseInt(gl.Object.DEPTH_TEST) >> 0);
		gl.Disable($parseInt(gl.Object.CULL_FACE) >> 0);
		canvas = new Canvas.Ptr(view, gl, ca, 0, 0, devicePixelRatio, fullscreen, ($sliceType(Managed)).make(0));
		canvas.Resize(width, height, fullscreen);
		return [canvas, null];
	};
	Canvas.Ptr.prototype.Resize = function(width, height, fullscreen) {
		var c, x, x$1;
		c = this;
		c.width = width;
		c.height = height;
		c.Object.style.display = $externalize("block", $String);
		c.Object.width = (x = c.pixelRatio, (((width >>> 16 << 16) * x >> 0) + (width << 16 >>> 16) * x) >> 0);
		c.Object.height = (x$1 = c.pixelRatio, (((height >>> 16 << 16) * x$1 >> 0) + (height << 16 >>> 16) * x$1) >> 0);
		if (fullscreen) {
			c.Object.style.width = $parseInt($global.innerWidth) >> 0;
			c.Object.style.height = $parseInt($global.innerHeight) >> 0;
		} else {
			c.Object.style.width = width;
			c.Object.style.height = height;
		}
		c.GL.Object.viewport(0, 0, c.Object.width, c.Object.height);
	};
	Canvas.prototype.Resize = function(width, height, fullscreen) { return this.$val.Resize(width, height, fullscreen); };
	Canvas.Ptr.prototype.OnLoss = function(handler) {
		var c;
		c = this;
		c.Object.addEventListener($externalize("webglcontextlost", $String), $externalize((function(event) {
			event.preventDefault();
			handler();
		}), ($funcType([js.Object], [], false))), $externalize(false, $Bool));
	};
	Canvas.prototype.OnLoss = function(handler) { return this.$val.OnLoss(handler); };
	Canvas.Ptr.prototype.OnRestored = function(handler) {
		var c;
		c = this;
		c.Object.addEventListener($externalize("webglcontextrestored", $String), $externalize((function(event) {
			var _tuple, _ref, _i, resource;
			_tuple = webgl.NewContext(c.Object, c.attrs); c.GL = _tuple[0];
			c.Resize(c.width, c.height, c.fullscreen);
			_ref = c.resources;
			_i = 0;
			while (_i < _ref.$length) {
				resource = ((_i < 0 || _i >= _ref.$length) ? $throwRuntimeError("index out of range") : _ref.$array[_ref.$offset + _i]);
				resource.Create();
				_i++;
			}
			handler();
		}), ($funcType([js.Object], [], false))), $externalize(false, $Bool));
	};
	Canvas.prototype.OnRestored = function(handler) { return this.$val.OnRestored(handler); };
	Canvas.Ptr.prototype.Width = function() {
		var c;
		c = this;
		return c.width;
	};
	Canvas.prototype.Width = function() { return this.$val.Width(); };
	Canvas.Ptr.prototype.Height = function() {
		var c;
		c = this;
		return c.height;
	};
	Canvas.prototype.Height = function() { return this.$val.Height(); };
	Canvas.Ptr.prototype.PixelRatio = function() {
		var c;
		c = this;
		return c.pixelRatio;
	};
	Canvas.prototype.PixelRatio = function() { return this.$val.PixelRatio(); };
	Canvas.Ptr.prototype.AddResource = function(r) {
		var c;
		c = this;
		c.resources = $append(c.resources, r);
	};
	Canvas.prototype.AddResource = function(r) { return this.$val.AddResource(r); };
	init = function() {
		rafPolyfill();
	};
	rafPolyfill = function() {
		var window, vendors, i, vendor, lastTime;
		window = $global;
		vendors = new ($sliceType($String))(["ms", "moz", "webkit", "o"]);
		if (window.requestAnimationFrame === undefined) {
			i = 0;
			while (i < vendors.$length && (window.requestAnimationFrame === undefined)) {
				vendor = ((i < 0 || i >= vendors.$length) ? $throwRuntimeError("index out of range") : vendors.$array[vendors.$offset + i]);
				window.requestAnimationFrame = window[$externalize(vendor + "RequestAnimationFrame", $String)];
				window.cancelAnimationFrame = window[$externalize(vendor + "CancelAnimationFrame", $String)];
				if (window.cancelAnimationFrame === undefined) {
					window.cancelAnimationFrame = window[$externalize(vendor + "CancelRequestAnimationFrame", $String)];
				}
				i = i + (1) >> 0;
			}
		}
		lastTime = 0;
		if (window.requestAnimationFrame === undefined) {
			window.requestAnimationFrame = $externalize((function(callback) {
				var currTime, timeToCall, id;
				currTime = $parseFloat(new ($global.Date)().getTime());
				timeToCall = math.Max(0, 16 - (currTime - lastTime));
				id = window.setTimeout($externalize((function() {
					callback(currTime + timeToCall);
				}), ($funcType([], [], false))), timeToCall);
				lastTime = currTime + timeToCall;
				return $parseInt(id) >> 0;
			}), ($funcType([($funcType([$Float32], [], false))], [$Int], false)));
		}
		if (window.cancelAnimationFrame === undefined) {
			window.cancelAnimationFrame = $externalize((function(id) {
				$global.clearTimeout(id);
			}), ($funcType([$Int], [], false)));
		}
	};
	RequestAnimationFrame = $pkg.RequestAnimationFrame = function(callback) {
		return $parseInt($global.requestAnimationFrame($externalize(callback, ($funcType([$Float32], [], false))))) >> 0;
	};
	CancelAnimationFrame = $pkg.CancelAnimationFrame = function(id) {
		$global.cancelAnimationFrame();
	};
	Game.Ptr.prototype.Load = function() {
		var a;
		a = this;
	};
	Game.prototype.Load = function() { return this.$val.Load(); };
	Game.Ptr.prototype.Setup = function() {
		var a;
		a = this;
	};
	Game.prototype.Setup = function() { return this.$val.Setup(); };
	Game.Ptr.prototype.Update = function(dt) {
		var a;
		a = this;
	};
	Game.prototype.Update = function(dt) { return this.$val.Update(dt); };
	Game.Ptr.prototype.Draw = function() {
		var a;
		a = this;
	};
	Game.prototype.Draw = function() { return this.$val.Draw(); };
	Game.Ptr.prototype.Mouse = function(x, y, action) {
		var a;
		a = this;
	};
	Game.prototype.Mouse = function(x, y, action) { return this.$val.Mouse(x, y, action); };
	Game.Ptr.prototype.Key = function(key, action) {
		var a;
		a = this;
	};
	Game.prototype.Key = function(key, action) { return this.$val.Key(key, action); };
	NewShader = $pkg.NewShader = function(app, vertSrc, fragSrc) {
		var $deferred = [], $err = null, gl, vertShader, fragShader, program;
		/* */ try { $deferFrames.push($deferred);
		gl = app.Canvas.GL;
		vertShader = gl.CreateShader($parseInt(gl.Object.VERTEX_SHADER) >> 0);
		gl.ShaderSource(vertShader, vertSrc);
		gl.CompileShader(vertShader);
		if (gl.GetShaderParameterb(vertShader, $parseInt(gl.Object.COMPILE_STATUS) >> 0) === false) {
			console.log(gl.GetShaderInfoLog(vertShader));
		}
		$deferred.push([$methodVal(gl, "DeleteShader"), [vertShader]]);
		fragShader = gl.CreateShader($parseInt(gl.Object.FRAGMENT_SHADER) >> 0);
		gl.ShaderSource(fragShader, fragSrc);
		gl.CompileShader(fragShader);
		if (gl.GetShaderParameterb(fragShader, $parseInt(gl.Object.COMPILE_STATUS) >> 0) === false) {
			console.log(gl.GetShaderInfoLog(fragShader));
		}
		$deferred.push([$methodVal(gl, "DeleteShader"), [fragShader]]);
		program = gl.CreateProgram();
		gl.AttachShader(program, vertShader);
		gl.AttachShader(program, fragShader);
		gl.LinkProgram(program);
		if (!gl.GetProgramParameterb(program, $parseInt(gl.Object.LINK_STATUS) >> 0)) {
			console.log(gl.GetProgramInfoLog(program));
		}
		gl.ValidateProgram(program);
		if (!gl.GetProgramParameterb(program, $parseInt(gl.Object.VALIDATE_STATUS) >> 0)) {
			console.log(gl.GetProgramInfoLog(program));
		}
		return new Shader.Ptr(program, app);
		/* */ } catch(err) { $err = err; return ($ptrType(Shader)).nil; } finally { $deferFrames.pop(); $callDeferred($deferred, $err); }
	};
	Shader.Ptr.prototype.Bind = function() {
		var s;
		s = this;
		s.app.Canvas.GL.UseProgram(s.Object);
	};
	Shader.prototype.Bind = function() { return this.$val.Bind(); };
	Shader.Ptr.prototype.GetUniform = function(uniform) {
		var s;
		s = this;
		return s.app.Canvas.GL.GetUniformLocation(s.Object, uniform);
	};
	Shader.prototype.GetUniform = function(uniform) { return this.$val.GetUniform(uniform); };
	Shader.Ptr.prototype.GetAttrib = function(attrib) {
		var s;
		s = this;
		return s.app.Canvas.GL.GetAttribLocation(s.Object, attrib);
	};
	Shader.prototype.GetAttrib = function(attrib) { return this.$val.GetAttrib(attrib); };
	Region.Ptr.prototype.Flip = function(x, y) {
		var r, tmp, tmp$1;
		r = this;
		if (x) {
			tmp = r.u;
			r.u = r.u2;
			r.u2 = tmp;
		}
		if (y) {
			tmp$1 = r.v;
			r.v = r.v2;
			r.v2 = tmp$1;
		}
	};
	Region.prototype.Flip = function(x, y) { return this.$val.Flip(x, y); };
	Region.Ptr.prototype.Width = function() {
		var r;
		r = this;
		return r.width;
	};
	Region.prototype.Width = function() { return this.$val.Width(); };
	Region.Ptr.prototype.Height = function() {
		var r;
		r = this;
		return r.height;
	};
	Region.prototype.Height = function() { return this.$val.Height(); };
	NewTexture = $pkg.NewTexture = function(app, image, mipmaps) {
		var gl, texture;
		gl = app.Canvas.GL;
		texture = new Texture.Ptr(app, null, image, $parseInt(gl.Object.LINEAR) >> 0, $parseInt(gl.Object.LINEAR) >> 0, $parseInt(gl.Object.CLAMP_TO_EDGE) >> 0, $parseInt(gl.Object.CLAMP_TO_EDGE) >> 0, mipmaps);
		texture.Create();
		return texture;
	};
	Texture.Ptr.prototype.Create = function() {
		var t, gl;
		t = this;
		gl = t.app.Canvas.GL;
		t.tex = gl.CreateTexture();
		gl.BindTexture($parseInt(gl.Object.TEXTURE_2D) >> 0, t.tex);
		gl.TexParameteri($parseInt(gl.Object.TEXTURE_2D) >> 0, $parseInt(gl.Object.TEXTURE_WRAP_S) >> 0, t.uWrap);
		gl.TexParameteri($parseInt(gl.Object.TEXTURE_2D) >> 0, $parseInt(gl.Object.TEXTURE_WRAP_T) >> 0, t.vWrap);
		gl.TexParameteri($parseInt(gl.Object.TEXTURE_2D) >> 0, $parseInt(gl.Object.TEXTURE_MIN_FILTER) >> 0, t.minFilter);
		gl.TexParameteri($parseInt(gl.Object.TEXTURE_2D) >> 0, $parseInt(gl.Object.TEXTURE_MAG_FILTER) >> 0, t.maxFilter);
		if (t.mipmaps) {
			gl.GenerateMipmap($parseInt(gl.Object.TEXTURE_2D) >> 0);
		}
		gl.TexImage2D($parseInt(gl.Object.TEXTURE_2D) >> 0, 0, $parseInt(gl.Object.RGBA) >> 0, $parseInt(gl.Object.RGBA) >> 0, $parseInt(gl.Object.UNSIGNED_BYTE) >> 0, t.img);
		gl.BindTexture($parseInt(gl.Object.TEXTURE_2D) >> 0, null);
	};
	Texture.prototype.Create = function() { return this.$val.Create(); };
	Texture.Ptr.prototype.Bind = function() {
		var t;
		t = this;
		t.app.Canvas.GL.BindTexture($parseInt(t.app.Canvas.GL.Object.TEXTURE_2D) >> 0, t.tex);
	};
	Texture.prototype.Bind = function() { return this.$val.Bind(); };
	Texture.Ptr.prototype.Unbind = function() {
		var t;
		t = this;
		t.app.Canvas.GL.BindTexture($parseInt(t.app.Canvas.GL.Object.TEXTURE_2D) >> 0, null);
	};
	Texture.prototype.Unbind = function() { return this.$val.Unbind(); };
	Texture.Ptr.prototype.Width = function() {
		var t;
		t = this;
		return $parseInt(t.img.width) >> 0;
	};
	Texture.prototype.Width = function() { return this.$val.Width(); };
	Texture.Ptr.prototype.Height = function() {
		var t;
		t = this;
		return $parseInt(t.img.height) >> 0;
	};
	Texture.prototype.Height = function() { return this.$val.Height(); };
	Texture.Ptr.prototype.SetFilter = function(min, max) {
		var t;
		t = this;
		t.minFilter = min;
		t.maxFilter = max;
		t.Bind();
		t.app.Canvas.GL.TexParameteri($parseInt(t.app.Canvas.GL.Object.TEXTURE_2D) >> 0, $parseInt(t.app.Canvas.GL.Object.TEXTURE_MIN_FILTER) >> 0, min);
		t.app.Canvas.GL.TexParameteri($parseInt(t.app.Canvas.GL.Object.TEXTURE_2D) >> 0, $parseInt(t.app.Canvas.GL.Object.TEXTURE_MAG_FILTER) >> 0, max);
		t.Unbind();
	};
	Texture.prototype.SetFilter = function(min, max) { return this.$val.SetFilter(min, max); };
	Texture.Ptr.prototype.Filter = function() {
		var t;
		t = this;
		return [t.minFilter, t.maxFilter];
	};
	Texture.prototype.Filter = function() { return this.$val.Filter(); };
	Texture.Ptr.prototype.SetWrap = function(u, v) {
		var t;
		t = this;
		t.uWrap = u;
		t.vWrap = v;
		t.Bind();
		t.app.Canvas.GL.TexParameteri($parseInt(t.app.Canvas.GL.Object.TEXTURE_2D) >> 0, $parseInt(t.app.Canvas.GL.Object.TEXTURE_WRAP_S) >> 0, u);
		t.app.Canvas.GL.TexParameteri($parseInt(t.app.Canvas.GL.Object.TEXTURE_2D) >> 0, $parseInt(t.app.Canvas.GL.Object.TEXTURE_WRAP_T) >> 0, v);
		t.Unbind();
	};
	Texture.prototype.SetWrap = function(u, v) { return this.$val.SetWrap(u, v); };
	Texture.Ptr.prototype.Wrap = function() {
		var t;
		t = this;
		return [t.uWrap, t.vWrap];
	};
	Texture.prototype.Wrap = function() { return this.$val.Wrap(); };
	Texture.Ptr.prototype.Region = function(x, y, w, h) {
		var t, invTexWidth, invTexHeight, u, v, u2, v2, width, height;
		t = this;
		invTexWidth = 1 / t.Width();
		invTexHeight = 1 / t.Height();
		u = x * invTexWidth;
		v = y * invTexHeight;
		u2 = (x + w >> 0) * invTexWidth;
		v2 = (y + h >> 0) * invTexHeight;
		width = math.Abs(w);
		height = math.Abs(h);
		return new Region.Ptr(t, u, v, u2, v2, width, height);
	};
	Texture.prototype.Region = function(x, y, w, h) { return this.$val.Region(x, y, w, h); };
	Texture.Ptr.prototype.Split = function(w, h) {
		var t, x, y, width, height, _q, rows, _q$1, cols, startX, tiles, row, col;
		t = this;
		x = 0;
		y = 0;
		width = t.Width();
		height = t.Height();
		rows = (_q = height / h, (_q === _q && _q !== 1/0 && _q !== -1/0) ? _q >> 0 : $throwRuntimeError("integer divide by zero"));
		cols = (_q$1 = width / w, (_q$1 === _q$1 && _q$1 !== 1/0 && _q$1 !== -1/0) ? _q$1 >> 0 : $throwRuntimeError("integer divide by zero"));
		startX = x;
		tiles = ($sliceType(($ptrType(Region)))).make(0);
		row = 0;
		while (row < rows) {
			x = startX;
			col = 0;
			while (col < cols) {
				tiles = $append(tiles, t.Region(x, y, w, h));
				x = x + (w) >> 0;
				col = col + (1) >> 0;
			}
			y = y + (h) >> 0;
			row = row + (1) >> 0;
		}
		return tiles;
	};
	Texture.prototype.Split = function(w, h) { return this.$val.Split(w, h); };
	$pkg.$init = function() {
		App.methods = [["AddResource", "AddResource", "", [Managed], [], false, 5], ["Bool", "Bool", "", [], [$Bool], false, 5], ["Call", "Call", "", [$String, ($sliceType($emptyInterface))], [js.Object], true, 5], ["Delete", "Delete", "", [$String], [], false, 5], ["Float", "Float", "", [], [$Float64], false, 5], ["Get", "Get", "", [$String], [js.Object], false, 5], ["Height", "Height", "", [], [$Float32], false, 5], ["Index", "Index", "", [$Int], [js.Object], false, 5], ["Int", "Int", "", [], [$Int], false, 5], ["Int64", "Int64", "", [], [$Int64], false, 5], ["Interface", "Interface", "", [], [$emptyInterface], false, 5], ["Invoke", "Invoke", "", [($sliceType($emptyInterface))], [js.Object], true, 5], ["IsNull", "IsNull", "", [], [$Bool], false, 5], ["IsUndefined", "IsUndefined", "", [], [$Bool], false, 5], ["Length", "Length", "", [], [$Int], false, 5], ["New", "New", "", [($sliceType($emptyInterface))], [js.Object], true, 5], ["OnLoss", "OnLoss", "", [($funcType([], [], false))], [], false, 5], ["OnRestored", "OnRestored", "", [($funcType([], [], false))], [], false, 5], ["PixelRatio", "PixelRatio", "", [], [$Int], false, 5], ["Resize", "Resize", "", [$Int, $Int, $Bool], [], false, 5], ["Set", "Set", "", [$String, $emptyInterface], [], false, 5], ["SetIndex", "SetIndex", "", [$Int, $emptyInterface], [], false, 5], ["Str", "Str", "", [], [$String], false, 5], ["Uint64", "Uint64", "", [], [$Uint64], false, 5], ["Unsafe", "Unsafe", "", [], [$Uintptr], false, 5], ["Width", "Width", "", [], [$Float32], false, 5]];
		($ptrType(App)).methods = [["AddResource", "AddResource", "", [Managed], [], false, 5], ["Bool", "Bool", "", [], [$Bool], false, 5], ["Call", "Call", "", [$String, ($sliceType($emptyInterface))], [js.Object], true, 5], ["Delete", "Delete", "", [$String], [], false, 5], ["Float", "Float", "", [], [$Float64], false, 5], ["Get", "Get", "", [$String], [js.Object], false, 5], ["Height", "Height", "", [], [$Float32], false, 5], ["Index", "Index", "", [$Int], [js.Object], false, 5], ["Int", "Int", "", [], [$Int], false, 5], ["Int64", "Int64", "", [], [$Int64], false, 5], ["Interface", "Interface", "", [], [$emptyInterface], false, 5], ["Invoke", "Invoke", "", [($sliceType($emptyInterface))], [js.Object], true, 5], ["IsNull", "IsNull", "", [], [$Bool], false, 5], ["IsUndefined", "IsUndefined", "", [], [$Bool], false, 5], ["Length", "Length", "", [], [$Int], false, 5], ["New", "New", "", [($sliceType($emptyInterface))], [js.Object], true, 5], ["NewBatch", "NewBatch", "", [], [($ptrType(Batch))], false, -1], ["NewShader", "NewShader", "", [$String, $String], [($ptrType(Shader))], false, -1], ["NewTexture", "NewTexture", "", [$String, $Bool], [($ptrType(Texture))], false, -1], ["OnLoss", "OnLoss", "", [($funcType([], [], false))], [], false, 5], ["OnRestored", "OnRestored", "", [($funcType([], [], false))], [], false, 5], ["PixelRatio", "PixelRatio", "", [], [$Int], false, 5], ["Resize", "Resize", "", [$Int, $Int, $Bool], [], false, 5], ["Set", "Set", "", [$String, $emptyInterface], [], false, 5], ["SetBgColor", "SetBgColor", "", [$Uint8, $Uint8, $Uint8, $Uint8], [], false, -1], ["SetIndex", "SetIndex", "", [$Int, $emptyInterface], [], false, 5], ["Str", "Str", "", [], [$String], false, 5], ["Uint64", "Uint64", "", [], [$Uint64], false, 5], ["Unsafe", "Unsafe", "", [], [$Uintptr], false, 5], ["Width", "Width", "", [], [$Float32], false, 5], ["animate", "animate", "github.com/ajhager/enj", [$Float32], [], false, -1], ["create", "create", "github.com/ajhager/enj", [$Float32], [], false, -1]];
		App.init([["responder", "responder", "github.com/ajhager/enj", Responder, ""], ["lastTime", "lastTime", "github.com/ajhager/enj", $Float32, ""], ["justResumed", "justResumed", "github.com/ajhager/enj", $Bool, ""], ["requestId", "requestId", "github.com/ajhager/enj", $Int, ""], ["Load", "Load", "", ($ptrType(Assets)), ""], ["Canvas", "", "", ($ptrType(Canvas)), ""]]);
		($ptrType(Assets)).methods = [["Image", "Image", "", [$String], [], false, -1], ["Load", "Load", "", [($funcType([], [], false))], [], false, -1]];
		Assets.init([["queue", "queue", "github.com/ajhager/enj", ($sliceType($String)), ""], ["cache", "cache", "github.com/ajhager/enj", ($mapType($String, js.Object)), ""], ["loads", "loads", "github.com/ajhager/enj", $Int, ""], ["errors", "errors", "github.com/ajhager/enj", $Int, ""]]);
		($ptrType(Batch)).methods = [["Begin", "Begin", "", [], [], false, -1], ["Draw", "Draw", "", [($ptrType(Region)), $Float32, $Float32, $Float32, $Float32, $Float32, $Float32, $Float32], [], false, -1], ["End", "End", "", [], [], false, -1], ["SetColor", "SetColor", "", [$Uint8, $Uint8, $Uint8, $Uint8], [], false, -1], ["flush", "flush", "github.com/ajhager/enj", [], [], false, -1]];
		Batch.init([["app", "app", "github.com/ajhager/enj", ($ptrType(App)), ""], ["drawing", "drawing", "github.com/ajhager/enj", $Bool, ""], ["lastTexture", "lastTexture", "github.com/ajhager/enj", ($ptrType(Texture)), ""], ["color", "color", "github.com/ajhager/enj", $Float32, ""], ["vertices", "vertices", "github.com/ajhager/enj", js.Object, ""], ["vertexVBO", "vertexVBO", "github.com/ajhager/enj", js.Object, ""], ["indices", "indices", "github.com/ajhager/enj", js.Object, ""], ["indexVBO", "indexVBO", "github.com/ajhager/enj", js.Object, ""], ["index", "index", "github.com/ajhager/enj", $Int, ""], ["shader", "shader", "github.com/ajhager/enj", ($ptrType(Shader)), ""], ["customShader", "customShader", "github.com/ajhager/enj", ($ptrType(Shader)), ""], ["blendingDisabled", "blendingDisabled", "github.com/ajhager/enj", $Bool, ""], ["blendSrcFunc", "blendSrcFunc", "github.com/ajhager/enj", $Int, ""], ["blendDstFunc", "blendDstFunc", "github.com/ajhager/enj", $Int, ""], ["inPosition", "inPosition", "github.com/ajhager/enj", $Int, ""], ["inColor", "inColor", "github.com/ajhager/enj", $Int, ""], ["inTexCoords", "inTexCoords", "github.com/ajhager/enj", $Int, ""], ["ufProjection", "ufProjection", "github.com/ajhager/enj", js.Object, ""], ["projX", "projX", "github.com/ajhager/enj", $Float32, ""], ["projY", "projY", "github.com/ajhager/enj", $Float32, ""]]);
		Managed.init([["Create", "Create", "", [], [], false]]);
		Canvas.methods = [["Bool", "Bool", "", [], [$Bool], false, 0], ["Call", "Call", "", [$String, ($sliceType($emptyInterface))], [js.Object], true, 0], ["Delete", "Delete", "", [$String], [], false, 0], ["Float", "Float", "", [], [$Float64], false, 0], ["Get", "Get", "", [$String], [js.Object], false, 0], ["Index", "Index", "", [$Int], [js.Object], false, 0], ["Int", "Int", "", [], [$Int], false, 0], ["Int64", "Int64", "", [], [$Int64], false, 0], ["Interface", "Interface", "", [], [$emptyInterface], false, 0], ["Invoke", "Invoke", "", [($sliceType($emptyInterface))], [js.Object], true, 0], ["IsNull", "IsNull", "", [], [$Bool], false, 0], ["IsUndefined", "IsUndefined", "", [], [$Bool], false, 0], ["Length", "Length", "", [], [$Int], false, 0], ["New", "New", "", [($sliceType($emptyInterface))], [js.Object], true, 0], ["Set", "Set", "", [$String, $emptyInterface], [], false, 0], ["SetIndex", "SetIndex", "", [$Int, $emptyInterface], [], false, 0], ["Str", "Str", "", [], [$String], false, 0], ["Uint64", "Uint64", "", [], [$Uint64], false, 0], ["Unsafe", "Unsafe", "", [], [$Uintptr], false, 0]];
		($ptrType(Canvas)).methods = [["AddResource", "AddResource", "", [Managed], [], false, -1], ["Bool", "Bool", "", [], [$Bool], false, 0], ["Call", "Call", "", [$String, ($sliceType($emptyInterface))], [js.Object], true, 0], ["Delete", "Delete", "", [$String], [], false, 0], ["Float", "Float", "", [], [$Float64], false, 0], ["Get", "Get", "", [$String], [js.Object], false, 0], ["Height", "Height", "", [], [$Float32], false, -1], ["Index", "Index", "", [$Int], [js.Object], false, 0], ["Int", "Int", "", [], [$Int], false, 0], ["Int64", "Int64", "", [], [$Int64], false, 0], ["Interface", "Interface", "", [], [$emptyInterface], false, 0], ["Invoke", "Invoke", "", [($sliceType($emptyInterface))], [js.Object], true, 0], ["IsNull", "IsNull", "", [], [$Bool], false, 0], ["IsUndefined", "IsUndefined", "", [], [$Bool], false, 0], ["Length", "Length", "", [], [$Int], false, 0], ["New", "New", "", [($sliceType($emptyInterface))], [js.Object], true, 0], ["OnLoss", "OnLoss", "", [($funcType([], [], false))], [], false, -1], ["OnRestored", "OnRestored", "", [($funcType([], [], false))], [], false, -1], ["PixelRatio", "PixelRatio", "", [], [$Int], false, -1], ["Resize", "Resize", "", [$Int, $Int, $Bool], [], false, -1], ["Set", "Set", "", [$String, $emptyInterface], [], false, 0], ["SetIndex", "SetIndex", "", [$Int, $emptyInterface], [], false, 0], ["Str", "Str", "", [], [$String], false, 0], ["Uint64", "Uint64", "", [], [$Uint64], false, 0], ["Unsafe", "Unsafe", "", [], [$Uintptr], false, 0], ["Width", "Width", "", [], [$Float32], false, -1]];
		Canvas.init([["Object", "", "", js.Object, ""], ["GL", "GL", "", ($ptrType(webgl.Context)), ""], ["attrs", "attrs", "github.com/ajhager/enj", ($ptrType(webgl.ContextAttributes)), ""], ["width", "width", "github.com/ajhager/enj", $Int, ""], ["height", "height", "github.com/ajhager/enj", $Int, ""], ["pixelRatio", "pixelRatio", "github.com/ajhager/enj", $Int, ""], ["fullscreen", "fullscreen", "github.com/ajhager/enj", $Bool, ""], ["resources", "resources", "github.com/ajhager/enj", ($sliceType(Managed)), ""]]);
		Responder.init([["Draw", "Draw", "", [], [], false], ["Key", "Key", "", [$Int, $Int], [], false], ["Load", "Load", "", [], [], false], ["Mouse", "Mouse", "", [$Float32, $Float32, $Int], [], false], ["Setup", "Setup", "", [], [], false], ["Update", "Update", "", [$Float32], [], false]]);
		($ptrType(Game)).methods = [["Draw", "Draw", "", [], [], false, -1], ["Key", "Key", "", [$Int, $Int], [], false, -1], ["Load", "Load", "", [], [], false, -1], ["Mouse", "Mouse", "", [$Float32, $Float32, $Int], [], false, -1], ["Setup", "Setup", "", [], [], false, -1], ["Update", "Update", "", [$Float32], [], false, -1]];
		Game.init([]);
		Shader.methods = [["Bool", "Bool", "", [], [$Bool], false, 0], ["Call", "Call", "", [$String, ($sliceType($emptyInterface))], [js.Object], true, 0], ["Delete", "Delete", "", [$String], [], false, 0], ["Float", "Float", "", [], [$Float64], false, 0], ["Get", "Get", "", [$String], [js.Object], false, 0], ["Index", "Index", "", [$Int], [js.Object], false, 0], ["Int", "Int", "", [], [$Int], false, 0], ["Int64", "Int64", "", [], [$Int64], false, 0], ["Interface", "Interface", "", [], [$emptyInterface], false, 0], ["Invoke", "Invoke", "", [($sliceType($emptyInterface))], [js.Object], true, 0], ["IsNull", "IsNull", "", [], [$Bool], false, 0], ["IsUndefined", "IsUndefined", "", [], [$Bool], false, 0], ["Length", "Length", "", [], [$Int], false, 0], ["New", "New", "", [($sliceType($emptyInterface))], [js.Object], true, 0], ["Set", "Set", "", [$String, $emptyInterface], [], false, 0], ["SetIndex", "SetIndex", "", [$Int, $emptyInterface], [], false, 0], ["Str", "Str", "", [], [$String], false, 0], ["Uint64", "Uint64", "", [], [$Uint64], false, 0], ["Unsafe", "Unsafe", "", [], [$Uintptr], false, 0]];
		($ptrType(Shader)).methods = [["Bind", "Bind", "", [], [], false, -1], ["Bool", "Bool", "", [], [$Bool], false, 0], ["Call", "Call", "", [$String, ($sliceType($emptyInterface))], [js.Object], true, 0], ["Delete", "Delete", "", [$String], [], false, 0], ["Float", "Float", "", [], [$Float64], false, 0], ["Get", "Get", "", [$String], [js.Object], false, 0], ["GetAttrib", "GetAttrib", "", [$String], [$Int], false, -1], ["GetUniform", "GetUniform", "", [$String], [js.Object], false, -1], ["Index", "Index", "", [$Int], [js.Object], false, 0], ["Int", "Int", "", [], [$Int], false, 0], ["Int64", "Int64", "", [], [$Int64], false, 0], ["Interface", "Interface", "", [], [$emptyInterface], false, 0], ["Invoke", "Invoke", "", [($sliceType($emptyInterface))], [js.Object], true, 0], ["IsNull", "IsNull", "", [], [$Bool], false, 0], ["IsUndefined", "IsUndefined", "", [], [$Bool], false, 0], ["Length", "Length", "", [], [$Int], false, 0], ["New", "New", "", [($sliceType($emptyInterface))], [js.Object], true, 0], ["Set", "Set", "", [$String, $emptyInterface], [], false, 0], ["SetIndex", "SetIndex", "", [$Int, $emptyInterface], [], false, 0], ["Str", "Str", "", [], [$String], false, 0], ["Uint64", "Uint64", "", [], [$Uint64], false, 0], ["Unsafe", "Unsafe", "", [], [$Uintptr], false, 0]];
		Shader.init([["Object", "", "", js.Object, ""], ["app", "app", "github.com/ajhager/enj", ($ptrType(App)), ""]]);
		($ptrType(Region)).methods = [["Flip", "Flip", "", [$Bool, $Bool], [], false, -1], ["Height", "Height", "", [], [$Float32], false, -1], ["Width", "Width", "", [], [$Float32], false, -1]];
		Region.init([["texture", "texture", "github.com/ajhager/enj", ($ptrType(Texture)), ""], ["u", "u", "github.com/ajhager/enj", $Float32, ""], ["v", "v", "github.com/ajhager/enj", $Float32, ""], ["u2", "u2", "github.com/ajhager/enj", $Float32, ""], ["v2", "v2", "github.com/ajhager/enj", $Float32, ""], ["width", "width", "github.com/ajhager/enj", $Float32, ""], ["height", "height", "github.com/ajhager/enj", $Float32, ""]]);
		($ptrType(Texture)).methods = [["Bind", "Bind", "", [], [], false, -1], ["Create", "Create", "", [], [], false, -1], ["Filter", "Filter", "", [], [$Int, $Int], false, -1], ["Height", "Height", "", [], [$Int], false, -1], ["Region", "Region", "", [$Int, $Int, $Int, $Int], [($ptrType(Region))], false, -1], ["SetFilter", "SetFilter", "", [$Int, $Int], [], false, -1], ["SetWrap", "SetWrap", "", [$Int, $Int], [], false, -1], ["Split", "Split", "", [$Int, $Int], [($sliceType(($ptrType(Region))))], false, -1], ["Unbind", "Unbind", "", [], [], false, -1], ["Width", "Width", "", [], [$Int], false, -1], ["Wrap", "Wrap", "", [], [$Int, $Int], false, -1]];
		Texture.init([["app", "app", "github.com/ajhager/enj", ($ptrType(App)), ""], ["tex", "tex", "github.com/ajhager/enj", js.Object, ""], ["img", "img", "github.com/ajhager/enj", js.Object, ""], ["minFilter", "minFilter", "github.com/ajhager/enj", $Int, ""], ["maxFilter", "maxFilter", "github.com/ajhager/enj", $Int, ""], ["uWrap", "uWrap", "github.com/ajhager/enj", $Int, ""], ["vWrap", "vWrap", "github.com/ajhager/enj", $Int, ""], ["mipmaps", "mipmaps", "github.com/ajhager/enj", $Bool, ""]]);
		i8 = new ($global.Int8Array)(4);
		i32 = new ($global.Int32Array)(i8.buffer, 0, 1);
		f32 = new ($global.Float32Array)(i8.buffer, 0, 1);
		batchVert = " \nattribute vec4 in_Position;\nattribute vec4 in_Color;\nattribute vec2 in_TexCoords;\n\nuniform vec2 uf_Projection;\n\nvarying vec4 var_Color;\nvarying vec2 var_TexCoords;\n\nvoid main() {\n  var_Color = in_Color;\n  var_TexCoords = in_TexCoords;\n  gl_Position = vec4(in_Position.x / uf_Projection.x - 1.0,\n                     in_Position.y / -uf_Projection.y + 1.0,\n                     0.0, 1.0);\n}\n";
		batchFrag = "\nprecision lowp float;\n\nvarying vec4 var_Color;\nvarying vec2 var_TexCoords;\n\nuniform sampler2D uf_Texture;\n\nvoid main (void) {\n  gl_FragColor = var_Color * texture2D (uf_Texture, var_TexCoords);\n}\n";
		init();
	};
	return $pkg;
})();
$packages["sync/atomic"] = (function() {
	var $pkg = {}, CompareAndSwapInt32, AddInt32;
	CompareAndSwapInt32 = $pkg.CompareAndSwapInt32 = function(addr, old, new$1) {
		if (addr.$get() === old) {
			addr.$set(new$1);
			return true;
		}
		return false;
	};
	AddInt32 = $pkg.AddInt32 = function(addr, delta) {
		var new$1;
		new$1 = addr.$get() + delta >> 0;
		addr.$set(new$1);
		return new$1;
	};
	$pkg.$init = function() {
	};
	return $pkg;
})();
$packages["sync"] = (function() {
	var $pkg = {}, atomic = $packages["sync/atomic"], runtime = $packages["runtime"], Pool, Mutex, poolLocal, syncSema, allPools, runtime_registerPoolCleanup, runtime_Syncsemcheck, poolCleanup, init, indexLocal, runtime_Semacquire, runtime_Semrelease, init$1;
	Pool = $pkg.Pool = $newType(0, "Struct", "sync.Pool", "Pool", "sync", function(local_, localSize_, store_, New_) {
		this.$val = this;
		this.local = local_ !== undefined ? local_ : 0;
		this.localSize = localSize_ !== undefined ? localSize_ : 0;
		this.store = store_ !== undefined ? store_ : ($sliceType($emptyInterface)).nil;
		this.New = New_ !== undefined ? New_ : $throwNilPointerError;
	});
	Mutex = $pkg.Mutex = $newType(0, "Struct", "sync.Mutex", "Mutex", "sync", function(state_, sema_) {
		this.$val = this;
		this.state = state_ !== undefined ? state_ : 0;
		this.sema = sema_ !== undefined ? sema_ : 0;
	});
	poolLocal = $pkg.poolLocal = $newType(0, "Struct", "sync.poolLocal", "poolLocal", "sync", function(private$0_, shared_, Mutex_, pad_) {
		this.$val = this;
		this.private$0 = private$0_ !== undefined ? private$0_ : null;
		this.shared = shared_ !== undefined ? shared_ : ($sliceType($emptyInterface)).nil;
		this.Mutex = Mutex_ !== undefined ? Mutex_ : new Mutex.Ptr();
		this.pad = pad_ !== undefined ? pad_ : ($arrayType($Uint8, 128)).zero();
	});
	syncSema = $pkg.syncSema = $newType(12, "Array", "sync.syncSema", "syncSema", "sync", null);
	Pool.Ptr.prototype.Get = function() {
		var p, x, x$1, x$2;
		p = this;
		if (p.store.$length === 0) {
			if (!(p.New === $throwNilPointerError)) {
				return p.New();
			}
			return null;
		}
		x$2 = (x = p.store, x$1 = p.store.$length - 1 >> 0, ((x$1 < 0 || x$1 >= x.$length) ? $throwRuntimeError("index out of range") : x.$array[x.$offset + x$1]));
		p.store = $subslice(p.store, 0, (p.store.$length - 1 >> 0));
		return x$2;
	};
	Pool.prototype.Get = function() { return this.$val.Get(); };
	Pool.Ptr.prototype.Put = function(x) {
		var p;
		p = this;
		if ($interfaceIsEqual(x, null)) {
			return;
		}
		p.store = $append(p.store, x);
	};
	Pool.prototype.Put = function(x) { return this.$val.Put(x); };
	runtime_registerPoolCleanup = function(cleanup) {
	};
	runtime_Syncsemcheck = function(size) {
	};
	Mutex.Ptr.prototype.Lock = function() {
		var m, awoke, old, new$1;
		m = this;
		if (atomic.CompareAndSwapInt32(new ($ptrType($Int32))(function() { return this.$target.state; }, function($v) { this.$target.state = $v; }, m), 0, 1)) {
			return;
		}
		awoke = false;
		while (true) {
			old = m.state;
			new$1 = old | 1;
			if (!(((old & 1) === 0))) {
				new$1 = old + 4 >> 0;
			}
			if (awoke) {
				new$1 = new$1 & ~(2);
			}
			if (atomic.CompareAndSwapInt32(new ($ptrType($Int32))(function() { return this.$target.state; }, function($v) { this.$target.state = $v; }, m), old, new$1)) {
				if ((old & 1) === 0) {
					break;
				}
				runtime_Semacquire(new ($ptrType($Uint32))(function() { return this.$target.sema; }, function($v) { this.$target.sema = $v; }, m));
				awoke = true;
			}
		}
	};
	Mutex.prototype.Lock = function() { return this.$val.Lock(); };
	Mutex.Ptr.prototype.Unlock = function() {
		var m, new$1, old;
		m = this;
		new$1 = atomic.AddInt32(new ($ptrType($Int32))(function() { return this.$target.state; }, function($v) { this.$target.state = $v; }, m), -1);
		if ((((new$1 + 1 >> 0)) & 1) === 0) {
			$panic(new $String("sync: unlock of unlocked mutex"));
		}
		old = new$1;
		while (true) {
			if (((old >> 2 >> 0) === 0) || !(((old & 3) === 0))) {
				return;
			}
			new$1 = ((old - 4 >> 0)) | 2;
			if (atomic.CompareAndSwapInt32(new ($ptrType($Int32))(function() { return this.$target.state; }, function($v) { this.$target.state = $v; }, m), old, new$1)) {
				runtime_Semrelease(new ($ptrType($Uint32))(function() { return this.$target.sema; }, function($v) { this.$target.sema = $v; }, m));
				return;
			}
			old = m.state;
		}
	};
	Mutex.prototype.Unlock = function() { return this.$val.Unlock(); };
	poolCleanup = function() {
		var _ref, _i, i, p, i$1, l, _ref$1, _i$1, j, x;
		_ref = allPools;
		_i = 0;
		while (_i < _ref.$length) {
			i = _i;
			p = ((_i < 0 || _i >= _ref.$length) ? $throwRuntimeError("index out of range") : _ref.$array[_ref.$offset + _i]);
			(i < 0 || i >= allPools.$length) ? $throwRuntimeError("index out of range") : allPools.$array[allPools.$offset + i] = ($ptrType(Pool)).nil;
			i$1 = 0;
			while (i$1 < (p.localSize >> 0)) {
				l = indexLocal(p.local, i$1);
				l.private$0 = null;
				_ref$1 = l.shared;
				_i$1 = 0;
				while (_i$1 < _ref$1.$length) {
					j = _i$1;
					(x = l.shared, (j < 0 || j >= x.$length) ? $throwRuntimeError("index out of range") : x.$array[x.$offset + j] = null);
					_i$1++;
				}
				l.shared = ($sliceType($emptyInterface)).nil;
				i$1 = i$1 + (1) >> 0;
			}
			_i++;
		}
		allPools = new ($sliceType(($ptrType(Pool))))([]);
	};
	init = function() {
		runtime_registerPoolCleanup(poolCleanup);
	};
	indexLocal = function(l, i) {
		var x;
		return (x = l, ((i < 0 || i >= x.length) ? $throwRuntimeError("index out of range") : x[i]));
	};
	runtime_Semacquire = function() {
		$panic("Native function not implemented: sync.runtime_Semacquire");
	};
	runtime_Semrelease = function() {
		$panic("Native function not implemented: sync.runtime_Semrelease");
	};
	init$1 = function() {
		var s;
		s = syncSema.zero(); $copy(s, syncSema.zero(), syncSema);
		runtime_Syncsemcheck(12);
	};
	$pkg.$init = function() {
		($ptrType(Pool)).methods = [["Get", "Get", "", [], [$emptyInterface], false, -1], ["Put", "Put", "", [$emptyInterface], [], false, -1], ["getSlow", "getSlow", "sync", [], [$emptyInterface], false, -1], ["pin", "pin", "sync", [], [($ptrType(poolLocal))], false, -1], ["pinSlow", "pinSlow", "sync", [], [($ptrType(poolLocal))], false, -1]];
		Pool.init([["local", "local", "sync", $UnsafePointer, ""], ["localSize", "localSize", "sync", $Uintptr, ""], ["store", "store", "sync", ($sliceType($emptyInterface)), ""], ["New", "New", "", ($funcType([], [$emptyInterface], false)), ""]]);
		($ptrType(Mutex)).methods = [["Lock", "Lock", "", [], [], false, -1], ["Unlock", "Unlock", "", [], [], false, -1]];
		Mutex.init([["state", "state", "sync", $Int32, ""], ["sema", "sema", "sync", $Uint32, ""]]);
		($ptrType(poolLocal)).methods = [["Lock", "Lock", "", [], [], false, 2], ["Unlock", "Unlock", "", [], [], false, 2]];
		poolLocal.init([["private$0", "private", "sync", $emptyInterface, ""], ["shared", "shared", "sync", ($sliceType($emptyInterface)), ""], ["Mutex", "", "", Mutex, ""], ["pad", "pad", "sync", ($arrayType($Uint8, 128)), ""]]);
		syncSema.init($Uintptr, 3);
		allPools = ($sliceType(($ptrType(Pool)))).nil;
		init();
		init$1();
	};
	return $pkg;
})();
$packages["math/rand"] = (function() {
	var $pkg = {}, math = $packages["math"], sync = $packages["sync"], Source, Rand, lockedSource, rngSource, ke, we, fe, kn, wn, fn, globalRand, rng_cooked, absInt32, NewSource, New, Float32, seedrand;
	Source = $pkg.Source = $newType(8, "Interface", "rand.Source", "Source", "math/rand", null);
	Rand = $pkg.Rand = $newType(0, "Struct", "rand.Rand", "Rand", "math/rand", function(src_) {
		this.$val = this;
		this.src = src_ !== undefined ? src_ : null;
	});
	lockedSource = $pkg.lockedSource = $newType(0, "Struct", "rand.lockedSource", "lockedSource", "math/rand", function(lk_, src_) {
		this.$val = this;
		this.lk = lk_ !== undefined ? lk_ : new sync.Mutex.Ptr();
		this.src = src_ !== undefined ? src_ : null;
	});
	rngSource = $pkg.rngSource = $newType(0, "Struct", "rand.rngSource", "rngSource", "math/rand", function(tap_, feed_, vec_) {
		this.$val = this;
		this.tap = tap_ !== undefined ? tap_ : 0;
		this.feed = feed_ !== undefined ? feed_ : 0;
		this.vec = vec_ !== undefined ? vec_ : ($arrayType($Int64, 607)).zero();
	});
	Rand.Ptr.prototype.ExpFloat64 = function() {
		var r, j, i, x, x$1;
		r = this;
		while (true) {
			j = r.Uint32();
			i = (j & 255) >>> 0;
			x = j * $coerceFloat32(((i < 0 || i >= we.length) ? $throwRuntimeError("index out of range") : we[i]));
			if (j < ((i < 0 || i >= ke.length) ? $throwRuntimeError("index out of range") : ke[i])) {
				return x;
			}
			if (i === 0) {
				return 7.69711747013105 - math.Log(r.Float64());
			}
			if (((i < 0 || i >= fe.length) ? $throwRuntimeError("index out of range") : fe[i]) + r.Float64() * ((x$1 = i - 1 >>> 0, ((x$1 < 0 || x$1 >= fe.length) ? $throwRuntimeError("index out of range") : fe[x$1])) - ((i < 0 || i >= fe.length) ? $throwRuntimeError("index out of range") : fe[i])) < math.Exp(-x)) {
				return x;
			}
		}
	};
	Rand.prototype.ExpFloat64 = function() { return this.$val.ExpFloat64(); };
	absInt32 = function(i) {
		if (i < 0) {
			return (-i >>> 0);
		}
		return (i >>> 0);
	};
	Rand.Ptr.prototype.NormFloat64 = function() {
		var r, j, i, x, y, x$1;
		r = this;
		while (true) {
			j = (r.Uint32() >> 0);
			i = j & 127;
			x = j * $coerceFloat32(((i < 0 || i >= wn.length) ? $throwRuntimeError("index out of range") : wn[i]));
			if (absInt32(j) < ((i < 0 || i >= kn.length) ? $throwRuntimeError("index out of range") : kn[i])) {
				return x;
			}
			if (i === 0) {
				while (true) {
					x = -math.Log(r.Float64()) * 0.29047645161474317;
					y = -math.Log(r.Float64());
					if (y + y >= x * x) {
						break;
					}
				}
				if (j > 0) {
					return 3.442619855899 + x;
				}
				return -3.442619855899 - x;
			}
			if (((i < 0 || i >= fn.length) ? $throwRuntimeError("index out of range") : fn[i]) + r.Float64() * ((x$1 = i - 1 >> 0, ((x$1 < 0 || x$1 >= fn.length) ? $throwRuntimeError("index out of range") : fn[x$1])) - ((i < 0 || i >= fn.length) ? $throwRuntimeError("index out of range") : fn[i])) < math.Exp(-0.5 * x * x)) {
				return x;
			}
		}
	};
	Rand.prototype.NormFloat64 = function() { return this.$val.NormFloat64(); };
	NewSource = $pkg.NewSource = function(seed) {
		var rng;
		rng = new rngSource.Ptr(); $copy(rng, new rngSource.Ptr(), rngSource);
		rng.Seed(seed);
		return rng;
	};
	New = $pkg.New = function(src) {
		return new Rand.Ptr(src);
	};
	Rand.Ptr.prototype.Seed = function(seed) {
		var r;
		r = this;
		r.src.Seed(seed);
	};
	Rand.prototype.Seed = function(seed) { return this.$val.Seed(seed); };
	Rand.Ptr.prototype.Int63 = function() {
		var r;
		r = this;
		return r.src.Int63();
	};
	Rand.prototype.Int63 = function() { return this.$val.Int63(); };
	Rand.Ptr.prototype.Uint32 = function() {
		var r;
		r = this;
		return ($shiftRightInt64(r.Int63(), 31).$low >>> 0);
	};
	Rand.prototype.Uint32 = function() { return this.$val.Uint32(); };
	Rand.Ptr.prototype.Int31 = function() {
		var r, x;
		r = this;
		return ((x = $shiftRightInt64(r.Int63(), 32), x.$low + ((x.$high >> 31) * 4294967296)) >> 0);
	};
	Rand.prototype.Int31 = function() { return this.$val.Int31(); };
	Rand.Ptr.prototype.Int = function() {
		var r, u;
		r = this;
		u = (r.Int63().$low >>> 0);
		return (((u << 1 >>> 0) >>> 1 >>> 0) >> 0);
	};
	Rand.prototype.Int = function() { return this.$val.Int(); };
	Rand.Ptr.prototype.Int63n = function(n) {
		var r, x, x$1, x$2, x$3, x$4, x$5, max, v;
		r = this;
		if ((n.$high < 0 || (n.$high === 0 && n.$low <= 0))) {
			$panic(new $String("invalid argument to Int63n"));
		}
		if ((x = (x$1 = new $Int64(n.$high - 0, n.$low - 1), new $Int64(n.$high & x$1.$high, (n.$low & x$1.$low) >>> 0)), (x.$high === 0 && x.$low === 0))) {
			return (x$2 = r.Int63(), x$3 = new $Int64(n.$high - 0, n.$low - 1), new $Int64(x$2.$high & x$3.$high, (x$2.$low & x$3.$low) >>> 0));
		}
		max = (x$4 = (x$5 = $div64(new $Uint64(2147483648, 0), new $Uint64(n.$high, n.$low), true), new $Uint64(2147483647 - x$5.$high, 4294967295 - x$5.$low)), new $Int64(x$4.$high, x$4.$low));
		v = r.Int63();
		while ((v.$high > max.$high || (v.$high === max.$high && v.$low > max.$low))) {
			v = r.Int63();
		}
		return $div64(v, n, true);
	};
	Rand.prototype.Int63n = function(n) { return this.$val.Int63n(n); };
	Rand.Ptr.prototype.Int31n = function(n) {
		var r, _r, max, v, _r$1;
		r = this;
		if (n <= 0) {
			$panic(new $String("invalid argument to Int31n"));
		}
		if ((n & ((n - 1 >> 0))) === 0) {
			return r.Int31() & ((n - 1 >> 0));
		}
		max = ((2147483647 - (_r = 2147483648 % (n >>> 0), _r === _r ? _r : $throwRuntimeError("integer divide by zero")) >>> 0) >> 0);
		v = r.Int31();
		while (v > max) {
			v = r.Int31();
		}
		return (_r$1 = v % n, _r$1 === _r$1 ? _r$1 : $throwRuntimeError("integer divide by zero"));
	};
	Rand.prototype.Int31n = function(n) { return this.$val.Int31n(n); };
	Rand.Ptr.prototype.Intn = function(n) {
		var r, x;
		r = this;
		if (n <= 0) {
			$panic(new $String("invalid argument to Intn"));
		}
		if (n <= 2147483647) {
			return (r.Int31n((n >> 0)) >> 0);
		}
		return ((x = r.Int63n(new $Int64(0, n)), x.$low + ((x.$high >> 31) * 4294967296)) >> 0);
	};
	Rand.prototype.Intn = function(n) { return this.$val.Intn(n); };
	Rand.Ptr.prototype.Float64 = function() {
		var r, f;
		r = this;
		f = $flatten64(r.Int63()) / 9.223372036854776e+18;
		if (f === 1) {
			f = 0;
		}
		return f;
	};
	Rand.prototype.Float64 = function() { return this.$val.Float64(); };
	Rand.Ptr.prototype.Float32 = function() {
		var r, f;
		r = this;
		f = r.Float64();
		if ($float32IsEqual(f, 1)) {
			f = 0;
		}
		return f;
	};
	Rand.prototype.Float32 = function() { return this.$val.Float32(); };
	Rand.Ptr.prototype.Perm = function(n) {
		var r, m, i, j;
		r = this;
		m = ($sliceType($Int)).make(n);
		i = 0;
		while (i < n) {
			j = r.Intn(i + 1 >> 0);
			(i < 0 || i >= m.$length) ? $throwRuntimeError("index out of range") : m.$array[m.$offset + i] = ((j < 0 || j >= m.$length) ? $throwRuntimeError("index out of range") : m.$array[m.$offset + j]);
			(j < 0 || j >= m.$length) ? $throwRuntimeError("index out of range") : m.$array[m.$offset + j] = i;
			i = i + (1) >> 0;
		}
		return m;
	};
	Rand.prototype.Perm = function(n) { return this.$val.Perm(n); };
	Float32 = $pkg.Float32 = function() {
		return globalRand.Float32();
	};
	lockedSource.Ptr.prototype.Int63 = function() {
		var n = new $Int64(0, 0), r;
		r = this;
		r.lk.Lock();
		n = r.src.Int63();
		r.lk.Unlock();
		return n;
	};
	lockedSource.prototype.Int63 = function() { return this.$val.Int63(); };
	lockedSource.Ptr.prototype.Seed = function(seed) {
		var r;
		r = this;
		r.lk.Lock();
		r.src.Seed(seed);
		r.lk.Unlock();
	};
	lockedSource.prototype.Seed = function(seed) { return this.$val.Seed(seed); };
	seedrand = function(x) {
		var _q, hi, _r, lo;
		hi = (_q = x / 44488, (_q === _q && _q !== 1/0 && _q !== -1/0) ? _q >> 0 : $throwRuntimeError("integer divide by zero"));
		lo = (_r = x % 44488, _r === _r ? _r : $throwRuntimeError("integer divide by zero"));
		x = ((((48271 >>> 16 << 16) * lo >> 0) + (48271 << 16 >>> 16) * lo) >> 0) - ((((3399 >>> 16 << 16) * hi >> 0) + (3399 << 16 >>> 16) * hi) >> 0) >> 0;
		if (x < 0) {
			x = x + (2147483647) >> 0;
		}
		return x;
	};
	rngSource.Ptr.prototype.Seed = function(seed) {
		var rng, x, x$1, i, u, x$2, x$3, x$4, x$5;
		rng = this;
		rng.tap = 0;
		rng.feed = 334;
		seed = $div64(seed, new $Int64(0, 2147483647), true);
		if ((seed.$high < 0 || (seed.$high === 0 && seed.$low < 0))) {
			seed = (x = new $Int64(0, 2147483647), new $Int64(seed.$high + x.$high, seed.$low + x.$low));
		}
		if ((seed.$high === 0 && seed.$low === 0)) {
			seed = new $Int64(0, 89482311);
		}
		x$1 = ((seed.$low + ((seed.$high >> 31) * 4294967296)) >> 0);
		i = -20;
		while (i < 607) {
			x$1 = seedrand(x$1);
			if (i >= 0) {
				u = new $Int64(0, 0);
				u = $shiftLeft64(new $Int64(0, x$1), 40);
				x$1 = seedrand(x$1);
				u = (x$2 = $shiftLeft64(new $Int64(0, x$1), 20), new $Int64(u.$high ^ x$2.$high, (u.$low ^ x$2.$low) >>> 0));
				x$1 = seedrand(x$1);
				u = (x$3 = new $Int64(0, x$1), new $Int64(u.$high ^ x$3.$high, (u.$low ^ x$3.$low) >>> 0));
				u = (x$4 = ((i < 0 || i >= rng_cooked.length) ? $throwRuntimeError("index out of range") : rng_cooked[i]), new $Int64(u.$high ^ x$4.$high, (u.$low ^ x$4.$low) >>> 0));
				(x$5 = rng.vec, (i < 0 || i >= x$5.length) ? $throwRuntimeError("index out of range") : x$5[i] = new $Int64(u.$high & 2147483647, (u.$low & 4294967295) >>> 0));
			}
			i = i + (1) >> 0;
		}
	};
	rngSource.prototype.Seed = function(seed) { return this.$val.Seed(seed); };
	rngSource.Ptr.prototype.Int63 = function() {
		var rng, x, x$1, x$2, x$3, x$4, x$5, x$6, x$7, x$8, x$9;
		rng = this;
		rng.tap = rng.tap - (1) >> 0;
		if (rng.tap < 0) {
			rng.tap = rng.tap + (607) >> 0;
		}
		rng.feed = rng.feed - (1) >> 0;
		if (rng.feed < 0) {
			rng.feed = rng.feed + (607) >> 0;
		}
		x$7 = (x = (x$1 = (x$2 = rng.vec, x$3 = rng.feed, ((x$3 < 0 || x$3 >= x$2.length) ? $throwRuntimeError("index out of range") : x$2[x$3])), x$4 = (x$5 = rng.vec, x$6 = rng.tap, ((x$6 < 0 || x$6 >= x$5.length) ? $throwRuntimeError("index out of range") : x$5[x$6])), new $Int64(x$1.$high + x$4.$high, x$1.$low + x$4.$low)), new $Int64(x.$high & 2147483647, (x.$low & 4294967295) >>> 0));
		(x$8 = rng.vec, x$9 = rng.feed, (x$9 < 0 || x$9 >= x$8.length) ? $throwRuntimeError("index out of range") : x$8[x$9] = x$7);
		return x$7;
	};
	rngSource.prototype.Int63 = function() { return this.$val.Int63(); };
	$pkg.$init = function() {
		Source.init([["Int63", "Int63", "", [], [$Int64], false], ["Seed", "Seed", "", [$Int64], [], false]]);
		($ptrType(Rand)).methods = [["ExpFloat64", "ExpFloat64", "", [], [$Float64], false, -1], ["Float32", "Float32", "", [], [$Float32], false, -1], ["Float64", "Float64", "", [], [$Float64], false, -1], ["Int", "Int", "", [], [$Int], false, -1], ["Int31", "Int31", "", [], [$Int32], false, -1], ["Int31n", "Int31n", "", [$Int32], [$Int32], false, -1], ["Int63", "Int63", "", [], [$Int64], false, -1], ["Int63n", "Int63n", "", [$Int64], [$Int64], false, -1], ["Intn", "Intn", "", [$Int], [$Int], false, -1], ["NormFloat64", "NormFloat64", "", [], [$Float64], false, -1], ["Perm", "Perm", "", [$Int], [($sliceType($Int))], false, -1], ["Seed", "Seed", "", [$Int64], [], false, -1], ["Uint32", "Uint32", "", [], [$Uint32], false, -1]];
		Rand.init([["src", "src", "math/rand", Source, ""]]);
		($ptrType(lockedSource)).methods = [["Int63", "Int63", "", [], [$Int64], false, -1], ["Seed", "Seed", "", [$Int64], [], false, -1]];
		lockedSource.init([["lk", "lk", "math/rand", sync.Mutex, ""], ["src", "src", "math/rand", Source, ""]]);
		($ptrType(rngSource)).methods = [["Int63", "Int63", "", [], [$Int64], false, -1], ["Seed", "Seed", "", [$Int64], [], false, -1]];
		rngSource.init([["tap", "tap", "math/rand", $Int, ""], ["feed", "feed", "math/rand", $Int, ""], ["vec", "vec", "math/rand", ($arrayType($Int64, 607)), ""]]);
		ke = $toNativeArray("Uint32", [3801129273, 0, 2615860924, 3279400049, 3571300752, 3733536696, 3836274812, 3906990442, 3958562475, 3997804264, 4028649213, 4053523342, 4074002619, 4091154507, 4105727352, 4118261130, 4129155133, 4138710916, 4147160435, 4154685009, 4161428406, 4167506077, 4173011791, 4178022498, 4182601930, 4186803325, 4190671498, 4194244443, 4197554582, 4200629752, 4203493986, 4206168142, 4208670408, 4211016720, 4213221098, 4215295924, 4217252177, 4219099625, 4220846988, 4222502074, 4224071896, 4225562770, 4226980400, 4228329951, 4229616109, 4230843138, 4232014925, 4233135020, 4234206673, 4235232866, 4236216336, 4237159604, 4238064994, 4238934652, 4239770563, 4240574564, 4241348362, 4242093539, 4242811568, 4243503822, 4244171579, 4244816032, 4245438297, 4246039419, 4246620374, 4247182079, 4247725394, 4248251127, 4248760037, 4249252839, 4249730206, 4250192773, 4250641138, 4251075867, 4251497493, 4251906522, 4252303431, 4252688672, 4253062674, 4253425844, 4253778565, 4254121205, 4254454110, 4254777611, 4255092022, 4255397640, 4255694750, 4255983622, 4256264513, 4256537670, 4256803325, 4257061702, 4257313014, 4257557464, 4257795244, 4258026541, 4258251531, 4258470383, 4258683258, 4258890309, 4259091685, 4259287526, 4259477966, 4259663135, 4259843154, 4260018142, 4260188212, 4260353470, 4260514019, 4260669958, 4260821380, 4260968374, 4261111028, 4261249421, 4261383632, 4261513736, 4261639802, 4261761900, 4261880092, 4261994441, 4262105003, 4262211835, 4262314988, 4262414513, 4262510454, 4262602857, 4262691764, 4262777212, 4262859239, 4262937878, 4263013162, 4263085118, 4263153776, 4263219158, 4263281289, 4263340187, 4263395872, 4263448358, 4263497660, 4263543789, 4263586755, 4263626565, 4263663224, 4263696735, 4263727099, 4263754314, 4263778377, 4263799282, 4263817020, 4263831582, 4263842955, 4263851124, 4263856071, 4263857776, 4263856218, 4263851370, 4263843206, 4263831695, 4263816804, 4263798497, 4263776735, 4263751476, 4263722676, 4263690284, 4263654251, 4263614520, 4263571032, 4263523724, 4263472530, 4263417377, 4263358192, 4263294892, 4263227394, 4263155608, 4263079437, 4262998781, 4262913534, 4262823581, 4262728804, 4262629075, 4262524261, 4262414220, 4262298801, 4262177846, 4262051187, 4261918645, 4261780032, 4261635148, 4261483780, 4261325704, 4261160681, 4260988457, 4260808763, 4260621313, 4260425802, 4260221905, 4260009277, 4259787550, 4259556329, 4259315195, 4259063697, 4258801357, 4258527656, 4258242044, 4257943926, 4257632664, 4257307571, 4256967906, 4256612870, 4256241598, 4255853155, 4255446525, 4255020608, 4254574202, 4254106002, 4253614578, 4253098370, 4252555662, 4251984571, 4251383021, 4250748722, 4250079132, 4249371435, 4248622490, 4247828790, 4246986404, 4246090910, 4245137315, 4244119963, 4243032411, 4241867296, 4240616155, 4239269214, 4237815118, 4236240596, 4234530035, 4232664930, 4230623176, 4228378137, 4225897409, 4223141146, 4220059768, 4216590757, 4212654085, 4208145538, 4202926710, 4196809522, 4189531420, 4180713890, 4169789475, 4155865042, 4137444620, 4111806704, 4073393724, 4008685917, 3873074895]);
		we = $toNativeArray("Float32", [2.0249555365836613e-09, 1.4866739783681027e-11, 2.4409616689036184e-11, 3.1968806074589295e-11, 3.844677007314168e-11, 4.42282044321729e-11, 4.951644302919611e-11, 5.443358958023836e-11, 5.905943789574764e-11, 6.34494193296753e-11, 6.764381416113352e-11, 7.167294535648239e-11, 7.556032188826833e-11, 7.932458162551725e-11, 8.298078890689453e-11, 8.654132271912474e-11, 9.001651507523079e-11, 9.341507428706208e-11, 9.674443190998971e-11, 1.0001099254308699e-10, 1.0322031424037093e-10, 1.0637725422757427e-10, 1.0948611461891744e-10, 1.1255067711157807e-10, 1.1557434870246297e-10, 1.1856014781042035e-10, 1.2151082917633005e-10, 1.2442885610752796e-10, 1.2731647680563896e-10, 1.3017574518325858e-10, 1.330085347417409e-10, 1.3581656632677408e-10, 1.386014220061682e-10, 1.413645728254309e-10, 1.4410737880776736e-10, 1.4683107507629245e-10, 1.4953686899854546e-10, 1.522258291641876e-10, 1.5489899640730442e-10, 1.575573282952547e-10, 1.6020171300645814e-10, 1.628330109637588e-10, 1.6545202707884954e-10, 1.68059510752272e-10, 1.7065616975120435e-10, 1.73242697965037e-10, 1.758197337720091e-10, 1.783878739169964e-10, 1.8094774290045024e-10, 1.834998542005195e-10, 1.8604476292871652e-10, 1.8858298256319017e-10, 1.9111498494872592e-10, 1.9364125580789704e-10, 1.9616222535212557e-10, 1.9867835154840918e-10, 2.011900368525943e-10, 2.0369768372052732e-10, 2.062016807302669e-10, 2.0870240258208383e-10, 2.1120022397624894e-10, 2.136955057352452e-10, 2.1618855317040442e-10, 2.1867974098199738e-10, 2.2116936060356807e-10, 2.2365774510202385e-10, 2.2614519978869652e-10, 2.2863201609713002e-10, 2.3111849933865614e-10, 2.3360494094681883e-10, 2.3609159072179864e-10, 2.3857874009713953e-10, 2.4106666662859766e-10, 2.4355562011635357e-10, 2.460458781161634e-10, 2.485376904282077e-10, 2.5103127909709144e-10, 2.5352694943414633e-10, 2.560248957284017e-10, 2.585253955356137e-10, 2.610286709003873e-10, 2.6353494386732734e-10, 2.6604446423661443e-10, 2.6855745405285347e-10, 2.71074163116225e-10, 2.7359478571575835e-10, 2.7611959940720965e-10, 2.786487707240326e-10, 2.8118254946640775e-10, 2.8372118543451563e-10, 2.8626484516180994e-10, 2.8881380620404684e-10, 2.9136826285025563e-10, 2.9392840938946563e-10, 2.96494523377433e-10, 2.990667713476114e-10, 3.016454031001814e-10, 3.042306406797479e-10, 3.068226783753403e-10, 3.09421765987139e-10, 3.12028125559749e-10, 3.1464195138219964e-10, 3.17263521010247e-10, 3.1989300097734485e-10, 3.225306410836737e-10, 3.2517669112941405e-10, 3.2783134540359526e-10, 3.3049485370639786e-10, 3.3316743808242677e-10, 3.3584937608743815e-10, 3.385408342548857e-10, 3.4124211789610115e-10, 3.4395342130011386e-10, 3.4667499426710435e-10, 3.494071143528288e-10, 3.521500313574677e-10, 3.54903967325626e-10, 3.576691720574843e-10, 3.6044595086437425e-10, 3.632345535464765e-10, 3.660352021483959e-10, 3.688482297370399e-10, 3.716738583570134e-10, 3.7451239331964814e-10, 3.773641121807003e-10, 3.802292924959261e-10, 3.831082673322328e-10, 3.8600128648980103e-10, 3.8890865527996255e-10, 3.9183070676962473e-10, 3.9476774627011935e-10, 3.977200790927782e-10, 4.006880383045086e-10, 4.0367195697221803e-10, 4.066721681628138e-10, 4.0968900494320337e-10, 4.127228558914453e-10, 4.15774054074447e-10, 4.188429603146915e-10, 4.2192993543466173e-10, 4.25035395767992e-10, 4.2815970213716525e-10, 4.313032986313914e-10, 4.3446651831757777e-10, 4.376498607960855e-10, 4.408536868893975e-10, 4.4407846844229937e-10, 4.4732464954400086e-10, 4.5059267428371186e-10, 4.538830145062178e-10, 4.5719619756745544e-10, 4.605326675566346e-10, 4.638929240741163e-10, 4.672775499869886e-10, 4.706869893844612e-10, 4.74121908400349e-10, 4.775827511238617e-10, 4.810701836888143e-10, 4.845848167178701e-10, 4.881271498113904e-10, 4.916979601254923e-10, 4.952977472605369e-10, 4.989272883726414e-10, 5.025872495956207e-10, 5.062783525744408e-10, 5.100013189540675e-10, 5.13756870379467e-10, 5.175458395179078e-10, 5.21369003525507e-10, 5.252272505806843e-10, 5.29121357839557e-10, 5.330522134805449e-10, 5.3702081670437e-10, 5.41028055689452e-10, 5.450749851476644e-10, 5.491624932574268e-10, 5.532918012640664e-10, 5.574638528571541e-10, 5.616799247931681e-10, 5.659410717839819e-10, 5.702485705860738e-10, 5.746036979559221e-10, 5.790077306500052e-10, 5.83462111958255e-10, 5.879682296594524e-10, 5.925275825546805e-10, 5.971417249561739e-10, 6.01812211176167e-10, 6.065408175714992e-10, 6.113292094767075e-10, 6.16179329782085e-10, 6.21092954844471e-10, 6.260721940876124e-10, 6.311191569352559e-10, 6.362359528111483e-10, 6.414249686947926e-10, 6.466885360545405e-10, 6.520292639144998e-10, 6.574497612987784e-10, 6.629528592760892e-10, 6.685415554485985e-10, 6.742187919073217e-10, 6.799880103436351e-10, 6.858525969377638e-10, 6.918161599145378e-10, 6.978825850545434e-10, 7.040559801829716e-10, 7.103406751696184e-10, 7.167412219288849e-10, 7.232625609532306e-10, 7.2990985477972e-10, 7.366885990123251e-10, 7.436047333442275e-10, 7.506645305355164e-10, 7.57874762946642e-10, 7.652426470272644e-10, 7.727759543385559e-10, 7.804830115532013e-10, 7.883728114777e-10, 7.964550685635174e-10, 8.047402189070851e-10, 8.132396422944055e-10, 8.219657177122031e-10, 8.309318788590758e-10, 8.401527806789488e-10, 8.496445214056791e-10, 8.594246980742071e-10, 8.695127395874636e-10, 8.799300732498239e-10, 8.90700457834015e-10, 9.01850316648023e-10, 9.134091816243028e-10, 9.254100818978372e-10, 9.37890431984556e-10, 9.508922538259412e-10, 9.64463842123564e-10, 9.78660263939446e-10, 9.935448019859905e-10, 1.0091912860943353e-09, 1.0256859805934937e-09, 1.0431305819125214e-09, 1.0616465484503124e-09, 1.0813799855569073e-09, 1.1025096391392708e-09, 1.1252564435793033e-09, 1.149898620766976e-09, 1.176793218427008e-09, 1.2064089727203964e-09, 1.2393785997488749e-09, 1.2765849488616254e-09, 1.319313880365769e-09, 1.36954347862428e-09, 1.4305497897382224e-09, 1.5083649884672923e-09, 1.6160853766322703e-09, 1.7921247819074893e-09]);
		fe = $toNativeArray("Float32", [1, 0.9381436705589294, 0.900469958782196, 0.8717043399810791, 0.847785472869873, 0.8269932866096497, 0.8084216713905334, 0.7915276288986206, 0.7759568691253662, 0.7614634037017822, 0.7478685975074768, 0.7350381016731262, 0.7228676676750183, 0.7112747430801392, 0.7001926302909851, 0.6895664930343628, 0.6793505549430847, 0.669506311416626, 0.6600008606910706, 0.6508058309555054, 0.6418967247009277, 0.633251965045929, 0.62485271692276, 0.6166821718215942, 0.608725368976593, 0.6009689569473267, 0.5934008955955505, 0.5860103368759155, 0.5787873864173889, 0.5717230439186096, 0.5648092031478882, 0.5580382943153381, 0.5514034032821655, 0.5448982119560242, 0.5385168790817261, 0.5322538614273071, 0.526104211807251, 0.5200631618499756, 0.5141264200210571, 0.5082897543907166, 0.5025495290756226, 0.4969019889831543, 0.4913438558578491, 0.4858720004558563, 0.48048335313796997, 0.4751752018928528, 0.4699448347091675, 0.4647897481918335, 0.4597076177597046, 0.4546961486339569, 0.4497532546520233, 0.44487687945365906, 0.4400651156902313, 0.4353161156177521, 0.4306281507015228, 0.42599955201148987, 0.42142874002456665, 0.4169141948223114, 0.4124544560909271, 0.40804818272590637, 0.4036940038204193, 0.39939069747924805, 0.3951369822025299, 0.39093172550201416, 0.38677382469177246, 0.38266217708587646, 0.378595769405365, 0.37457355856895447, 0.37059465050697327, 0.366658091545105, 0.362762987613678, 0.358908474445343, 0.35509374737739563, 0.35131800174713135, 0.3475804924964905, 0.34388044476509094, 0.34021714329719543, 0.33658990263938904, 0.3329980671405792, 0.3294409513473511, 0.32591795921325684, 0.32242849469184875, 0.3189719021320343, 0.3155476748943329, 0.31215524673461914, 0.3087940812110901, 0.30546361207962036, 0.30216339230537415, 0.29889291524887085, 0.29565170407295227, 0.2924392819404602, 0.2892552316188812, 0.28609907627105713, 0.2829704284667969, 0.27986884117126465, 0.2767939269542694, 0.2737452983856201, 0.2707225978374481, 0.26772540807724, 0.26475343108177185, 0.2618062496185303, 0.258883535861969, 0.2559850215911865, 0.25311028957366943, 0.25025907158851624, 0.24743106961250305, 0.2446259707212448, 0.24184346199035645, 0.23908329010009766, 0.23634515702724457, 0.2336287796497345, 0.23093391954898834, 0.22826029360294342, 0.22560766339302063, 0.22297576069831848, 0.22036437690258026, 0.21777324378490448, 0.21520215272903442, 0.212650865316391, 0.21011915802955627, 0.20760682225227356, 0.20511364936828613, 0.20263944566249847, 0.20018397271633148, 0.19774706661701202, 0.1953285187482834, 0.19292815029621124, 0.19054576754570007, 0.18818120658397675, 0.18583425879478455, 0.18350479006767273, 0.18119260668754578, 0.17889754474163055, 0.17661945521831512, 0.17435817420482635, 0.1721135377883911, 0.16988539695739746, 0.16767361760139465, 0.16547803580760956, 0.16329853236675262, 0.16113494336605072, 0.1589871346950531, 0.15685498714447021, 0.15473836660385132, 0.15263713896274567, 0.1505511850118637, 0.1484803706407547, 0.14642459154129028, 0.1443837285041809, 0.14235764741897583, 0.1403462439775467, 0.13834942877292633, 0.136367067694664, 0.13439907133579254, 0.1324453204870224, 0.1305057406425476, 0.12858019769191742, 0.12666863203048706, 0.12477091699838638, 0.12288697808980942, 0.1210167184472084, 0.11916005611419678, 0.11731690168380737, 0.11548716574907303, 0.11367076635360718, 0.11186762899160385, 0.11007767915725708, 0.1083008274435997, 0.10653700679540634, 0.10478614270687103, 0.1030481606721878, 0.10132300108671188, 0.0996105819940567, 0.09791085124015808, 0.09622374176979065, 0.09454918652772903, 0.09288713335990906, 0.09123751521110535, 0.08960027992725372, 0.08797537535429001, 0.08636274188756943, 0.0847623273730278, 0.08317409455776215, 0.08159798383712769, 0.08003395050764084, 0.07848194986581802, 0.07694194465875626, 0.07541389018297195, 0.07389774918556213, 0.07239348441362381, 0.070901058614254, 0.06942043453454971, 0.06795158982276917, 0.06649449467658997, 0.06504911929368973, 0.06361543387174606, 0.06219341605901718, 0.06078304722905159, 0.0593843050301075, 0.05799717456102371, 0.05662164092063904, 0.05525768920779228, 0.05390531197190285, 0.05256449431180954, 0.05123523622751236, 0.04991753399372101, 0.04861138388514519, 0.047316793352365494, 0.04603376239538193, 0.044762298464775085, 0.04350241273641586, 0.04225412383675575, 0.04101744294166565, 0.039792392402887344, 0.03857899457216263, 0.03737728297710419, 0.03618728369474411, 0.03500903770327568, 0.03384258225560188, 0.0326879620552063, 0.031545232981443405, 0.030414443463087082, 0.0292956605553627, 0.028188949450850487, 0.027094384655356407, 0.02601204626262188, 0.024942025542259216, 0.023884421214461327, 0.022839335724711418, 0.021806888282299042, 0.020787203684449196, 0.019780423492193222, 0.018786700442433357, 0.017806200310587883, 0.016839107498526573, 0.015885621309280396, 0.014945968054234982, 0.01402039173990488, 0.013109165243804455, 0.012212592177093029, 0.011331013403832912, 0.010464809834957123, 0.009614413604140282, 0.008780314587056637, 0.007963077165186405, 0.007163353264331818, 0.0063819061033427715, 0.005619642324745655, 0.004877655766904354, 0.004157294984906912, 0.003460264764726162, 0.0027887988835573196, 0.0021459676790982485, 0.001536299823783338, 0.0009672692976891994, 0.0004541343660093844]);
		kn = $toNativeArray("Uint32", [1991057938, 0, 1611602771, 1826899878, 1918584482, 1969227037, 2001281515, 2023368125, 2039498179, 2051788381, 2061460127, 2069267110, 2075699398, 2081089314, 2085670119, 2089610331, 2093034710, 2096037586, 2098691595, 2101053571, 2103168620, 2105072996, 2106796166, 2108362327, 2109791536, 2111100552, 2112303493, 2113412330, 2114437283, 2115387130, 2116269447, 2117090813, 2117856962, 2118572919, 2119243101, 2119871411, 2120461303, 2121015852, 2121537798, 2122029592, 2122493434, 2122931299, 2123344971, 2123736059, 2124106020, 2124456175, 2124787725, 2125101763, 2125399283, 2125681194, 2125948325, 2126201433, 2126441213, 2126668298, 2126883268, 2127086657, 2127278949, 2127460589, 2127631985, 2127793506, 2127945490, 2128088244, 2128222044, 2128347141, 2128463758, 2128572095, 2128672327, 2128764606, 2128849065, 2128925811, 2128994934, 2129056501, 2129110560, 2129157136, 2129196237, 2129227847, 2129251929, 2129268426, 2129277255, 2129278312, 2129271467, 2129256561, 2129233410, 2129201800, 2129161480, 2129112170, 2129053545, 2128985244, 2128906855, 2128817916, 2128717911, 2128606255, 2128482298, 2128345305, 2128194452, 2128028813, 2127847342, 2127648860, 2127432031, 2127195339, 2126937058, 2126655214, 2126347546, 2126011445, 2125643893, 2125241376, 2124799783, 2124314271, 2123779094, 2123187386, 2122530867, 2121799464, 2120980787, 2120059418, 2119015917, 2117825402, 2116455471, 2114863093, 2112989789, 2110753906, 2108037662, 2104664315, 2100355223, 2094642347, 2086670106, 2074676188, 2054300022, 2010539237]);
		wn = $toNativeArray("Float32", [1.7290404663583558e-09, 1.2680928529462676e-10, 1.689751810696194e-10, 1.9862687883343e-10, 2.223243117382978e-10, 2.4244936613904144e-10, 2.601613091623989e-10, 2.761198769629658e-10, 2.9073962681813725e-10, 3.042996965518796e-10, 3.169979556627567e-10, 3.289802041894774e-10, 3.4035738116777736e-10, 3.5121602848242617e-10, 3.61625090983253e-10, 3.7164057942185025e-10, 3.813085680537398e-10, 3.906675816178762e-10, 3.997501218933053e-10, 4.0858399996679395e-10, 4.1719308563337165e-10, 4.255982233303257e-10, 4.3381759295968436e-10, 4.4186720948857783e-10, 4.497613115272969e-10, 4.57512583373898e-10, 4.6513240481438345e-10, 4.726310454117311e-10, 4.800177477726209e-10, 4.873009773476156e-10, 4.944885056978876e-10, 5.015873272284921e-10, 5.086040477664255e-10, 5.155446070048697e-10, 5.224146670812502e-10, 5.292193350214802e-10, 5.359634958068682e-10, 5.426517013518151e-10, 5.492881705038144e-10, 5.558769555769061e-10, 5.624218868405251e-10, 5.689264614971989e-10, 5.75394121238304e-10, 5.818281967329142e-10, 5.882316855831959e-10, 5.946076964136182e-10, 6.009590047817426e-10, 6.072883862451306e-10, 6.135985053390414e-10, 6.19892026598734e-10, 6.261713370037114e-10, 6.324390455780815e-10, 6.386973727678935e-10, 6.449488165749528e-10, 6.511955974453087e-10, 6.574400468473129e-10, 6.636843297158634e-10, 6.699307220081607e-10, 6.761814441702541e-10, 6.824387166481927e-10, 6.887046488657234e-10, 6.949815167800466e-10, 7.012714853260604e-10, 7.075767749498141e-10, 7.13899661608508e-10, 7.202424212593428e-10, 7.266072743483676e-10, 7.329966078550854e-10, 7.394128087589991e-10, 7.458582640396116e-10, 7.523354716987285e-10, 7.588469852493063e-10, 7.653954137154528e-10, 7.719834771435785e-10, 7.786139510912449e-10, 7.852897221383159e-10, 7.920137878869582e-10, 7.987892014504894e-10, 8.056192379868321e-10, 8.125072836762115e-10, 8.194568912323064e-10, 8.264716688799467e-10, 8.3355555791087e-10, 8.407127216614185e-10, 8.479473234679347e-10, 8.552640262671218e-10, 8.626675485068347e-10, 8.701631637464402e-10, 8.777562010564566e-10, 8.854524335966119e-10, 8.932581896381464e-10, 9.011799639857543e-10, 9.092249730890956e-10, 9.174008219758889e-10, 9.25715837318819e-10, 9.341788453909317e-10, 9.42799727177146e-10, 9.515889187738935e-10, 9.605578554783278e-10, 9.697193048552322e-10, 9.790869226478094e-10, 9.886760299337993e-10, 9.985036131254788e-10, 1.008588212947359e-09, 1.0189509236369076e-09, 1.0296150598776421e-09, 1.040606933955246e-09, 1.0519566329136865e-09, 1.0636980185552147e-09, 1.0758701707302976e-09, 1.0885182755160372e-09, 1.101694735439196e-09, 1.115461056855338e-09, 1.1298901814171813e-09, 1.1450695946990663e-09, 1.1611052119775422e-09, 1.178127595480305e-09, 1.1962995039027646e-09, 1.2158286599728285e-09, 1.2369856250415978e-09, 1.2601323318151003e-09, 1.2857697129220469e-09, 1.3146201904845611e-09, 1.3477839955200466e-09, 1.3870635751089821e-09, 1.43574030442295e-09, 1.5008658760251592e-09, 1.6030947680434338e-09]);
		fn = $toNativeArray("Float32", [1, 0.963599681854248, 0.9362826943397522, 0.9130436182022095, 0.8922816514968872, 0.8732430338859558, 0.8555005788803101, 0.8387836217880249, 0.8229072093963623, 0.8077383041381836, 0.7931770086288452, 0.7791460752487183, 0.7655841708183289, 0.7524415850639343, 0.7396772503852844, 0.7272568941116333, 0.7151514887809753, 0.7033361196517944, 0.6917891502380371, 0.6804918646812439, 0.6694276928901672, 0.6585819721221924, 0.6479418277740479, 0.6374954581260681, 0.6272324919700623, 0.6171433925628662, 0.6072195172309875, 0.5974531769752502, 0.5878370404243469, 0.5783646702766418, 0.5690299868583679, 0.5598273873329163, 0.550751805305481, 0.5417983531951904, 0.5329626798629761, 0.5242405533790588, 0.5156282186508179, 0.5071220397949219, 0.49871864914894104, 0.4904148280620575, 0.48220765590667725, 0.47409430146217346, 0.466072142124176, 0.45813870429992676, 0.45029163360595703, 0.44252872467041016, 0.4348478317260742, 0.42724698781967163, 0.41972434520721436, 0.41227802634239197, 0.40490642189979553, 0.39760786294937134, 0.3903807997703552, 0.3832238018512726, 0.3761354684829712, 0.3691144585609436, 0.36215949058532715, 0.3552693724632263, 0.3484429717063904, 0.3416791558265686, 0.33497685194015503, 0.32833510637283325, 0.3217529058456421, 0.3152293860912323, 0.30876362323760986, 0.3023548424243927, 0.2960021495819092, 0.2897048592567444, 0.28346219658851624, 0.2772735059261322, 0.271138072013855, 0.2650552988052368, 0.25902456045150757, 0.25304529070854187, 0.24711695313453674, 0.24123899638652802, 0.23541094362735748, 0.22963231801986694, 0.22390270233154297, 0.21822164952754974, 0.21258877217769623, 0.20700371265411377, 0.20146611332893372, 0.1959756463766098, 0.19053204357624054, 0.18513499200344086, 0.17978426814079285, 0.1744796335697174, 0.16922089457511902, 0.16400785744190216, 0.1588403731584549, 0.15371830761432648, 0.14864157140254974, 0.14361007511615753, 0.13862377405166626, 0.13368265330791473, 0.12878671288490295, 0.12393598258495331, 0.11913054436445236, 0.11437050998210907, 0.10965602099895477, 0.1049872562289238, 0.10036443918943405, 0.09578784555196762, 0.09125780314207077, 0.08677466958761215, 0.08233889937400818, 0.07795098423957825, 0.07361150532960892, 0.06932111829519272, 0.06508058309555054, 0.06089077144861221, 0.05675266310572624, 0.05266740173101425, 0.048636294901371, 0.044660862535238266, 0.040742866694927216, 0.03688438981771469, 0.03308788686990738, 0.029356317594647408, 0.025693291798233986, 0.02210330404341221, 0.018592102453112602, 0.015167297795414925, 0.011839478276669979, 0.0086244847625494, 0.005548994988203049, 0.0026696291752159595]);
		rng_cooked = $toNativeArray("Int64", [new $Int64(1173834291, 3952672746), new $Int64(1081821761, 3130416987), new $Int64(324977939, 3414273807), new $Int64(1241840476, 2806224363), new $Int64(669549340, 1997590414), new $Int64(2103305448, 2402795971), new $Int64(1663160183, 1140819369), new $Int64(1120601685, 1788868961), new $Int64(1848035537, 1089001426), new $Int64(1235702047, 873593504), new $Int64(1911387977, 581324885), new $Int64(492609478, 1609182556), new $Int64(1069394745, 1241596776), new $Int64(1895445337, 1771189259), new $Int64(772864846, 3467012610), new $Int64(2006957225, 2344407434), new $Int64(402115761, 782467244), new $Int64(26335124, 3404933915), new $Int64(1063924276, 618867887), new $Int64(1178782866, 520164395), new $Int64(555910815, 1341358184), new $Int64(632398609, 665794848), new $Int64(1527227641, 3183648150), new $Int64(1781176124, 696329606), new $Int64(1789146075, 4151988961), new $Int64(60039534, 998951326), new $Int64(1535158725, 1364957564), new $Int64(63173359, 4090230633), new $Int64(649454641, 4009697548), new $Int64(248009524, 2569622517), new $Int64(778703922, 3742421481), new $Int64(1038377625, 1506914633), new $Int64(1738099768, 1983412561), new $Int64(236311649, 1436266083), new $Int64(1035966148, 3922894967), new $Int64(810508934, 1792680179), new $Int64(563141142, 1188796351), new $Int64(1349617468, 405968250), new $Int64(1044074554, 433754187), new $Int64(870549669, 4073162024), new $Int64(1053232044, 433121399), new $Int64(2451824, 4162580594), new $Int64(2010221076, 4132415622), new $Int64(611252600, 3033822028), new $Int64(2016407895, 824682382), new $Int64(2366218, 3583765414), new $Int64(1522878809, 535386927), new $Int64(1637219058, 2286693689), new $Int64(1453075389, 2968466525), new $Int64(193683513, 1351410206), new $Int64(1863677552, 1412813499), new $Int64(492736522, 4126267639), new $Int64(512765208, 2105529399), new $Int64(2132966268, 2413882233), new $Int64(947457634, 32226200), new $Int64(1149341356, 2032329073), new $Int64(106485445, 1356518208), new $Int64(79673492, 3430061722), new $Int64(663048513, 3820169661), new $Int64(481498454, 2981816134), new $Int64(1017155588, 4184371017), new $Int64(206574701, 2119206761), new $Int64(1295374591, 2472200560), new $Int64(1587026100, 2853524696), new $Int64(1307803389, 1681119904), new $Int64(1972496813, 95608918), new $Int64(392686347, 3690479145), new $Int64(941912722, 1397922290), new $Int64(988169623, 1516129515), new $Int64(1827305493, 1547420459), new $Int64(1311333971, 1470949486), new $Int64(194013850, 1336785672), new $Int64(2102397034, 4131677129), new $Int64(755205548, 4246329084), new $Int64(1004983461, 3788585631), new $Int64(2081005363, 3080389532), new $Int64(1501045284, 2215402037), new $Int64(391002300, 1171593935), new $Int64(1408774047, 1423855166), new $Int64(1628305930, 2276716302), new $Int64(1779030508, 2068027241), new $Int64(1369359303, 3427553297), new $Int64(189241615, 3289637845), new $Int64(1057480830, 3486407650), new $Int64(634572984, 3071877822), new $Int64(1159653919, 3363620705), new $Int64(1213226718, 4159821533), new $Int64(2070861710, 1894661), new $Int64(1472989750, 1156868282), new $Int64(348271067, 776219088), new $Int64(1646054810, 2425634259), new $Int64(1716021749, 680510161), new $Int64(1573220192, 1310101429), new $Int64(1095885995, 2964454134), new $Int64(1821788136, 3467098407), new $Int64(1990672920, 2109628894), new $Int64(7834944, 1232604732), new $Int64(309412934, 3261916179), new $Int64(1699175360, 434597899), new $Int64(235436061, 1624796439), new $Int64(521080809, 3589632480), new $Int64(1198416575, 864579159), new $Int64(208735487, 1380889830), new $Int64(619206309, 2654509477), new $Int64(1419738251, 1468209306), new $Int64(403198876, 100794388), new $Int64(956062190, 2991674471), new $Int64(1938816907, 2224662036), new $Int64(1973824487, 977097250), new $Int64(1351320195, 726419512), new $Int64(1964023751, 1747974366), new $Int64(1394388465, 1556430604), new $Int64(1097991433, 1080776742), new $Int64(1761636690, 280794874), new $Int64(117767733, 919835643), new $Int64(1180474222, 3434019658), new $Int64(196069168, 2461941785), new $Int64(133215641, 3615001066), new $Int64(417204809, 3103414427), new $Int64(790056561, 3380809712), new $Int64(879802240, 2724693469), new $Int64(547796833, 598827710), new $Int64(300924196, 3452273442), new $Int64(2071705424, 649274915), new $Int64(1346182319, 2585724112), new $Int64(636549385, 3165579553), new $Int64(1185578221, 2635894283), new $Int64(2094573470, 2053289721), new $Int64(985976581, 3169337108), new $Int64(1170569632, 144717764), new $Int64(1079216270, 1383666384), new $Int64(2022678706, 681540375), new $Int64(1375448925, 537050586), new $Int64(182715304, 315246468), new $Int64(226402871, 849323088), new $Int64(1262421183, 45543944), new $Int64(1201038398, 2319052083), new $Int64(2106775454, 3613090841), new $Int64(560472520, 2992171180), new $Int64(1765620479, 2068244785), new $Int64(917538188, 4239862634), new $Int64(777927839, 3892253031), new $Int64(720683925, 958186149), new $Int64(1724185863, 1877702262), new $Int64(1357886971, 837674867), new $Int64(1837048883, 1507589294), new $Int64(1905518400, 873336795), new $Int64(267722611, 2764496274), new $Int64(341003118, 4196182374), new $Int64(1080717893, 550964545), new $Int64(818747069, 420611474), new $Int64(222653272, 204265180), new $Int64(1549974541, 1787046383), new $Int64(1215581865, 3102292318), new $Int64(418321538, 1552199393), new $Int64(1243493047, 980542004), new $Int64(267284263, 3293718720), new $Int64(1179528763, 3771917473), new $Int64(599484404, 2195808264), new $Int64(252818753, 3894702887), new $Int64(780007692, 2099949527), new $Int64(1424094358, 338442522), new $Int64(490737398, 637158004), new $Int64(419862118, 281976339), new $Int64(574970164, 3619802330), new $Int64(1715552825, 3084554784), new $Int64(882872465, 4129772886), new $Int64(43084605, 1680378557), new $Int64(525521057, 3339087776), new $Int64(1680500332, 4220317857), new $Int64(211654685, 2959322499), new $Int64(1675600481, 1488354890), new $Int64(1312620086, 3958162143), new $Int64(920972075, 2773705983), new $Int64(1876039582, 225908689), new $Int64(963748535, 908216283), new $Int64(1541787429, 3574646075), new $Int64(319760557, 1936937569), new $Int64(1519770881, 75492235), new $Int64(816689472, 1935193178), new $Int64(2142521206, 2018250883), new $Int64(455141620, 3943126022), new $Int64(1546084160, 3066544345), new $Int64(1932392669, 2793082663), new $Int64(908474287, 3297036421), new $Int64(1640597065, 2206987825), new $Int64(1594236910, 807894872), new $Int64(366158341, 766252117), new $Int64(2060649606, 3833114345), new $Int64(845619743, 1255067973), new $Int64(1201145605, 741697208), new $Int64(671241040, 2810093753), new $Int64(1109032642, 4229340371), new $Int64(1462188720, 1361684224), new $Int64(988084219, 1906263026), new $Int64(475781207, 3904421704), new $Int64(1523946520, 1769075545), new $Int64(1062308525, 2621599764), new $Int64(1279509432, 3431891480), new $Int64(404732502, 1871896503), new $Int64(128756421, 1412808876), new $Int64(1605404688, 952876175), new $Int64(1917039957, 1824438899), new $Int64(1662295856, 1005035476), new $Int64(1990909507, 527508597), new $Int64(1288873303, 3066806859), new $Int64(565995893, 3244940914), new $Int64(1257737460, 209092916), new $Int64(1899814242, 1242699167), new $Int64(1433653252, 456723774), new $Int64(1776978905, 1001252870), new $Int64(1468772157, 2026725874), new $Int64(857254202, 2137562569), new $Int64(765939740, 3183366709), new $Int64(1533887628, 2612072960), new $Int64(56977098, 1727148468), new $Int64(949899753, 3803658212), new $Int64(1883670356, 479946959), new $Int64(685713571, 1562982345), new $Int64(201241205, 1766109365), new $Int64(700596547, 3257093788), new $Int64(1962768719, 2365720207), new $Int64(93384808, 3742754173), new $Int64(1689098413, 2878193673), new $Int64(1096135042, 2174002182), new $Int64(1313222695, 3573511231), new $Int64(1392911121, 1760299077), new $Int64(771856457, 2260779833), new $Int64(1281464374, 1452805722), new $Int64(917811730, 2940011802), new $Int64(1890251082, 1886183802), new $Int64(893897673, 2514369088), new $Int64(1644345561, 3924317791), new $Int64(172616216, 500935732), new $Int64(1403501753, 676580929), new $Int64(581571365, 1184984890), new $Int64(1455515235, 1271474274), new $Int64(318728910, 3163791473), new $Int64(2051027584, 2842487377), new $Int64(1511537551, 2170968612), new $Int64(573262976, 3535856740), new $Int64(94256461, 1488599718), new $Int64(966951817, 3408913763), new $Int64(60951736, 2501050084), new $Int64(1272353200, 1639124157), new $Int64(138001144, 4088176393), new $Int64(1574896563, 3989947576), new $Int64(1982239940, 3414355209), new $Int64(1355154361, 2275136352), new $Int64(89709303, 2151835223), new $Int64(1216338715, 1654534827), new $Int64(1467562197, 377892833), new $Int64(1664767638, 660204544), new $Int64(85706799, 390828249), new $Int64(725310955, 3402783878), new $Int64(678849488, 3717936603), new $Int64(1113532086, 2211058823), new $Int64(1564224320, 2692150867), new $Int64(1952770442, 1928910388), new $Int64(788716862, 3931011137), new $Int64(1083670504, 1112701047), new $Int64(2079333076, 2452299106), new $Int64(1251318826, 2337204777), new $Int64(1774877857, 273889282), new $Int64(1798719843, 1462008793), new $Int64(2138834788, 1554494002), new $Int64(952516517, 182675323), new $Int64(548928884, 1882802136), new $Int64(589279648, 3700220025), new $Int64(381039426, 3083431543), new $Int64(1295624457, 3622207527), new $Int64(338126939, 432729309), new $Int64(480013522, 2391914317), new $Int64(297925497, 235747924), new $Int64(2120733629, 3088823825), new $Int64(1402403853, 2314658321), new $Int64(1165929723, 2957634338), new $Int64(501323675, 4117056981), new $Int64(1564699815, 1482500298), new $Int64(1406657158, 840489337), new $Int64(799522364, 3483178565), new $Int64(532129761, 2074004656), new $Int64(724246478, 3643392642), new $Int64(1482330167, 1583624461), new $Int64(1261660694, 287473085), new $Int64(1667835381, 3136843981), new $Int64(1138806821, 1266970974), new $Int64(135185781, 1998688839), new $Int64(392094735, 1492900209), new $Int64(1031326774, 1538112737), new $Int64(76914806, 2207265429), new $Int64(260686035, 963263315), new $Int64(1671145500, 2295892134), new $Int64(1068469660, 2002560897), new $Int64(1791233343, 1369254035), new $Int64(33436120, 3353312708), new $Int64(57507843, 947771099), new $Int64(201728503, 1747061399), new $Int64(1507240140, 2047354631), new $Int64(720000810, 4165367136), new $Int64(479265078, 3388864963), new $Int64(1195302398, 286492130), new $Int64(2045622690, 2795735007), new $Int64(1431753082, 3703961339), new $Int64(1999047161, 1797825479), new $Int64(1429039600, 1116589674), new $Int64(482063550, 2593309206), new $Int64(1329049334, 3404995677), new $Int64(1396904208, 3453462936), new $Int64(1014767077, 3016498634), new $Int64(75698599, 1650371545), new $Int64(1592007860, 212344364), new $Int64(1127766888, 3843932156), new $Int64(1399463792, 3573129983), new $Int64(1256901817, 665897820), new $Int64(1071492673, 1675628772), new $Int64(243225682, 2831752928), new $Int64(2120298836, 1486294219), new $Int64(193076235, 268782709), new $Int64(1145360145, 4186179080), new $Int64(624342951, 1613720397), new $Int64(857179861, 2703686015), new $Int64(1235864944, 2205342611), new $Int64(1474779655, 1411666394), new $Int64(619028749, 677744900), new $Int64(270855115, 4172867247), new $Int64(135494707, 2163418403), new $Int64(849547544, 2841526879), new $Int64(1029966689, 1082141470), new $Int64(377371856, 4046134367), new $Int64(51415528, 2142943655), new $Int64(1897659315, 3124627521), new $Int64(998228909, 219992939), new $Int64(1068692697, 1756846531), new $Int64(1283749206, 1225118210), new $Int64(1621625642, 1647770243), new $Int64(111523943, 444807907), new $Int64(2036369448, 3952076173), new $Int64(53201823, 1461839639), new $Int64(315761893, 3699250910), new $Int64(702974850, 1373688981), new $Int64(734022261, 147523747), new $Int64(100152742, 1211276581), new $Int64(1294440951, 2548832680), new $Int64(1144696256, 1995631888), new $Int64(154500578, 2011457303), new $Int64(796460974, 3057425772), new $Int64(667839456, 81484597), new $Int64(465502760, 3646681560), new $Int64(775020923, 635548515), new $Int64(602489502, 2508044581), new $Int64(353263531, 1014917157), new $Int64(719992433, 3214891315), new $Int64(852684611, 959582252), new $Int64(226415134, 3347040449), new $Int64(1784615552, 4102971975), new $Int64(397887437, 4078022210), new $Int64(1610679822, 2851767182), new $Int64(749162636, 1540160644), new $Int64(598384772, 1057290595), new $Int64(2034890660, 3907769253), new $Int64(579300318, 4248952684), new $Int64(1092907599, 132554364), new $Int64(1061621234, 1029351092), new $Int64(697840928, 2583007416), new $Int64(298619124, 1486185789), new $Int64(55905697, 2871589073), new $Int64(2017643612, 723203291), new $Int64(146250550, 2494333952), new $Int64(1064490251, 2230939180), new $Int64(342915576, 3943232912), new $Int64(1768732449, 2181367922), new $Int64(1418222537, 2889274791), new $Int64(1824032949, 2046728161), new $Int64(1653899792, 1376052477), new $Int64(1022327048, 381236993), new $Int64(1034385958, 3188942166), new $Int64(2073003539, 350070824), new $Int64(144881592, 61758415), new $Int64(1405659422, 3492950336), new $Int64(117440928, 3093818430), new $Int64(1693893113, 2962480613), new $Int64(235432940, 3154871160), new $Int64(511005079, 3228564679), new $Int64(610731502, 888276216), new $Int64(1200780674, 3574998604), new $Int64(870415268, 1967526716), new $Int64(591335707, 1554691298), new $Int64(574459414, 339944798), new $Int64(1223764147, 1154515356), new $Int64(1825645307, 967516237), new $Int64(1546195135, 596588202), new $Int64(279882768, 3764362170), new $Int64(492091056, 266611402), new $Int64(1754227768, 2047856075), new $Int64(1146757215, 21444105), new $Int64(1198058894, 3065563181), new $Int64(1915064845, 1140663212), new $Int64(633187674, 2323741028), new $Int64(2126290159, 3103873707), new $Int64(1008658319, 2766828349), new $Int64(1661896145, 1970872996), new $Int64(1628585413, 3766615585), new $Int64(1552335120, 2036813414), new $Int64(152606527, 3105536507), new $Int64(13954645, 3396176938), new $Int64(1426081645, 1377154485), new $Int64(2085644467, 3807014186), new $Int64(543009040, 3710110597), new $Int64(396058129, 916420443), new $Int64(734556788, 2103831255), new $Int64(381322154, 717331943), new $Int64(572884752, 3550505941), new $Int64(45939673, 378749927), new $Int64(149867929, 611017331), new $Int64(592130075, 758907650), new $Int64(1012992349, 154266815), new $Int64(1107028706, 1407468696), new $Int64(469292398, 970098704), new $Int64(1862426162, 1971660656), new $Int64(998365243, 3332747885), new $Int64(1947089649, 1935189867), new $Int64(1510248801, 203520055), new $Int64(842317902, 3916463034), new $Int64(1758884993, 3474113316), new $Int64(1036101639, 316544223), new $Int64(373738757, 1650844677), new $Int64(1240292229, 4267565603), new $Int64(1077208624, 2501167616), new $Int64(626831785, 3929401789), new $Int64(56122796, 337170252), new $Int64(1186981558, 2061966842), new $Int64(1843292800, 2508461464), new $Int64(206012532, 2791377107), new $Int64(1240791848, 1227227588), new $Int64(1813978778, 1709681848), new $Int64(1153692192, 3768820575), new $Int64(1145186199, 2887126398), new $Int64(700372314, 296561685), new $Int64(700300844, 3729960077), new $Int64(575172304, 372833036), new $Int64(2078875613, 2409779288), new $Int64(1829161290, 555274064), new $Int64(1041887929, 4239804901), new $Int64(1839403216, 3723486978), new $Int64(498390553, 2145871984), new $Int64(564717933, 3565480803), new $Int64(578829821, 2197313814), new $Int64(974785092, 3613674566), new $Int64(438638731, 3042093666), new $Int64(2050927384, 3324034321), new $Int64(869420878, 3708873369), new $Int64(946682149, 1698090092), new $Int64(1618900382, 4213940712), new $Int64(304003901, 2087477361), new $Int64(381315848, 2407950639), new $Int64(851258090, 3942568569), new $Int64(923583198, 4088074412), new $Int64(723260036, 2964773675), new $Int64(1473561819, 1539178386), new $Int64(1062961552, 2694849566), new $Int64(460977733, 2120273838), new $Int64(542912908, 2484608657), new $Int64(880846449, 2956190677), new $Int64(1970902366, 4223313749), new $Int64(662161910, 3502682327), new $Int64(705634754, 4133891139), new $Int64(1116124348, 1166449596), new $Int64(1038247601, 3362705993), new $Int64(93734798, 3892921029), new $Int64(1876124043, 786869787), new $Int64(1057490746, 1046342263), new $Int64(242763728, 493777327), new $Int64(1293910447, 3304827646), new $Int64(616460742, 125356352), new $Int64(499300063, 74094113), new $Int64(1351896723, 2500816079), new $Int64(1657235204, 514015239), new $Int64(1377565129, 543520454), new $Int64(107706923, 3614531153), new $Int64(2056746300, 2356753985), new $Int64(1390062617, 2018141668), new $Int64(131272971, 2087974891), new $Int64(644556607, 3166972343), new $Int64(372256200, 1517638666), new $Int64(1212207984, 173466846), new $Int64(1451709187, 4241513471), new $Int64(733932806, 2783126920), new $Int64(1972004134, 4167264826), new $Int64(29260506, 3907395640), new $Int64(1236582087, 1539634186), new $Int64(1551526350, 178241987), new $Int64(2034206012, 182168164), new $Int64(1044953189, 2386154934), new $Int64(1379126408, 4077374341), new $Int64(32803926, 1732699140), new $Int64(1726425903, 1041306002), new $Int64(1860414813, 2068001749), new $Int64(1005320202, 3208962910), new $Int64(844054010, 697710380), new $Int64(638124245, 2228431183), new $Int64(1337169671, 3554678728), new $Int64(1396494601, 173470263), new $Int64(2061597383, 3848297795), new $Int64(1220546671, 246236185), new $Int64(163293187, 2066374846), new $Int64(1771673660, 312890749), new $Int64(703378057, 3573310289), new $Int64(1548631747, 143166754), new $Int64(613554316, 2081511079), new $Int64(1197802104, 486038032), new $Int64(240999859, 2982218564), new $Int64(364901986, 1000939191), new $Int64(1902782651, 2750454885), new $Int64(1475638791, 3375313137), new $Int64(503615608, 881302957), new $Int64(638698903, 2514186393), new $Int64(443860803, 360024739), new $Int64(1399671872, 292500025), new $Int64(1381210821, 2276300752), new $Int64(521803381, 4069087683), new $Int64(208500981, 1637778212), new $Int64(720490469, 1676670893), new $Int64(1067262482, 3855174429), new $Int64(2114075974, 2067248671), new $Int64(2058057389, 2884561259), new $Int64(1341742553, 2456511185), new $Int64(983726246, 561175414), new $Int64(427994085, 432588903), new $Int64(885133709, 4059399550), new $Int64(2054387382, 1075014784), new $Int64(413651020, 2728058415), new $Int64(1839142064, 1299703678), new $Int64(1262333188, 2347583393), new $Int64(1285481956, 2468164145), new $Int64(989129637, 1140014346), new $Int64(2033889184, 1936972070), new $Int64(409904655, 3870530098), new $Int64(1662989391, 1717789158), new $Int64(1914486492, 1153452491), new $Int64(1157059232, 3948827651), new $Int64(790338018, 2101413152), new $Int64(1495744672, 3854091229), new $Int64(83644069, 4215565463), new $Int64(762206335, 1202710438), new $Int64(1582574611, 2072216740), new $Int64(705690639, 2066751068), new $Int64(33900336, 173902580), new $Int64(1405499842, 142459001), new $Int64(172391592, 1889151926), new $Int64(1648540523, 3034199774), new $Int64(1618587731, 516490102), new $Int64(93114264, 3692577783), new $Int64(68662295, 2953948865), new $Int64(1826544975, 4041040923), new $Int64(204965672, 592046130), new $Int64(1441840008, 384297211), new $Int64(95834184, 265863924), new $Int64(2101717619, 1333136237), new $Int64(1499611781, 1406273556), new $Int64(1074670496, 426305476), new $Int64(125704633, 2750898176), new $Int64(488068495, 1633944332), new $Int64(2037723464, 3236349343), new $Int64(444060402, 4013676611), new $Int64(1718532237, 2265047407), new $Int64(1433593806, 875071080), new $Int64(1804436145, 1418843655), new $Int64(2009228711, 451657300), new $Int64(1229446621, 1866374663), new $Int64(1653472867, 1551455622), new $Int64(577191481, 3560962459), new $Int64(1669204077, 3347903778), new $Int64(1849156454, 2675874918), new $Int64(316128071, 2762991672), new $Int64(530492383, 3689068477), new $Int64(844089962, 4071997905), new $Int64(1508155730, 1381702441), new $Int64(2089931018, 2373284878), new $Int64(1283216186, 2143983064), new $Int64(308739063, 1938207195), new $Int64(1754949306, 1188152253), new $Int64(1272345009, 615870490), new $Int64(742653194, 2662252621), new $Int64(1477718295, 3839976789), new $Int64(56149435, 306752547), new $Int64(720795581, 2162363077), new $Int64(2090431015, 2767224719), new $Int64(675859549, 2628837712), new $Int64(1678405918, 2967771969), new $Int64(1694285728, 499792248), new $Int64(403352367, 4285253508), new $Int64(962357072, 2856511070), new $Int64(679471692, 2526409716), new $Int64(353777175, 1240875658), new $Int64(1232590226, 2577342868), new $Int64(1146185433, 4136853496), new $Int64(670368674, 2403540137), new $Int64(1372824515, 1371410668), new $Int64(1970921600, 371758825), new $Int64(1706420536, 1528834084), new $Int64(2075795018, 1504757260), new $Int64(685663576, 699052551), new $Int64(1641940109, 3347789870), new $Int64(1951619734, 3430604759), new $Int64(2119672219, 1935601723), new $Int64(966789690, 834676166)]);
		globalRand = New(new lockedSource.Ptr(new sync.Mutex.Ptr(), NewSource(new $Int64(0, 1))));
	};
	return $pkg;
})();
$packages["/home/bknox/go/src/github.com/taotetek/flyingmeat"] = (function() {
	var $pkg = {}, enj = $packages["github.com/ajhager/enj"], rand = $packages["math/rand"], Sprite, Flyingmeat, app, region, batch, meats, init, main;
	Sprite = $pkg.Sprite = $newType(0, "Struct", "main.Sprite", "Sprite", "/home/bknox/go/src/github.com/taotetek/flyingmeat", function(X_, Y_, DX_, DY_, Image_) {
		this.$val = this;
		this.X = X_ !== undefined ? X_ : 0;
		this.Y = Y_ !== undefined ? Y_ : 0;
		this.DX = DX_ !== undefined ? DX_ : 0;
		this.DY = DY_ !== undefined ? DY_ : 0;
		this.Image = Image_ !== undefined ? Image_ : ($ptrType(enj.Region)).nil;
	});
	Flyingmeat = $pkg.Flyingmeat = $newType(0, "Struct", "main.Flyingmeat", "Flyingmeat", "/home/bknox/go/src/github.com/taotetek/flyingmeat", function(Game_) {
		this.$val = this;
		this.Game = Game_ !== undefined ? Game_ : ($ptrType(enj.Game)).nil;
	});
	init = function() {
	};
	Flyingmeat.Ptr.prototype.Load = function() {
		var f;
		f = this;
		app.Load.Image("../data/meat.png");
	};
	Flyingmeat.prototype.Load = function() { return this.$val.Load(); };
	Flyingmeat.Ptr.prototype.Setup = function() {
		var f, i$1;
		f = this;
		batch = app.NewBatch();
		region = app.NewTexture("../data/meat.png", false).Region(0, 0, 106, 126);
		i$1 = 0;
		while (i$1 < 100) {
			meats = $append(meats, new Sprite.Ptr(0, 0, rand.Float32() * 500, (rand.Float32() * 500) - 250, region));
			i$1 = i$1 + (1) >> 0;
		}
	};
	Flyingmeat.prototype.Setup = function() { return this.$val.Setup(); };
	Flyingmeat.Ptr.prototype.Update = function(dt) {
		var f, minX, maxX, minY, maxY, _ref, _i, meat;
		f = this;
		minX = 0;
		maxX = app.Canvas.Width() - region.Width();
		minY = 0;
		maxY = app.Canvas.Height() - region.Height();
		_ref = meats;
		_i = 0;
		while (_i < _ref.$length) {
			meat = ((_i < 0 || _i >= _ref.$length) ? $throwRuntimeError("index out of range") : _ref.$array[_ref.$offset + _i]);
			meat.X = meat.X + (meat.DX * dt);
			meat.Y = meat.Y + (meat.DY * dt);
			meat.DY = meat.DY + (750 * dt);
			if (meat.X < minX) {
				meat.DX = meat.DX * (-1);
				meat.X = minX;
			} else if (meat.X > maxX) {
				meat.DX = meat.DX * (-1);
				meat.X = maxX;
			}
			if (meat.Y < minY) {
				meat.DY = 0;
				meat.Y = minY;
			} else if (meat.Y > maxY) {
				meat.DY = meat.DY * (-0.8500000238418579);
				meat.Y = maxY;
				if (rand.Float32() > 0.5) {
					meat.DY = meat.DY - (rand.Float32() * 200);
				}
			}
			_i++;
		}
	};
	Flyingmeat.prototype.Update = function(dt) { return this.$val.Update(dt); };
	Flyingmeat.Ptr.prototype.Draw = function() {
		var f, _ref, _i, meat;
		f = this;
		batch.Begin();
		_ref = meats;
		_i = 0;
		while (_i < _ref.$length) {
			meat = ((_i < 0 || _i >= _ref.$length) ? $throwRuntimeError("index out of range") : _ref.$array[_ref.$offset + _i]);
			batch.Draw(meat.Image, meat.X, meat.Y, 0, 0, 1, 1, 0);
			_i++;
		}
		batch.End();
	};
	Flyingmeat.prototype.Draw = function() { return this.$val.Draw(); };
	main = function() {
		app = enj.NewApp(800, 600, false, "Meatcubes", new Flyingmeat.Ptr());
	};
	$pkg.$run = function($b) {
		$packages["github.com/gopherjs/gopherjs/js"].$init();
		$packages["runtime"].$init();
		$packages["errors"].$init();
		$packages["github.com/gopherjs/webgl"].$init();
		$packages["math"].$init();
		$packages["github.com/ajhager/enj"].$init();
		$packages["sync/atomic"].$init();
		$packages["sync"].$init();
		$packages["math/rand"].$init();
		$pkg.$init();
		main();
	};
	$pkg.$init = function() {
		Sprite.init([["X", "X", "", $Float32, ""], ["Y", "Y", "", $Float32, ""], ["DX", "DX", "", $Float32, ""], ["DY", "DY", "", $Float32, ""], ["Image", "Image", "", ($ptrType(enj.Region)), ""]]);
		Flyingmeat.methods = [["Key", "Key", "", [$Int, $Int], [], false, 0], ["Mouse", "Mouse", "", [$Float32, $Float32, $Int], [], false, 0]];
		($ptrType(Flyingmeat)).methods = [["Draw", "Draw", "", [], [], false, -1], ["Key", "Key", "", [$Int, $Int], [], false, 0], ["Load", "Load", "", [], [], false, -1], ["Mouse", "Mouse", "", [$Float32, $Float32, $Int], [], false, 0], ["Setup", "Setup", "", [], [], false, -1], ["Update", "Update", "", [$Float32], [], false, -1]];
		Flyingmeat.init([["Game", "", "", ($ptrType(enj.Game)), ""]]);
		app = ($ptrType(enj.App)).nil;
		region = ($ptrType(enj.Region)).nil;
		batch = ($ptrType(enj.Batch)).nil;
		meats = ($sliceType(($ptrType(Sprite)))).nil;
		init();
	};
	return $pkg;
})();
$error.implementedBy = [$packages["errors"].errorString.Ptr, $packages["github.com/gopherjs/gopherjs/js"].Error.Ptr, $packages["runtime"].NotSupportedError.Ptr, $packages["runtime"].TypeAssertionError.Ptr, $packages["runtime"].errorString, $ptrType($packages["runtime"].errorString)];
$packages["github.com/gopherjs/gopherjs/js"].Object.implementedBy = [$packages["github.com/ajhager/enj"].App, $packages["github.com/ajhager/enj"].App.Ptr, $packages["github.com/ajhager/enj"].Canvas, $packages["github.com/ajhager/enj"].Canvas.Ptr, $packages["github.com/ajhager/enj"].Shader, $packages["github.com/ajhager/enj"].Shader.Ptr, $packages["github.com/gopherjs/gopherjs/js"].Error, $packages["github.com/gopherjs/gopherjs/js"].Error.Ptr, $packages["github.com/gopherjs/webgl"].Context, $packages["github.com/gopherjs/webgl"].Context.Ptr];
$packages["github.com/ajhager/enj"].Managed.implementedBy = [$packages["github.com/ajhager/enj"].Texture.Ptr];
$packages["github.com/ajhager/enj"].Responder.implementedBy = [$packages["/home/bknox/go/src/github.com/taotetek/flyingmeat"].Flyingmeat.Ptr, $packages["github.com/ajhager/enj"].Game.Ptr];
$packages["math/rand"].Source.implementedBy = [$packages["math/rand"].Rand.Ptr, $packages["math/rand"].lockedSource.Ptr, $packages["math/rand"].rngSource.Ptr];
$go($packages["/home/bknox/go/src/github.com/taotetek/flyingmeat"].$run, [], true);

})();
//# sourceMappingURL=flyingmeat.js.map
